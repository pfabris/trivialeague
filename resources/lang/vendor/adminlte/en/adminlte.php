<?php

return [

    'full_name'                   => 'Full Name',
    'first_name'                  => 'First name',
    'last_name'                   => 'Last name',
    'email'                       => 'Email',
    'password'                    => 'Password',
    'retype_password'             => 'Retype password',
    'remember_me'                 => 'Remember Me',
    'register'                    => 'Register',
    'register_a_new_membership'   => 'Join up!',
    'i_forgot_my_password'        => 'I forgot my password',
    'i_already_have_a_membership' => 'Already Registered? Go to Login',
    'sign_in'                     => 'Log In',
    'log_out'                     => 'Log Out',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Welcome to :appName!',
    'register_message'            => 'Join up!',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Send Password Reset Link',
];
