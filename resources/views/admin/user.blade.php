@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ Users 〰️ ' . $user->nameFull )

@section('content_header')
	<h1>{{$league->leagueName}} User</h1>
	<ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="{{ route('adminLeague', null, false) }}">League</a></li>
        <li><i class="fa fa-users"></i> <a href="{{ route('adminLeagueUsers', null, false) }}">Users</a></li>
        <li class="active"><i class="fa fa-user"></i> {{ $user->nameFull }}</a></li>
	</ol>
	 
@stop

@section('content')

	<!-- ######################  USER ROW  ############################################################################ -->
	<div class="row">
			
			<div class="col-md-4">
				<!-- ****** AVATAR and STATS Callout ****************************************************** -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" 
							src="{{ ( empty($user->avatar) ? asset('storage/avatars/users/default.jpg') : $user->avatar ) }}" alt="User avatar">
						
						<h3 class="profile-username text-center">{{ $user->nameFull }}</h3>
						
						<p class="text-muted text-center">{{ $user->primaryRole }}</p>
						
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Games Played</b> <a class="pull-right">{{ formatNumber($gamesPlayed) }}</a>
							</li>
							<li class="list-group-item">
								<b>Current Season Rank </b> <a class="pull-right">{{ formatNumber($seasonRank) }}</a>
							</li>
							<li class="list-group-item">
								<b>All Time Rank</b> <a class="pull-right">{{ formatNumber($allTimeRank) }}</a>
							</li>
						</ul>			
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			
			<div class="col-md-8">
			
				<div class="box">
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border">
						<h3 class="box-title">
							<i class="fa fa-user"></i> User Details
						</h3>
					</div>
					
					@if ($errors->any())
					<div class="box-header with-border">
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
				    </div>
					@endif
	
					<!-- ****** USER FORM ****************************************************** -->
					
					<form class="form-horizontal" method="POST" 
						action="{{ route('adminUserUpdate',['id' => $user->id]) }}">
						
						<input name="_method" type="hidden" value="PATCH">
				        {{csrf_field()}}
						
						<input type="hidden" name="league_id" value="{{$league->id}}">
						<input type="hidden" name="user_id" value="{{$user->id}}">
						
						<div class="box-body">
							<div class="form-group">
								<label for="inputFirstName" class="col-sm-2 control-label">FirstName</label>
					
								<div class="col-sm-10">
									<input type="text" name="nameFirst" value="{{$user->nameFirst}}" class="form-control" id="inputFirstName" placeholder="First Name" required>
								</div>
							</div>
							<div class="form-group">
								<label for="inputLastName" class="col-sm-2 control-label">LastName</label>
					
								<div class="col-sm-10">
									<input type="text" name="nameLast" value="{{$user->nameLast}}" class="form-control" id="inputLastName" placeholder="Last Name">
								</div>
							</div>
							<div class="form-group">
								<label for="selectTeam" class="col-sm-2 control-label">Team</label>
					
								<div class="col-sm-10">
									<select id="selectTeam" name="team_id" class="form-control">
										<optgroup label="">
											<option value="">Unaffiliated</option>
										</optgroup>
										<optgroup label="Teams">
											@forelse ($teams as $team)
											<option value="{{$team->teamID}}" @if($team->teamID==$user->teamID) selected=selected @endif>{{$team->teamName}}</option>
											@empty
										
											@endforelse
										</optgroup>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputDateJoined" class="col-sm-2 control-label">Date Joined</label>
								<div class="col-sm-10">
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" name="dateJoined" class="form-control pull-right datepicker" id="inputDateJoined" value="{{$user->dateJoined}}" placeholder="The date, roughly, that you joined the League (yyyy-mm-dd)">
									</div>
									<!-- /.input group -->
								</div>
							</div>
							
							<div class="form-group">
								<label for="selectRole" class="col-sm-2 control-label">Role</label>
					
								<div class="col-sm-10">
									<select id="selectRole" name="primaryRole" class="form-control">
										@forelse ($roles as $role)
											<option value="{{$role}}" @if($role==$user->primaryRole) selected=selected @endif>{{$role}}</option>
										@empty
										
										@endforelse
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Email</label>
					
								<div class="col-sm-10">
									<input type="email" name="email" value="{{$user->email}}" class="form-control" id="inputEmail" placeholder="Email" data-error="Email address is invalid">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputPhone" class="col-sm-2 control-label">Mobile</label>
					
								<div class="col-sm-10">
									<input type="text" name="phoneMobile" value="{{$user->phoneMobile}}" class="form-control" id="inputPhone" placeholder="Phone">
								</div>
							</div>

							<div class="form-group">
								<label for="selectNotify" class="col-sm-2 control-label">Notify Via</label>
					
								<div class="col-sm-10">
									<select id="selectNotify" name="notificationPreference" class="form-control">
										@forelse ($notificationPrefs as $pref)
											<option value="{{$pref}}" @if($pref==$user->notificationPreference) selected=selected @endif>{{$pref}}</option>
										@empty
										
										@endforelse
										</optgroup>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="status" value="Active" @if($user->status=="Active") checked=checked @endif> Active
										</label>
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
						
						@can('edit-users')
						<div class="box-footer">
							<button type="submit" class="btn btn-primary pull-right"> <i class="fa fa-save"></i> Save Changes</button>
						</div>
						<!-- /.box-footer -->
						@endcan
						
					</form>
					
					@can('edit-users')
					<div class="box-footer">
						
						<div class="col-sm-4">
						<form action="{{ route('adminUserDelete',['id' => $user->id]) }}" method="post" id="deleteForm" >
						    {{ csrf_field() }}
						    <input type="hidden" name="_method" value="delete"/>
						    <button type="button" class="btn btn-danger" id="btnDeleteUser"> <i class="fa fa-trash"></i> Delete User</button>
						</form>
						</div>
						
						@if(empty($user->password))
						<div class="col-sm-4">
						<form action="{{ route('processInvitation') }}" method="post" >
						    {{ csrf_field() }}
						    <input type="hidden" name="userID" value="{{$user->id}}"/>
						    <input type="hidden" name="email" value="{{$user->email}}"/>
						    <button type="submit" class="btn btn-success" @if(empty($user->email)) disabled=disabled @endif> <i class="fa fa-envelope"></i> Send Invitation Email</button>
						</form>
						</div>
						@endif
					</div>
					<!-- /.box-footer -->
					@endcan
					
					
					
				</div>
				<!-- /.box -->
				
			</div>
			<!-- /.col-md-8 -->

	</div>
	<!-- /.row -->
	
	<!-- ######################  Permissions ROW  ############################################################################ -->
	<div class="row">
		<div class="col-md-12">
			

			
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Permissions</h3>
					
				</div>
				<!-- /.box-header -->
				
				<div class="box-body no-padding">
					<table class="table table-striped">
						<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th style="width: 10px"></th>
								<th>This has these roles:</th>
							</tr>
							@forelse($roles as $role)
							<tr>
								<td>{{$loop->iteration}}</td>
								<td><i class="fas fa-minus-circle"></i></td>
								<td>{{$role}}</td>
								
							</tr>
							@empty
							<tr>
								<td colspan="2"><p>No Roles for this user</p></td>
							</tr>
							@endforelse
							
							<tr>
								<th style=""></th>
								<th  colspan="2">This user can:</th>
							</tr>
							@forelse($permissions as $perm)
							<tr>
								<td></td>
								<td colspan="2">{{$perm->name}}</td>
							</tr>
							@empty
							<tr>
								<td colspan="2"><p>No Permissions for this user</p></td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
			
		</div>
	</div>
	<!-- /.row -->
	
	@component('modals.default',['modal_id' => 'modal-default-delete'])
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop

@section('footer')

		<p>
			<span class="text-small">
				Created: {{ date('M j, Y g:ia', strtotime( $user->created_at )) }} 
			</span>
			<span class="pull-right text-small">
				Modified: {{ date('M j, Y g:ia', strtotime( $user->updated_at )) }} 
			</span>
		</p>

@stop

@section('js')
<script type="text/javascript">
	
	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
	var leagueID 	 	= "{{ $league->id }}";
	$('#btnDeleteUser').click(function()
	{

		$('#modal-default-delete .modal-title').html('<h4 class="text-danger"><i class="fas fa-trash text-danger"></i> Delete User</h4>');
		$('#modal-default-delete .modal-body').html("<p>Are you sure you want to delete this User? Their stats will be gone.");
		$('#modal-default-delete-btn-ok').text('Delete User');
		$('#modal-default-delete').modal('show');
		$('#modal-default-delete-btn-ok').click(function() 
		{ 
			$("#deleteForm").submit();
		});
		
	});
</script>		
@stop