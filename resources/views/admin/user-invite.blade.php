@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ Invite User ' . $user->nameFull )

@section('content_header')
	<h1>{{$league->leagueName}} User</h1>
	<ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="{{ route('adminLeague', null, false) }}">League</a></li>
        <li><i class="fa fa-users"></i> <a href="{{ route('adminLeagueUsers', null, false) }}">Users</a></li>
        <li class="active"><i class="fa fa-user"></i> {{ $user->nameFull }}</a></li>
	</ol>
	 
@stop

@section('content')

	<!-- ######################  INVITE  ############################################################################ -->
	<div class="row">
			
			<div class="col-md-12">
				<div class="box box-primary">


					<form action="{{ route('invite') }}" method="post">
					    {{ csrf_field() }}
					    <input type="email" name="email" value="{$user->email}"/>
					    <button type="submit">Send invite</button>
					</form>
					

				<!-- /.box -->
			</div>

	</div>
	
	
	@component('modals.default')
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop
