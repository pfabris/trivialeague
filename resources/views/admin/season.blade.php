@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ '. $season->seasonName )

@section('content_header')
	<h1>{{ $season->seasonName }} Season — Game Nights</h1>
	<ol class="breadcrumb">
        <li><a href="{{ route('adminSeasons') }}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('adminSeason', ['id' => $season->id ]) }}"><i class="fa fa-navicon"></i> {{ $season->seasonName }} </a></li>
	</ol>
@stop

@section('content')

		<div class="row" id="gameNights">
			
			<div class="col-md-6">
			
				<div class="box">
					
					<!-- ****** MAIN Games Table  ****************************************************** -->
					<div class="box-body table-responsive">
						<table class="table table-striped">
							<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>GameNight</th>
								<th> # Matches </th>
								<th></th>
							</tr>
							
							@forelse ($gameNights as $gameNight)
								@php
								if($gameNight->date > date('Y-m-d') && ($color=="success" || $color=="info"))
								{
									$color="info";
								}
								else if($gameNight->date > date('Y-m-d'))
								{
									$color="success";
								}
								else if($gameNight->date < date('Y-m-d'))
								{
									$color="warning";
								}
								else {
									$color="danger";
								}
								@endphp
									<tr>
										<td>
											<span class="label label-{{$color}}">{{ $loop->iteration }}</span>
										</td>
										<td>
											<a href="{{ route('adminGameNight', $gameNight->id, false) }}"> {{ date('D M j, Y', strtotime( $gameNight->date )) }}</a>
										</td>
										<td>
											{{$gameNight->games_count}}
										</td>
										<td>
											@if($gameNight->games_count>0)
											<a href="{{ route('adminGameNight', $gameNight->id, false) }}" class="btn btn-block btn-success btn-xs"> 
												Show Matches <i class="fa fa-arrow-circle-right"></i>
											</a>
											@else
											<a href="{{ route('adminGameNightSetup', $gameNight->id, false) }}" class="btn btn-block btn-warning btn-xs"> 
												Setup Matches <i class="fa fa-edit"></i>
											</a>
											@endif
										</td>
									</tr>
									@if($loop->last)
										<tr>
										<td colspan="5">
											@can('edit-season')
												<button type="button" id="btnGenerateGameNights" class="btn btn-block btn-danger ">
													<i class="fas fa-trash"></i> Delete GameNights
												</button>
											@endcan
										</td>
										</tr>
									@endif
									
							@empty
									<tr>
										<td colspan="4">😟 No GameNights Setup yet...</td>
									</tr>
									
							@endforelse
							
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>	
			<!-- /.col-md-12 -->
	</div>
	<!-- /.row -->

	@component('modals.danger',['modal_id' => 'modal-danger'])
		Not working yet!
	@endcomponent

@stop

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/overrides.css">
@stop

@section('js')
	<script>
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
		$('#btnGenerateGameNights').click(function() 
		{
			$('#modal-danger').modal('show');
		});
		

	</script>
@stop