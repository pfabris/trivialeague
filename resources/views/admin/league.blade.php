@extends('adminlte::page')

@section('title', $league->leagueName )

@php
	$arbitraryData = json_decode($league->arbitraryJSON);
@endphp


@section('css')
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/overrides.css">
		<meta name="csrf-token" content="{{ csrf_token() }}">

@stop

@section('content_header')
	<h1>{{$league->leagueName}}</h1>
	<ol class="breadcrumb">
        <li><i class="fa fa-home"></i> League</li>
	</ol>
@stop

@section('content')

	<!-- ######################  LEAGUE SETTINGS  ###################################################################### -->
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						Settings
					</h3>
				</div>
				
				<form class="form-horizontal" method="POST" 
						action="{{ route('adminLeagueUpdate',['id' => $league->id]) }}">
						
						<input name="_method" type="hidden" value="PATCH">
				        {{csrf_field()}}
						
						<input type="hidden" name="league_id" value="{{$league->id}}">
						
						<div class="box-body">
							
<!-- Name -->
							<div class="form-group">
								<label for="inputLeagueName" class="col-sm-2 control-label">League Name</label>
								<div class="col-sm-10">
									<input type="text" name="leagueName" value="{{$league->leagueName}}" 
										class="form-control" id="inputLeagueName" placeholder="League Name" required>
								</div>
							</div>
							<!--.form-group -->

<!-- Neighborhood -->							
							<div class="form-group">
								<label for="inputNeighbourhood" class="col-sm-2 control-label">Neighbourhood</label>
								<div class="col-sm-10">
									<input type="text" name="neighbourhood" value="{{$league->neighbourhood}}" 
										class="form-control" id="inputNeighbourhood" placeholder="Neighbourhood" required>
								</div>
							</div>
							<!--.form-group -->

<!-- Address -->							
							<div class="form-group">
								<label for="inputCity" class="col-sm-2 control-label">City</label>
								<div class="col-sm-4">
									<input type="text" name="city" value="{{$league->city}}" 
										class="form-control" id="inputCity" placeholder="City" required>
								</div>
								<label for="inputProv" class="col-sm-1 control-label">Prov</label>
								<div class="col-sm-1">
									<input type="text" name="province" value="{{$league->province}}" 
										class="form-control" id="inputProv" placeholder="Prov" required>
								</div>
								<label for="inputCountry" class="col-sm-2 control-label">Country</label>
								<div class="col-sm-2">
									<input type="text" name="country" value="{{$league->country}}" 
										class="form-control" id="inputCountry" placeholder="Country" required>
								</div>
							</div>
							<!--.form-group -->
							
<!-- Day and Time -->
							<div class="form-group">
								<label for="inputDayOfWeek" class="col-sm-2 control-label">Play On Day of Week</label>
								<div class="col-sm-1">
									<input type="text" name="gameNightDayOfWeek" value="{{$league->gameNightDayOfWeek}}" 
										class="form-control" id="inputDayOfWeek" placeholder="Day of Week" required>
								</div>
								<label for="inputGameNightStartTime" class="col-sm-2 control-label">Start Time</label>
								<div class="col-sm-2">
									<input type="text" name="gameNightStartTime" value="{{$league->gameNightStartTime}}" 
										class="form-control" id="inputGameNightStartTime" placeholder="Start Time" required>
								</div>
							</div>
							<!--.form-group -->
							
<!-- Per Game Settings -->							
							<div class="form-group">
								<label for="inputRoundsPerGame" class="col-sm-2 control-label">Per Game: Rounds</label>
								<div class="col-sm-1">
									<input type="text" name="roundsPerGame" value="{{$league->roundsPerGame}}" 
										class="form-control" id="inputRoundsPerGame" placeholder="# of Rounds" required>
								</div>
								<label for="inputQuestionsPerRound" class="col-sm-2 control-label">Questions</label>
								<div class="col-sm-1">
									<input type="text" name="questionsPerRound" value="{{$league->questionsPerRound}}" 
										class="form-control" id="inputQuestionsPerRound" placeholder="# Questions" required>
								</div>
								<label for="inputSeatsPerGame" class="col-sm-2 control-label">Players</label>
								<div class="col-sm-1">
									<input type="text" name="seatsPerGame" value="{{$league->seatsPerGame}}" 
										class="form-control" id="inputSeatsPerGame" placeholder="# Players" required>
								</div>
								<div class="col-sm-3">
									<div class="checkbox icheck">
										Swap Question Order 2nd Half 
										<label>
											<input type="checkbox" name="swapQuestionOrder2ndHalf" value="1" @if($league->swapQuestionOrder2ndHalf=="1") checked=checked @endif >
										</label>
									</div>
									
								</div>
							</div>
							<!--.form-group -->

<!-- Arbitrary Data -->
							<div class="form-group">
								<label for="inputArbitraryLeagueEmail" class="col-sm-2 control-label">League Email</label>
								<div class="col-sm-10">
									<input type="text" name="arbitraryLeagueEmail" value="{{ optional($arbitraryData)->league_email }}" 
										class="form-control" id="inputArbitraryLeagueEmail" placeholder="League Email Address">
								</div>
							</div>
							<div class="form-group">
								<label for="inputArbitraryLeagueFacebook" class="col-sm-2 control-label">League Facebook Page</label>
								<div class="col-sm-10">
									<input type="text" name="arbitraryLeagueFacebook" value="{{ optional($arbitraryData)->social_facebook }}" 
										class="form-control" id="inputArbitraryLeagueFacebook" placeholder="https://www.facebook.com/...">
								</div>
							</div>
							<div class="form-group">
								<label for="inputArbitraryLeagueTwitter" class="col-sm-2 control-label">League Twitter Handle</label>
								<div class="col-sm-10">
									<input type="text" name="arbitraryLeagueTwitter" value="{{ optional($arbitraryData)->social_twitter }}" 
										class="form-control" id="inputArbitraryLeagueTwitter" placeholder="@TwitterHandle">
								</div>
							</div>

							<div class="form-group">
								<label for="inputLeagueManager" class="col-sm-2 control-label">League Manager</label>
								<div class="col-sm-5">
									<input type="text" name="leagueManager" value="{{ $league->user->id }}" 
										class="form-control" id="inputLeagueManager" placeholder="id">
								</div>
								<div class="col-sm-5">
									<p>{{ $league->user->nameFull }}</p>
								</div>

							</div>

						</div>
						<!--.box-body -->
				</form>
				
			</div>
		</div>
	</div>
	
	
	
	<!-- ######################  LOCATIONS  ############################################################################ -->
	<div class="row">
			
			<div class="col-md-12">
				<div class="box">
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border">
						<h3 class="box-title">
							League Locations
						</h3>
					</div>
					
					<!-- ****** LOCATIONS Table  ****************************************************** -->
					<div class="box-body table-responsive">
						<table class="table">
							<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>Location</th>
								<th>Address</th>
							</tr>
							
							@forelse ($locations as $location)
									<tr>
										<td>
											<span class="label label-default">{{ $loop->iteration }}</span>
										</td>
										<td>
											<a href="{{ route('adminLocation', $location->id, false) }}">{{ $location->locationName }}</a>
										</td>
										<td>
											{{ $location->address }}
										</td>
									</tr>
							@empty
							<tr>
								<td colspan="3">
									🤭 No Locations setup yet.
								</td>
							</tr>
							@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>

	</div>
	
	<!-- ######################  MAP  ############################################################################ -->
	<div class="row">
		
		<div class="col-md-12">
			<div id="map">
			</div>
		</div>
	</div>

	@component('modals.default')
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop



@section('adminlte_js')
	<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '10%' // optional
			});
		});
	</script>
	
	<script>
		$.ajaxSetup({
		  beforeSend: function() {}
		})
	</script>
	<script>
      $(document).ready(function (){
			var map;
		    var elevator;
			var center = {lat: 43.6659661, lng: -79.3407657};
			var mapOptions = {
			    zoom: 13,
			    center: center,
			    //mapTypeId: 'terrain'
			};
			var labels = '123456789';
			var labelIndex = 0;
			var map = new google.maps.Map(document.getElementById('map'), mapOptions);
			var infoWindow = new google.maps.InfoWindow();
			
		  	var locations = {
		        @foreach ($locations as $loc)
				'{{$loc->locationName}}': { name: '{{$loc->locationName}}', lat: {{$loc->lat}}, lng:{{$loc->lng}}, address:'{{$loc->address}}' } @if(!$loop->last),@endif
		        @endforeach
			}
			
			for (var key in locations) 
			{
				if (!locations.hasOwnProperty(key)) continue;
				var loc = locations[key];
				//console.log( p.lat + " " + p.lng );
			    var latlng = new google.maps.LatLng(loc.lat, loc.lng);
			    var marker = new google.maps.Marker({
				                position: latlng,
				                map: map,
				                label: {text: labels[labelIndex++ % labels.length], color: "white"}
				            });
				
				//Attach click event to the marker.
	            (function (marker, loc) 
	            {
	                google.maps.event.addListener(marker, "click", function (e) {
	                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
	                    infoWindow.setContent("<div style = 'width:140px;min-height:32px'><strong>" + loc.name + "</strong><br>" + loc.address +"</div>");
	                    infoWindow.open(map, marker);
	                });
	            })(marker, loc);

			}	
	    });
    </script>
    
	<script async defer
    	src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.maps.api_key') }}">
    </script>
@stop
