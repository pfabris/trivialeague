@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ ' . $location->locationName)

@section('content_header')
	<h1>{{$league->leagueName}}</h1>
	<ol class="breadcrumb">
        <li><a href="{{ route('adminLeague') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">{{$location->locationName}}</li>
	</ol>
	 
@stop

@section('content')

	<!-- ######################    ############################################################################ -->
	<div class="row">
			
			<div class="col-md-6">
				<div class="box">
					<div class="box-body">
						<h3 class="box-title">
							{{$location->locationName}}
						</h3>
						<p>
							<i class="fa fa-map"></i> {{$location->address}} {{$location->city}} {{$location->province}} {{$location->postalCode}} 
						</p>
						@if(!empty($location->website))
						<p>
							<i class="fa fa-globe"></i> <a href="http://{{$location->website}}" target="_blank">{{$location->website}}</a>
						</p>
						@endif
						@if(!empty($location->phone))
						<p>
							<i class="fa fa-phone"></i> {{$location->phone}}
						</p>
						@endif
						@if(!empty($location->email))
						<p>
							<i class="fa fa-mail"></i> <a href="mailto:{{$location->email}}">{{$location->email}}</a>
						</p>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-6">
			
				<div class="box">
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border bg-blue">
						<h3 class="box-title">
							{{$location->locationName}} Teams
						</h3>
					</div>
					
					<!-- ****** TEAMS Table  ****************************************************** -->
					<div class="box-body table-responsive">
						<table class="table">
							<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>Team</th>
								<th>Captain</th>
								<th>Email</th>
							</tr>
							
							@forelse ($teams as $team)
									<tr>
										<td>
											<span class="label label-default">{{ $loop->iteration }}</span>
										</td>
										<td>
											<a href="{{ route('adminTeam', $team->id, false }}">{{ $team->teamName }}</a>
										</td>
										<td>
											{{ $captains[$loop->index]->nameFull }} 
										</td>
										<td>
											@if( !empty ($captains[$loop->index]->email) )
												<a href="mailto:{{ $captains[$loop->index]->email }}"><i class="fa fa-envelope"> </i> {{ $captains[$loop->index]->email }}</a>
											@endif
										</td>
									</tr>
							@empty
							<tr>
								<td colspan="3">
									🤭 No Teams setup yet.
								</td>
							</tr>
							@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
	
	<!-- ######################    ############################################################################ -->
	<div class="row">
			
			<div class="col-md-4">
			
				<div class="box">
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border bg-blue">
						<h3 class="box-title">
							Game Nights played at {{$location->locationName}} 
						</h3>
					</div>
					
					<!-- ****** GAMENIGHTS Table  ****************************************************** -->
					<div class="box-body table-responsive">
						<table class="table">
							<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>Date</th>
							</tr>
							
							@forelse ($gameNights as $gameNight)
									<tr>
										<td>
											<span class="label label-default">{{ $loop->iteration }}</span>
										</td>
										<td>
											<a href="{{ route('adminGameNight',$gameNight->id, false) }}">{{ date('M j, Y', strtotime( $gameNight->date )) }}</a>
										</td>
									</tr>
							@empty
							<tr>
								<td colspan="2">
									🤭 No Game Nights setup yet.
								</td>
							</tr>
							@endforelse
							</tbody>
						</table>
					</div>
					
					
				</div>
			</div>

	</div>
	
	@component('modals.default')
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop