@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ '. $season->seasonName . '  〰️ GameNight ' .  $gameNight->gameNumber )

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/overrides.css">
	<style>
		ul.sortable  {
			list-style: none;
			padding: 6px;
		}
		.sortable li {
			background: #eaeaea;
			margin: 0px 0px 4px 0px;
			padding: 6px;
			cursor: move;
			cursor: -webkit-grabbing;  
		}
		.sortable-ghost {
			opacity: .2;
		}
		ul.target {
			min-height: 42px;
			border-radius: 3px;
			border: 1px dashed #b3b3b3;
		}
	</style>
@stop
@section('content_header')
	<h1>
		{{ date('M j, Y', strtotime( $gameNight->date )) }} GameNight {{ $gameNight->gameNumber }}
	</h1>

	<ol class="breadcrumb">
		<li><a href="{{ route('adminSeasons') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ route('adminSeason', ['id' => $season->id ]) }}"><i class="fa fa-navicon"></i> {{ $season->seasonName }} </a></li>
		<li class="active"><i class="fa fa-gamepad"></i> {{ date('M j', strtotime( $gameNight->date )) }} </li>
	</ol>

@stop

@section('content')



	<div class="row">
		
		
		<div class="col-xs-8">
			
			<div class="box">
				
				<div class="box-header">
					<h3 class="">
						Locations: Setup Matches
					</h3>
				</div>
			</div>

			
					
					@foreach($locations as $location)

							
								
								<div class="box box-danger box-solid  @if(!$loop->first) collapsed-box @endif " 
									id="location_box_{{ $location->id }}">
									
									<div class="box-header">
										<h3 class="box-title">
											{{$location->locationName}}
										</h3>
										<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa @if(!$loop->first) fa-plus @else fa-minus @endif"></i>
										</button>
										</div>
									</div>
									<div class="box-body">
										
										
										<div class="col-sm-6" id="visiting_team_box_{{ $location->id }}">
											<div class="box box-info">
												<div class="box-header with-border">
													<p>Visitor</p>
												</div>
								                <!-- /.box-header -->
									
								                <div class="box-body no-padding">
													<ul id="visitingTeam_{{$loop->iteration}}" data-location="{{ $location->id }}" data-type="visitor-location" class="sortable target">
														
													</ul>
													<!-- / visitor_ -->
								                </div>
								                <!-- /.box-body -->
											</div>
										</div>	
										<!-- #visiting_team_box ./col -->

										<div class="col-sm-6" id="home_team_box_{{ $location->id }}">
											<div class="box box-info">
												<div class="box-header with-border">
													<label>@ Home Team</label>
													<div class="box-tools pull-left">
														<span class="label label-danger"></span>
													</div>
												</div>
												<div class="box-body no-padding">
													@foreach($location->teams as $team)
													
													<div class="radio iradio">
														<label for="{{ $team->id }}">
															<input type="radio" class="radio-input" name="{{ $location->id }}" id="{{ $team->id }}" data-teamid="{{ $team->id }}"> 
															<span id="label_{{ $team->id }}">{{ $team->teamName }}</span>
														</label>
													</div>
													@endforeach
												</div>
											</div>
										</div>
										<!-- #home_team_box ./col -->
																
									</div>
									
								</div>
								<!-- #location_box ./box -->
						
					@endforeach
					
				
		
		</div>
		<!-- /.col -->
		
		<div class="col-xs-4">
			<div class="box">
				
				<div class="box-header">
					<h3 class="">
						Available Teams
					</h3>
				</div>
				
				<div class="box-body">
					<ul id="teamPool" class="sortable">
						@foreach($locations as $location)
							@foreach($location->teams as $team)
							<li data-id="{{$team->id}}" id="pool_{{$team->id}}">
								<i class="fa fa-users"></i> {{$team->teamName}}
							</li>
							@endforeach
						@endforeach
					</ul>
				</div>
				<!-- ./box-body -->
				
			</div>
			<!-- ./box -->
		</div>
		<!-- /.col -->
		
		
		
	</div>
	<!-- /.row -->
			
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-block btn-success" id="btn_setupGameNight">Setup</button>
		</div>
	</div>
	


		
@stop

@section('footer')
@stop



@section('js')
<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script>

	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
	var gamenight_id = "{{ $gameNight->id }}";
	var numberOfLocations = {{ count($locations) }};
	var homeTeamList = {};
	var visitorList = {};
	localStorage.setItem("teamsList", "");
	localStorage.setItem("visitorList", "");
	
	$(function () 
	{
		$('.iradio input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			disabledRadioClass: 'iradio_square-blue-disabled',
			increaseArea: '10%' // optional
		});
		$('.iradio input').on('ifChecked', function(event)
		{
			var selectedTeam = event.currentTarget.id;
			
			if ( $( ".target #pool_" + selectedTeam ).length )
			{
				var x = $(".target #pool_" + selectedTeam );
				$( ".target #pool_" + selectedTeam ).remove();
				$( "#teamPool" ).prepend(x);	
			}
			$("#pool_" + selectedTeam ).hide();
			homeTeamList[$(this).attr('name')]=selectedTeam;
		});
		$('.iradio input').on('ifUnchecked', function(event)
		{
			var unSelectedTeam = event.currentTarget.id;
			$("#pool_" + unSelectedTeam ).show();
		});
	});
	
	$(document).ready(function() 
	{

		Sortable.create(teamPool, 
		{
			sort: false,
			animation: 140,
			group: {
				name:'teamsList',
				pull: true,
				put: ['visitorList']
			}
		});
		
		
		
		var options = {
			sort: false,
			animation: 140,
			group: 
			{
				name:'visitorList',
				pull: true,
				put: function (to) 
				{
					return to.el.children.length < 1;
			    }
			},
			onSort: function (/**Event*/evt)  // Called by any change to the list (add / update / remove)
			{
				// add the TeamID that was just added to the Box to the VisitorList
				var locationID		= evt.target.dataset.location;
				var selectedTeamID	= evt.item.id.split("_")[1];
				visitorList[locationID]=selectedTeamID;
				enable_btn_setupGameNight();
			},
			onAdd: function (/**Event*/evt) 
			{
				// disable the HomeTeam Radio Button selector for the TeamID that was just added to a Box
				var selectedTeamID	= evt.item.id.split("_")[1];
				$( "#" + selectedTeamID ).iCheck('disable');
				$( "#label_" + selectedTeamID ).css({"text-decoration":"line-through","color":"#cecece"});
			},
			onRemove: function (/**Event*/evt) 
			{
				// enable the HomeTeam Radio Button selector for the TeamID that was just removed from a Box
				var selectedTeamID	= evt.item.id.split("_")[1];
				$( "#" + selectedTeamID ).iCheck('enable');
				$( "#label_" + selectedTeamID ).css({"text-decoration":"none","color":"inherit"});
			}
		}
		
		for( var i=1 ; i<=numberOfLocations ; i++ )
		{
			var el = document.getElementById( 'visitingTeam_' + i );
			Sortable.create( el,  options );
		}
		
		enable_btn_setupGameNight();

		function enable_btn_setupGameNight()
		{
			// Enable the Setup Button when all HomeTeams and Visitor Teams have been selected
			if( (Object.keys(homeTeamList).length + Object.keys(visitorList).length) == (numberOfLocations * 2) ) 
			{
				$('#btn_setupGameNight').prop('disabled', false).html('<i class="fa fa-fw fa-play"></i> Setup');
			}
			else
			{
				$('#btn_setupGameNight').prop('disabled', true).html('<i class="fa fa-fw fa-ban"></i> To Complete Setup, Match teams for all locations first)');
			}
		}
		
		$('#btn_setupGameNight').click(function()
		{
			// Create Game and initialize GameTeams (final GameTeams, GameTeamRounds and Scores are setup in game-edit)
			
			// console.log(visitorList);
			// console.log(homeTeamList);
			
			//Create Games and GameTeams
			$.post(
				"{{route('adminAjaxGameNightSetup', $gameNight->id)}}", 
				{ 
					visitorTeamList : visitorList, 
					homeTeamList: homeTeamList, 
					gamenight_id : gamenight_id
				}, 
				function( response, status ) {null}
			)
			.done(function() 
			{
				window.location.href="{{route( 'adminGameNight', $gameNight->id)}}";
			})
			.fail(function(response, status, error) 
			{
				console.log(response);
				$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
				$('#modal-danger').modal('show');
			})
		});
	});
</script>
@stop