@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ Users')

@section('content_header')
	<h1>{{$league->leagueName}} Users</h1>
	<ol class="breadcrumb">
        <li><i class="fa fa-home"></i> <a href="{{ route('adminLeague', null, false) }}">League</a></li>
        <li class="active"><i class="fa fa-users"></i> Users</a></li>
	</ol>
	 
@stop

@section('content')

	<!-- ######################    ############################################################################ -->
	<div class="row">
			
			<div class="col-md-12">
			
				<div class="box">
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border bg-blue">
						<h3 class="box-title">
							League Users
							
						</h3>
						<button type="submit" class="btn btn-default pull-right" id="addUser"> <i class="fas fa-user-plus"></i> Add User</button>
					</div>
					<!-- ****** USERS Table  ****************************************************** -->
					<div class="box-body table-responsive">
						<table class="table">
							<tbody>
							<tr>
								<th style="width: 10px">#</th>
								<th>Name</th>
								<th>Role</th>
								<th>Status</th>
								<th>Team</th>
								<th>Email</th>
							</tr>
							
							@forelse ($users as $user)
									<tr>
										<td>
											<span class="label label-default">{{ $loop->iteration }}</span>
										</td>
										<td>
											@if( $user->primaryRole != "Team" )
												<a href="/admin/league/users/{{ $user->userID }}">{{ $user->nameFull }}</a>
											@endif
										</td>
										<td>
											{{ $user->primaryRole }}
										</td>
										<td>
											{{ $user->status }}
										</td>
										<td>
											{{ $user->teamName }}
										</td>
										<td>
											{{ $user->email }}
										</td>
									</tr>
							@empty
							<tr>
								<td colspan="5">
									🤭 No Users setup yet.
								</td>
							</tr>
							@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>

	</div>
	
	@component('modals.default')
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop

@section('js')
<script type="text/javascript">
	
	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
	var leagueID = "{{ config('trivialeague.league_id') }}";

	$('#modal-default .modal-body')
		.html( '<div class="row">\
					<div class="col-xs-6"><input type="text" id="newUserNameF" class="form-control" placeholder="First Name"></div>\
					<div class="col-xs-6"><input type="text" id="newUserNameL" class="form-control" placeholder="Last Name"></div>\
					<div class="col-xs-12"><div class="form-group">\
						<div class="radio"><label><input type="radio" name="role" id="role1" value="Guest" checked>Guest</label></div>\
						<div class="radio"><label><input type="radio" name="role" id="role2" value="Regular">Regular</label></div>\
						<div class="radio"><label><input type="radio" name="role" id="role3" value="Captain">Captain</label></div>\
						<div class="radio"><label><input type="radio" name="role" id="role4" value="Author">Author</label></div>\
						<div class="radio"><label><input type="radio" name="role" id="role5" value="Quiz Master">QM</label></div>\
					</div></div>\
				</div>');
	$('#addUser').click(function() 
	{
		$('#modal-default .modal-title').html("🙂 Add a new League member");
		$('#newUserNameF').val('');
		$('#newUserNameL').val('');
		$('#modal-default').modal('show');
	});
	
	// Add a new User
	$('#modal-default-btn-ok').click(function() 
	{
		var userNameF	= $("#newUserNameF").val();
		var userNameL	= $("#newUserNameL").val();
		var role		= $("input[name=role]:checked").val();
		if(userNameF == "") {
			return false;
		}
		
		$.ajax({
			method: "POST",
			url: "{{route('adminAjaxAddUser', null, false)}}",
			data: 
			{
				league_id: leagueID,
		        nameFirst: userNameF,
		        nameLast: userNameL,
		        nameFull: (userNameF + " " + userNameL).trim(),
		        dateJoined: "{{ date('Y-m-d') }}",
		        primaryRole: role,
				type: "Human",
				status: "Active"
			},
			cache: false
		})
		.done( function(response) 
		{
			window.location.href="/admin/league/users/" + response.id;
		})
		.fail(function(response, status, error) 
		{
			console.log(response);
			$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
			$('#modal-danger').modal('show');
		})
	});

</script>
@stop