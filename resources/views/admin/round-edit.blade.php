@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ - Submit Rounds' )

@section('content_header')
	<h1>Submit Rounds</h1>
	<ol class="breadcrumb">
        <li><a href="{{ route('adminSeasons') }}"><i class="fa fa-home"></i> Home</a></li>
	</ol>
@stop

@section('content')

<section class="content">
	<form id="roundsForm">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					
					<h3 class="box-title">Submit a Round</h3>
					
					<div class="form-group">
						<label for="roundTitle">Round Title</label>
						<input type="text" class="form-control" id="roundTitle" name="roundTitle" value="Test Round" placeholder="ie. Songs of Sound of Music">
					</div>
		
					<div class="form-group">
						<label>Description</label> <small>To be read aloud by the QM</small>
						<textarea class="form-control textarea-ckeditor" name="description" rows="3">Description of the Test Round</textarea>
					</div>
					
				</div>
			</div>
		</div>
		
			@for ($i = 0; $i < 10; $i++)
			<div class="col-md-6">
				<div class="box -collapsed-box">
					
					<div class="box-header with-border ">
						<h3 class="box-title">Question {{ $i+1 }}</h3>
						<div class="pull-right box-tools">
							<button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
							<i class="fa fa-minus"></i></button>
						</div>
					</div>
					
					<div class="box-body pad">
						<div class="form-group">
							<textarea class="form-control textarea-ckeditor" name="question[{{ $i }}]" id="Q{{ $i }}" rows="3" placeholder="Question {{ $i+1 }}">Test #{{ $i+1 }}</textarea>
						</div>
						<div class="form-group">
							<label for="answer{{ $i }}">Answer {{ $i+1 }}</label>
							<input type="email" class="form-control" id="answer{{ $i }}" name="answer[{{ $i }}]" placeholder="The answer to Q{{ $i+1 }}" value="Answer #{{ $i+1 }}">
						</div>
						<div class="form-group">
							<label for="notes{{ $i }}">Notes on Q{{ $i+1 }}</label>
							<input type="email" class="form-control" id="notes{{ $i }}" name="notes[{{ $i }}]" placeholder="Additional info about the answer" value="Note #{{ $i+1 }}">
						</div>
					</div>
					
				</div>
			</div>
			@endfor
			
			<button type="button" class="btn btn-block btn-primary btn-lg" id="btnSubmit">Submit this Round</button>
			
		</div>
	</div>
	</form>
</section>

@stop

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/overrides.css">
	<style type="text/css">
		
	</style>
@stop

@section('js')

	<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
	<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
	<script>
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
		var config = {
          toolbar : [ 
          		[ 'Bold', 'Italic', 'Underline', '-', 'RemoveFormat' ], 
          		[ 'Link', 'Unlink' ],
          		[ 'NumberedList', 'BulletedList' ],
          		[ 'Maximize' ]
          ],
          height : 120,
          skin : 'moono-lisa'
        };


		$('.textarea-ckeditor').ckeditor(config);
		
		@can('submit-rounds')
		
			$('#btnSubmit').click(function()
		    {
		        var formData= $("#roundsForm").serialize();
		        
		        $.ajax({
		            type : 'POST',
		            url: '{{ route("adminAjaxSubmitRound", $league->id, false) }}',
		            data: formData,
					cache: false,
		            success: function( msg ) {
		                console.log(msg);
		            },
		            error: function (msg) {
			            console.log(msg);
		            }
		        });
		    });
		    
	    @endcan
		
		
	</script>
@stop

