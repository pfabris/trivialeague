@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ Seasons')

@section('content_header')
	<h1>{{ $league->leagueName }} ➰ All Seasons</h1>
@stop

@section('content')

		<div class="row" id="seasons">
			
			<div class="col-md-12">
				
				<div class="box">
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header">
						<h3 class="box-title">
							<i class="fa fa-home"></i> 
							Seasons
						</h3>
						@can('edit-season')
						<div class="col-xs-2 pull-right">
							<button type="button" id="btnAddSeason" class="btn btn-block btn-primary ">
								<i class="fas fa-plus"></i> Add Season
							</button>
						</div>
						@endcan
					</div>
					<!-- /.box-header -->
				</div>
			</div>
				@forelse ($seasons as $season)
				
				@php
					$start 		= new DateTime($season->dateStart);
					$end		= new DateTime($season->dateEnd);
					$now		= new DateTime();
					$total 		= $start->diff($end);
					$current	= $start->diff($now);
					$totalDays	= $total->format('%R%a');
					$currentDays= $current->format('%R%a');
					$percentageComplete = round($currentDays/$totalDays,2)*100;
					$class		= ( ($start <= $now && $end>=$now) || $start > $now ? 'aqua' : 'info');
				@endphp
				<div class="col-md-6">
					<div class="info-box bg-{{$class}}">
						<span class="info-box-icon"><i class="ion-ios-calendar-outline"></i></span>
						
						<div class="info-box-content">
							<span class="info-box-text">
								{{$season->seasonName}}
							</span>
							<span class="info-box-number">
								{{ date('M j, Y', strtotime( $season->dateStart )) }} &mdash; 
								{{ date('M j, Y', strtotime( $season->dateEnd )) }}
							</span>
						
							<div class="progress">
								<div class="progress-bar" style="width: {{ $percentageComplete }}%"></div>
							</div>
							
							<span class="progress-description">
								
								<a href="{{ route('adminSeason', $season->id) }}" class="small-box-footer" style="color:white" >
									Game Nights <i class="fa fa-arrow-circle-right"></i>
								</a>
							</span>
						</div>
						<!-- /.info-box-content -->
					</div>
				</div>
				@empty
									
					<h3>😞 No Seasons Setup yet...</h3>
									
				@endforelse

		</div>	
		<!-- /.col-md-6 -->
	</div>
	<!-- /.row -->
	
	<div class="modal fade" id="modal-default-addSeason">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Add a Season</h4>
				</div>
				<div class="modal-body">
					<div class="form-horizontal">
						
						<div class="form-group">
							<label for="newSeasonName" class="col-sm-3 control-label">Season Name</label>
							<div class="col-sm-9">
								<input type="text" id="newSeasonName" name="newSeasonName" class="form-control" placeholder="Winter {{ date("Y") }}">
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputDateStart" class="col-sm-6 control-label">First Game</label>
							<div class="col-sm-3">
								<input type="text" id="inputDateStart" name="inputDateStart" class="form-control" placeholder="YYYY-MM-DD">
							</div>
						</div>
						
						
						<div class="form-group">
							<label for="inputDateSkip1" class="col-sm-6 control-label">Skip the following holidays</label>
							<div class="col-sm-3">
								<input type="text" id="inputDateSkip1" name="inputDateSkip1" class="form-control" placeholder="YYYY-MM-DD">
							</div>
							<div class="col-sm-3">
								<input type="text" id="inputDateSkip2" name="inputDateSkip2" class="form-control" placeholder="YYYY-MM-DD">
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputDateEnd" class="col-sm-6 control-label">Last Game</label>
							<div class="col-sm-3">
								<input type="text" id="inputDateEnd" name="inputDateEnd" class="form-control" placeholder="YYYY-MM-DD">
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<button type="button" id="modal-default-addSeason-btn-submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	@component('modals.danger',['modal_id' => 'modal-danger'])
	@endcomponent

@stop

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="/css/overrides.css">
	<style type="text/css" media="screen">
		.info-box-icon {
			height: 94px;
		}
	</style>
@stop

@section('js')
	<script type="text/javascript">
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
		$(document).ready(function(){
		
		$('#btnAddSeason').click(function() 
		{
			$('#modal-default-addSeason').modal('show');
		});

		var container=$('#modal-default-addSeason');
		var options={
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		};
		$('#inputDateStart').datepicker(options);
		$('#inputDateEnd').datepicker(options);
		$('#inputDateSkip1').datepicker(options);
		$('#inputDateSkip2').datepicker(options);
		
		
		// Add a new Season
		$('#modal-default-addSeason-btn-submit').click(function() 
		{
			var leagueID	= "{{ config('trivialeague.league_id') }}";
			var seasonName	= $("#newSeasonName").val();
			var startDate	= $("#inputDateStart").val();
			var endDate		= $("#inputDateEnd").val();
			var skipDate1	= $("#inputDateSkip1").val();
			var skipDate2	= $("#inputDateSkip2").val();
			
			if(seasonName == "" || startDate == "" ){
				alert("You must enter a Season Name, a First Game and a Last Game.")
				return false;
			}
			
			$.ajax({
				method: "POST",
				url: "{{route('adminAjaxAddSeason')}}",
				data: 
				{
					league_id: leagueID,
					seasonName: seasonName,
					dateStart: startDate,
					dateEnd: endDate,
					dateSkip1: skipDate1,
					dateSkip2: skipDate2
				},
				cache: false
			})
			.done( function(response) {
				$('#modal-default').modal('hide');
				location.reload();
			})
			.fail(function(response, status, error) 
			{
				console.log(response);
				$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
				$('#modal-danger').modal('show');
			})
		});
		
		})	
		
	</script>
@stop