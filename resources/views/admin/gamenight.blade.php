@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ '. $season->seasonName . '  〰️ GameNight ' .  $gameNight->gameNumber )

@section('content_header')
	<h1>
		{{ date('M j, Y', strtotime( $gameNight->date )) }} GameNight {{ $gameNight->gameNumber }}
	</h1>

	<ol class="breadcrumb">
		<li><a href="{{ route('adminSeasons') }}"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="{{ route('adminSeason', ['id' => $season->id ]) }}"><i class="fa fa-navicon"></i> {{ $season->seasonName }} </a></li>
		<li class="active"><i class="fa fa-gamepad"></i> {{ date('M j', strtotime( $gameNight->date )) }}</a> </li>
	</ol>

@stop

@section('content')



	@forelse ($games as $game)
	
			@php
				if( $game->gamePlayersCount==0 ) 
				{
					$classColor	= "bg-purple"; 
					$classIcon	= "fa-cogs"; 
					$href		= route('adminGameSetup', $game->gameID, false);
					$hrefText	= "Choose Players";
				} else {
					$classColor	= "bg-green-active"; 
					$classIcon	= "fa-edit"; 
					$href		= route('adminScorecard', $game->gameID, false);
					$hrefText	= "Go to ScoreCard";
				}
				$existsBanner = Storage::disk('public')->exists('banners/locations/'. $game->banner);
				$existsAvatar = Storage::disk('public')->exists('avatars/locations/'. $game->avatar);
				
				if (!$existsBanner || $game->banner==null) {
					$banner = 'default.jpg';
				} else {
					$banner = $game->banner;
				}
				if (!$existsAvatar|| $game->avatar==null) {
					$avatar = 'default.jpg';
				} else {
					$avatar = $game->avatar;
				}
			@endphp
			
			@if ($loop->index % 3 == 0)	
			<div class="row">
			@endif
				
				<div class="col-md-4">
					<!-- Widget: user widget style 1 -->
					<div class="box box-widget widget-user">
						<!-- Add the bg color to the header using any of the bg-* classes -->
		
						<div class="widget-user-header bg-black" style="background: url({{ asset('storage/banners/locations/'. $banner) }}) center center;">
							<h3 class="widget-user-username">{{ $game->locationName }}</h3>
		
							<h5 class="widget-user-desc"></h5>
						</div>
		
						<div class="widget-user-image"><img class="img-circle" src="{{ asset('storage/avatars/locations/'. $avatar) }}" alt="User Avatar"></div>
		
						<div class="box-footer">
							<div class="row">
								<div class="col-sm-6 border-right">
									<div class="description-block">
										<h5 class="description-header">{{ $game->team1_Name }}</h5>
											<span class="description-text"></span>
									</div><!-- /.description-block -->
								</div><!-- /.col -->
		
									
								<div class="col-sm-6">
									<div class="description-block">
										<h5 class="description-header">{{ $game->team2_Name }}</h5>
											<span class="description-text"></span>
									</div><!-- /.description-block -->
								</div><!-- /.col -->
							</div><!-- /.row -->
							<div class="row">
								<div class="col-sm-12">
									<div class="description-block">
										<h5 class="description-header"></h5>
										<span class="description-text">
											
											@can('edit-scorecard')
											<a href="{{ $href }}" class="btn btn-sm {{$classColor}}" > 
												{{$hrefText}}
												<i class="fa fa-arrow-circle-right"></i>
											</a>
											@endcan
											
										</span>
									</div><!-- /.description-block -->
								</div><!-- /.col -->
							</div><!-- /.row -->
						</div>
					</div><!-- /.widget-user -->
				</div>
			
			@if ($loop->index % 3 == 2 || $loop->last)	
			</div>
			<!-- /.row -->
			@endif
	
		
			
		@empty
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">
								☹️ No Matches Setup yet... 
							</h3>
							<br><br>
							<a href="{{ route('adminGameNightSetup', $gameNight->id, false) }}" class="btn btn-block btn-warning btn"> 
								Setup Matches <i class="fa fa-edit"></i>
							</a>
							
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			
		@endforelse
	


	<div class="row">
	
		<div class="col-md-12">
			
			@if( count($rounds) == 0 )
			
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						😞 No rounds setup yet.
					</h3>
				</div>
			</div>
			
			@else
			<div class="box">
				
				<div class="box-header">
					<h3 class="box-title">
				  		Question Rounds
				  	</h3>
				</div>
				<!-- /.box-header -->
				
				<div class="box-body table-responsive">
					
					<form id="roundsForm">
						<input type="hidden" name="gamenight_id" value="{{$gameNight->id}}">
						<table class="table table-striped ">
							<tbody>
								<tr>
									<th class="number-header">
										#
									</th>
									<th>
										Category Title
									</th>
									<th>
										Description
									</th>
									
								</tr>
								
								@forelse($rounds as $round)
								
								<tr>
									<td>
										{{$round->roundNumber}}						
									</td>
									<td>
										<input class="form-control" type="text" name="title[{{$round->id}}]" id="title_{{$round->id}}" value="{{$round->title}}">
									</td>
									<td>
										<input class="form-control" type="text" name="description[{{$round->id}}]" id="description_{{$round->id}}" value="{{$round->description}}">
									</td>
								</tr>
								
								@empty
								<tr>
									<td colspan="3">
										😞 No rounds setup yet.
									</td>
								</tr>
								@endforelse
								
								@if(count($rounds))
									@can('edit-rounds')
									<tr>
										<td colspan="3">
											<button type="button" class="btn btn-block btn-success" id="btnSave">Save</button>
										</td>
									</tr>
									@endcan
								@endif
								
							</tbody>
						</table>
	
					</form>
					
				</div>
				<!-- /.box-body -->
			
			</div>
			<!-- /.box -->
			@endif
			
		</div>
		<!-- /.col -->
		
	</div>
	<!-- /.row -->
	
@stop

@section('footer')
@stop

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/overrides.css">
	<style>
	   .widget-user-username {
		   background-color:rgba(0, 0, 0, 0.3);
		   padding-left:6px;
		   border-radius: 2px;
	   }
	</style>
@stop

@section('js')
<script type="text/javascript">
	$(document).ready(function() 
	{
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
		@can('edit-rounds')
		$('#btnSave').click(function()
	    {
	        var formData= $("#roundsForm").serialize();
	        
	        $.ajax({
	            type : 'PATCH',
	            url: '{{ route("adminAjaxUpdateRounds", $gameNight->id, false) }}',
	            data: formData,
				cache: false,
	            success: function( msg ) {
	                console.log(msg);
	            },
	            error: function (msg) {
		            console.log(msg);
	            }
	        });
	    });
	    @endcan
	});
</script>
@stop