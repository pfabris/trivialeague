@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ '. $season->seasonName . ' 〰️ GameNight ' . $gameNight->gameNumber . " " . date('F j', strtotime( $gameNight->date )) . " - Setup Game @ " . $location->locationName )

@section('content_header')
	<h1>GameNight {{ $gameNight->gameNumber }} @ {{ $location->locationName }}</h1>
	<ol class="breadcrumb">
        <li><a href="{{ route('adminSeasons') }}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('adminSeason', ['id' => $season->id ]) }}"><i class="fa fa-navicon"></i> {{ $season->seasonName }} </a></li>
        <li><a href="{{ route('adminGameNight', ['id' => $gameNight->id ]) }}"><i class="fa fa-gamepad"></i> {{ date('M j', strtotime( $gameNight->date )) }}</a> </li>
		<li class="active"><i class="fa fa-soccer-ball-o"></i> {{ $location->locationName }} </li>
	</ol>
@stop

@section('content')

	
	<div class="box box-solid">
		
		
		
		<div class="box-body">
			
			<div class="row">
			
				<div id="team1Container" class="col-xs-6 col-md-3 col-lg-3">
					<div class="box box-info">
						
		                <div class="box-header with-border">

							<div class="form-group">
								<label for="choose-team1">Visiting Team</label>
								<select id="choose-team1" class="form-control">
									<option value="" selected="selected"></option>
									@forelse($teams as $team)
										<option value="{{ $team->team_id }}"
										@if($team->team_id == $teams[0]->team_id) 
											selected=selected
										@endif
										>{{ $team->teamName }}</option>
									@empty
									@endforelse
								</select>
							</div>							
							
		                </div>
		                <!-- /.box-header -->
		                			
						<div class="box-body no-padding">
							<ul id="team1" class="sortable">
								
							</ul>
							<ul id="team1TeamRole" class="sortable">
								
							</ul>
		                </div>
		                <!-- /.box-body -->
		                
		                <div class="box-footer text-center">
			                <button id="addUserTeam1" type="button" class="btn btn-primary">
			                	<i class="fas fa-user-plus"></i> New Player</button>
		                </div>
		                <!-- /.box-footer -->
		                
					</div>
				</div>
				
				<div id="team1SelectedPlayersContainer" class="col-xs-6 col-md-3 col-lg-3">
					<div class="box box-info">
						<div class="box-header with-border">
							<label for="">Tonight's Players</label>
						
							<div class="box-tools pull-right">
								<span class="label label-danger"></span>
							</div>
						</div>
		                <!-- /.box-header -->
			
		                <div class="box-body no-padding">
							<ul id="team1Players" class="sortable">
								<p class="small text-muted"><i class="fas fa-bullseye"></i> Drag Team 1 players here</p>
							</ul>
							<!-- /.players-list -->
		                </div>
		                <!-- /.box-body -->
					</div>
				</div>			
	
			<div class="clearfix visible-xs"></div>
		
				<div id="team2Container" class="col-xs-6 col-md-3 col-lg-3">
					<div class="box box-danger">
		                <div class="box-header with-border">
							<div class="form-group">
								<label for="choose-team1">Home Team</label>
								<select id="choose-team2" class="form-control">
									<option value="" selected="selected"></option>
									@forelse($teams as $team)
										<option value="{{ $team->team_id }}"
										@if($team->team_id == $teams[1]->team_id) 
											selected=selected
										@endif
										>{{ $team->teamName }}</option>
									@empty
									@endforelse
								</select>
							</div>
						</div>
		                <!-- /.box-header -->					
						<div class="box-body no-padding">
							<ul id="team2" class="sortable">
								
							</ul>
							<ul id="team2TeamRole" class="sortable">
								
							</ul>
		                </div>
		                <!-- /.box-body -->
		                <div class="box-footer text-center">
			                <button id="addUserTeam2" type="button" class="btn btn-primary">
				                <i class="fas fa-user-plus"></i> New Player</button>
		                </div>
		                <!-- /.box-footer -->
					</div>				
				</div>

				<div id="team2SelectedPlayersContainer" class="col-xs-6 col-md-3 col-lg-3">
					<div class="box box-danger">
						<div class="box-header with-border">
							<label for="">Tonight's Players</label>
						
							<div class="box-tools pull-right">
								<span class="label label-danger"></span>
							</div>
						</div>
		                <!-- /.box-header -->
			
		                <div class="box-body no-padding">
							<ul id="team2Players" class="sortable">
								<p class="small text-muted"><i class="fas fa-bullseye"></i> Drag Team 2 players here</p>
							</ul>
							<!-- /.players-list -->
		                </div>
		                <!-- /.box-body -->
		                
					</div>
				</div>			
				
				
			</div>
		</div>
	</div>
	
	<div class="box box-solid">
		<div class="box-header">
			<div class="col-md-12">
				<button id="btn_setupGame" type="button" disabled="disabled" class="btn btn-block btn-success btn-lg"> <i class="fa fa-fw fa-ban"></i> (Add 5 players per team first)</button>
			</div>
		</div>
	</div>
	
	@component('modals.default-sm',['modal_id' => 'modal-default'])
	@endcomponent
	@component('modals.danger',['modal_id' => 'modal-danger'])
	@endcomponent

@stop

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="/css/overrides.css">
	<style type="text/css">
		
		ul.sortable  {
			list-style: none;
			padding: 6px;
		}
		.sortable li {
			background: #eaeaea;
			margin: 0px 0px 4px 0px;
			padding: 6px;
			cursor: move;
			cursor: -webkit-grabbing;  
		}
		.sortable-ghost {
			opacity: .2;
		}
		ul#team1Players, ul#team2Players {
			min-height: 240px;
		}
		.small { font-size: 9; }
		#team1SelectedPlayersContainer div, #team2SelectedPlayersContainer div { display: none; }
		#addUserTeam1, #addUserTeam2 { display:none; }
		.box.box-solid > .box-header .btn:hover, .box.box-solid > .box-header a:hover {
		    background: #20895e;
		}
	</style>
@stop

@section('js')
	<script>
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
		var playersPerTeam = 5;
		var playersPerGame = 10;

		var leagueID	 = "{{ config('trivialeague.league_id') }}";
		var game_ID	 	 = "{{ $gameID }}";
		var gameteam1_ID = "{{ $teams[0]->gameteam_id }}";
		var gameteam2_ID = "{{ $teams[1]->gameteam_id }}";
		var team1_ID 	 = "{{ $teams[0]->team_id }}";
		var team2_ID 	 = "{{ $teams[1]->team_id }}";
		var team1_players = [];
		var team2_players = [];
		localStorage.setItem("team1_players" , "");
		localStorage.setItem("team2_players" , "");
		

		var liPrototype = '<li data-id="__id__"><i class="fa fa-user"></i> __name__ <span class="label label-success pull-right">__role__</span></li>';		
		
		var team1List = Sortable.create(team1, 
		{
			sort: false,
			animation: 200,
			ghostClass: 'sortable-ghost',
			group: {
				name: 'team1_players',
				pull: true,
				put: true
			}
		});
		var team2List = Sortable.create(team2, 
		{
			sort: false,
			animation: 200,
			ghostClass: 'sortable-ghost',
			group: {
				name: 'team2_players',
				pull: true,
				put: true
			}
		});		

		Sortable.create(team1TeamRole, 
		{
			sort: false,
			animation: 200,
			ghostClass: 'sortable-ghost',
			group: {
				name: 'team1_players',
				pull: 'clone',
				put: true
			}	
		});
		Sortable.create(team2TeamRole, 
		{
			sort: false,
			animation: 200,
			ghostClass: 'sortable-ghost',
			group: {
				name: 'team2_players',
				pull: 'clone',
				put: true
			}	
		});
		
		var team1PlayersSortable = Sortable.create(team1Players, 
		{
			animation: 100,
			ghostClass: 'sortable-ghost',
			group: {
				name:'team1_players',
				pull: true,
				put: function (to) {
					return to.el.children.length < playersPerTeam;
				}
			},
			onSort: function (/**Event*/evt)  // Called by any change to the list (add / update / remove)
			{
				var itemEl = evt.item;  // dragged HTMLElement
				evt.to;    				// target list
				evt.from;  				// previous list
				evt.oldIndex;  			// element's old index within old parent
				evt.newIndex;  			// element's new index within new parent
				//console.log(itemEl);
				team1PlayersSortable.save();
				$("#team1Players p").remove();
			},
			store: {
				get: function (sortable) 
				{
					var order = localStorage.getItem(sortable.options.group.name);
					//console.log('get:', sortable.toArray(), order);
					return order ? order.split('|') : [];
				},
				set: function (sortable) 
				{
					var order = sortable.toArray();
					localStorage.setItem(sortable.options.group.name, order.join('|'));
					team1_players = order;
					enable_btn_setupGame();
					//console.log(team1_players);
				}
			}
		});
		var team2PlayersSortable = Sortable.create(team2Players, 
		{
			animation: 100,
			ghostClass: 'sortable-ghost',
			group: {
				name:'team2_players',
				pull: true,
				put: function (to) {
					return to.el.children.length < playersPerTeam;
				}
			},
			onSort: function (/**Event*/evt)  // Called by any change to the list (add / update / remove)
			{
				var itemEl = evt.item;  // dragged HTMLElement
				evt.to;    				// target list
				evt.from;  				// previous list
				evt.oldIndex;  			// element's old index within old parent
				evt.newIndex;  			// element's new index within new parent
				//console.log(itemEl);
				team2PlayersSortable.save();
				$("#team2Players p").remove();
			},
			store: {
				get: function (sortable) 
				{
					var order = localStorage.getItem(sortable.options.group.name);
					//console.log('get:', sortable.toArray(), order);
					return order ? order.split('|') : [];
				},
				set: function (sortable) 
				{
					var order = sortable.toArray();
					localStorage.setItem(sortable.options.group.name, order.join('|'));
					team2_players = order;
					enable_btn_setupGame();
					//console.log(team2_players);
				}
			}
		});
	
		
		
		$('#modal-default .modal-body')
			.html( '<div class="row">\
						<div class="col-xs-6"><input type="text" id="newUserNameF" class="form-control" placeholder="First Name"></div>\
						<div class="col-xs-6"><input type="text" id="newUserNameL" class="form-control" placeholder="Last Name"></div>\
						<div class="col-xs-12"><div class="form-group">\
							<div class="radio"><label><input type="radio" name="role" id="role1" value="Guest" checked>Guest</label></div>\
							<div class="radio"><label><input type="radio" name="role" id="role2" value="Regular">Regular</label></div>\
						</div></div>\
					</div>');
		$('#addUserTeam1').click(function() {
			$('#modal-default .modal-title').html("🙂 Add someone to Team 1");
			$('#modal-default-btn-ok').data('teamID',team1_ID);
			$('#modal-default-btn-ok').data('teamNumber', 1);
			$('#newUserNameF').val('');
			$('#newUserNameL').val('');
			$('#modal-default').modal('show');
		});
		$('#addUserTeam2').click(function() {
			$('#modal-default .modal-title').html("🙂 Add someone to Team 2");
			$('#modal-default-btn-ok').data('teamID',team2_ID);
			$('#modal-default-btn-ok').data('teamNumber', 2);			
			$('#newUserNameF').val('');
			$('#newUserNameL').val('');
			$('#modal-default').modal('show');
		});
		
		// Add a new Guest User to a Team
		$('#modal-default-btn-ok').click(function() 
		{
			var teamID 		= $(this).data("teamID");
			var teamNumber 	= $(this).data("teamNumber");
			var userNameF	= $("#newUserNameF").val();
			var userNameL	= $("#newUserNameL").val();
			var role		= $("input[name=role]:checked").val();
			if(userNameF == "") {
				return false;
			}
			
			$.ajax({
				method: "POST",
				url: "{{route('adminAjaxAddUser')}}",
				data: 
				{
					team_id: teamID,
					league_id: leagueID,
			        nameFirst: userNameF,
			        nameLast: userNameL,
			        nameFull: (userNameF + " " + userNameL).trim(),
			        dateJoined: "{{ date('Y-m-d') }}",
			        primaryRole: role,
					type: "Human",
					status: "Active"
				},
				cache: false
			})
			.done( function(response) {
				var thisLI = formatLI( response.id, (userNameF + " "+ userNameL).trim(), "Guest" );
				$('ul#team'+teamNumber).append(thisLI);
				$('#modal-default').modal('hide');
			})
			.fail(function(response, status, error) 
			{
				console.log(response);
				$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
				$('#modal-danger').modal('show');
			})
		});
		
		
		
		$('#choose-team1').on('change', function () 
		{
			team1_ID = this.value;
			$("#team1SelectedPlayersContainer div").show();
			$("#addUserTeam1").show();
			
			// Fetch the TeamMates
			$.get( '/ajax/admin/teams/'+ team1_ID +'/users' , function(data)
			{
				// iterate over elements in the data response and build the <li> elements, one for each team mate
				// replace the existing elements
				$("#team1").html("");
				$("#team1TeamRole").html("");
				
				jQuery.each(data.data, function(i, val) 
				{
					var thisLI = formatLI( val.id, val.nameFull,val.primaryRole );
					if(val.primaryRole == "Team"){
						$("#team1TeamRole").append(thisLI);
					}
					else {
						$("#team1").append(thisLI);	
					}
				});
			});
		});
		
		$('#choose-team2').on('change', function () 
		{
			team2_ID = this.value;
			$("#team2SelectedPlayersContainer div").show();
			$("#addUserTeam2").show();
			
			// Fetch the TeamMates
			$.get('/ajax/admin/teams/' + team2_ID + '/users' , function(data)
			{
				// iterate over elements in the data response and build the <li> elements, one for each team mate
				// replace the existing elements
				$("#team2").html("");
				$("#team2TeamRole").html("");
				
				jQuery.each(data.data, function(i, val) 
				{
					var thisLI = formatLI( val.id, val.nameFull,val.primaryRole );
					if(val.primaryRole == "Team"){
						$("#team2TeamRole").append(thisLI);
					}
					else {
						$("#team2").append(thisLI);	
					}
				});
			});
		});
		
		
		
		function enable_btn_setupGame()
		{
			//console.log(team1_players.length + team2_players.length);
			if(team1_players.length + team2_players.length == playersPerGame ) 
			{
				$('#btn_setupGame').prop('disabled', false).html('<i class="fa fa-fw fa-play"></i> Play');
			}
			else
			{
				$('#btn_setupGame').prop('disabled', true).html('<i class="fa fa-fw fa-ban"></i> (Add 5 players per team first)');
			}
		}
		
		
		$('#btn_setupGame').click(function()
		{
			//Create GameTeams, GameTeamRounds and Scores for Teams 1 and 2.
			$.post(
				"{{route('adminAjaxGameSetup')}}", 
				{ 
					game_id : game_ID, 
					gameteam1_ID: gameteam1_ID, 
					team1_ID : team1_ID, 
					team1_players : team1_players, 
					gameteam2_ID: gameteam2_ID, 
					team2_ID : team2_ID, 
					team2_players: team2_players 
				}, 
				function( response, status ) {null}
			)
			.done(function() {
				window.location.href="{{route( 'adminSeason', $season->id)}}";
			})
			.fail(function(response, status, error) {
				console.log(response);
				$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
				$('#modal-danger').modal('show');
			})
			
			
		});
		
		function formatLI (id, name, role) 
		{
			var thisLI = liPrototype.replace("__id__", id);
			thisLI = thisLI.replace("__name__", name);
			thisLI = thisLI.replace("__role__", role);
			if(role == "Team"){
				thisLI = thisLI.replace("label-success","label-danger");
			} else if(role == "Captain"){
				thisLI = thisLI.replace("label-success","label-info");
			} else if(role == "Guest"){
				thisLI = thisLI.replace("label-success","label-warning");
			}
			return thisLI;
		}
		
		// Load the Player Pickers when the Page is loaded.
		$( document ).ready(function() {
			$( "#choose-team1" ).trigger( "change" );
			$( "#choose-team2" ).trigger( "change" );
		});
		
		
	</script>
@stop