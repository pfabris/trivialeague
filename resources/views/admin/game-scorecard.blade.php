@extends('adminlte::page')

@section('title', $league->leagueName . ' ➰ '. $season->seasonName . ' 〰️ GameNight ' . $gameNight->gameNumber . " " . date('F j', strtotime( $gameNight->date )) . " @ " . $location->locationName . " Scorecard")

@section('content_header')
	
	<h1>Scorecard</h1>
	
	<ol class="breadcrumb">
        <li><a href="{{ route('adminSeasons') }}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('adminSeason', ['id' => $season->id ]) }}"><i class="fa fa-navicon"></i> {{ $season->seasonName }} </a></li>
        <li><a href="{{ route('adminGameNight', ['id' => $gameNight->id ]) }}"><i class="fa fa-gamepad"></i> {{ date('M j', strtotime( $gameNight->date )) }}</a> </li>
		<li class="active"><i class="fa fa-soccer-ball-o"></i> {{ $location->locationName }} </li>
	</ol>
	 
@stop

@section('content')

	
	<!-- ######################  GAME SCORECARD  ############################################################################ -->
	<div class="row" >
			
			<div class="col-md-12">
			
				<div class="box">	
					
					@php

						$existsAvatarT1 = Storage::disk('public')->exists('avatars/teams/'. $gameTeamResults[0]->avatar);
						$existsAvatarT2 = Storage::disk('public')->exists('avatars/teams/'. $gameTeamResults[1]->avatar);
						if (!$existsAvatarT1 || $gameTeamResults[0]->avatar==null ) {
							$avatarT1 = 'default.jpg';
						} else {
							$avatarT1 = $gameTeamResults[0]->avatar;
						}
						if (!$existsAvatarT2 || $gameTeamResults[1]->avatar==null ) {
							$avatarT2 = 'default.jpg';
						} else {
							$avatarT2 = $gameTeamResults[1]->avatar;
						}
					@endphp
					
					<div class="box-body table-responsive">
						<table class="table table-bordered" id="scorecard">
							<tbody>
								
								<!-- TEAM HEADER -->
								<tr>
									<th colspan="2">
										{{ date('M j', strtotime( $gameTeamResults[0]->date )) }} @ {{ $gameTeamResults[0]->locationName }}
										@if($gameIsOver)<br><i class="fas fa-lock" style="color:green;"></i> Finished @endif
									</th>
									
									<!-- TEAM 1 -->
									<th colspan="6" class="teamCell">
										<img class="profile-user-img img-responsive img-circle" src="{{ asset('storage/avatars/teams/'. $avatarT1) }}" alt="Team Avatar" height="50">
										<span id="t1_TeamName">
											<i id="t1_trophy" class="fa fa-fw fa-circle"></i> {{ $gameTeamResults[0]->teamName }}
										</span>
									</th>
									
									<th>
										<a class="btn btn-app btn-sm" id="swapTeamPositions">
											<i class="fa fa-exchange-alt"></i> Swap
										</a>
									</th>
									
									<!-- TEAM 2 -->
									<th colspan="6" class="teamCell">
										<img class="profile-user-img img-responsive img-circle" src="{{ asset('storage/avatars/teams/'. $avatarT2) }}" alt="Team Avatar" height="50">
										<span id="t2_TeamName">
											<i id="t2_trophy" class="fa fa-fw fa-circle"></i> {{ $gameTeamResults[1]->teamName }}
										</span>
									</th>
								</tr>
								
								
								<!-- PLAYER HEADER -->
								<tr>
									<th style="width: 10px">#</th>
									<th>Round</th>
									
									
									@forelse ($playersTeam1 as $player)
										<th id="seat{{ $loop->iteration }}" class="playerCell"><div><a class="gamePlayer" id="gamePlayer_{{$player->gamePlayerID}}_{{$team1->gameteam_id}}">{{ $player->nameFull }}</a></div></th>
									@empty
										<th colspan="5"></th>
									@endforelse
									<th class="playerCell" style="background-color: #e1e1e1;">Total</th>
									
									<th class="lockColumn">
										<i class="fa fa-lock"></i>
									</th>
									
									@forelse ($playersTeam2 as $player)
										<th id="seat{{ $loop->iteration + 5 }}" class="playerCell"><div><a class="gamePlayer" id="gamePlayer_{{$player->gamePlayerID}}_{{$team2->gameteam_id}}">{{ $player->nameFull }}</a></div></th>
									@empty
										<th colspan="5"></th>
									@endforelse
									
									<th class="playerCell" style="background-color: #e1e1e1;">Total</th>
								</tr>



								<!-- SCORES GRID -->
								@php 
									$i=0; // Special Counter for gameTeamRound, incremented on 5th and 10th iterations
								@endphp
								@forelse ($scores as $result)
									
									@if ($loop->index % 10 == 0)
									<tr>
									@endif
								
										@if ($loop->index % 10 == 0)
											<!-- ROUND TITLE -->
											<td class="roundTitle">
												{{ $result->roundNumber }}
											</td>
											
											<td class="roundTitle">
												{{ $result->title }}
											</td>
										@endif
										
										<!-- SCORE CELL -->
										<td 
											@if ($result->score == 2) class="scoreCell noselect bg-success"
											@elseif ($result->score == 1) class="scoreCell noselect bg-warning"
											@elseif ($result->score == 0) class="scoreCell noselect bg-default"
											@elseif ($result->score == "-1") class="scoreCell noselect bg-danger"
											@else class="scoreCell noselect bg-secondary"
											@endif
											id="{{$result->scoreID}}" data-roundnumber="{{$result->roundNumber}}" data-teamposition="{{$result->teamPosition}}" data-gameplayer="{{$result->gamePlayerID}}" data-gameteam="{{$result->gameTeamID}}"
										>
											
											@if( $result->score !== null ) {{ $result->score }} @endif
											
										</td>
										
										@if ($loop->index % 10 == 4 || $loop->index % 10 == 9)
											<!-- ROUND TOTAL CELL for TEAM -->
											<td id="roundTotal_t{{ $gameTeamRounds[$i]->teamPosition }}_{{ $gameTeamRounds[$i]->roundNumber }}" class="round_total">
												
												{{ $gameTeamRounds[$i]->totalScore }}
												
											</td>
											@php 
												$i++;
											@endphp
										@endif
										
										@if ($loop->index % 10 == 4 )
											<!-- GUTTER -->
											<td class="gutter lockColumn">
												<input id="lock_round_{{$result->roundNumber}}" data-roundnumber="{{$result->roundNumber}}" type="checkbox" class="checkbox_lockRound" value="1">
											</td>
										@endif
										
									@if ($loop->index % 10 == 9)
									</tr>
									@endif

								@empty
									<tr><td colspan="15"><p>No Results yet ☹️</td></tr>
								@endforelse
							
							
								
								<!-- BOTTOM ROW - TEAM PLAYER TOTALS AND TEAM TOTAL -->
								<tr>
									<td colspan="2"></td>
									
									@forelse ($playersTeam1 as $player)
										<!-- TEAM 1 -->
										<td style="text-align:center" id="{{$player->gamePlayerID}}" class="round_total">
											{{ $player->deuceCount }}
										</td>
									@empty
										<td colspan="5"></td>
									@endforelse
									
									<!-- TEAM 1 TOTAL -->
									<td
										@if ($gameTeamResults[0]->gameTotalScore > $gameTeamResults[1]->gameTotalScore) class="bg-green round_total"
										@elseif ($gameTeamResults[0]->gameTotalScore < $gameTeamResults[1]->gameTotalScore) class="bg-red round_total"
										@else class="bg-default round_total"
										@endif
										id="t1_TotalScore"
									>
										{{ $gameTeamResults[0]->gameTotalScore }}
										
									</td>
									
									
									<td style="width: 1px;"></td>
									
									
									@forelse ($playersTeam2 as $player)
									<!-- TEAM 2 -->
									<td style="text-align:center" id="{{$player->gamePlayerID}}" class="round_total">
										{{ $player->deuceCount }}
									</td>
									@empty
									@endforelse
									
									<!-- TEAM 2 TOTAL -->
									<td style="text-align:center"
										@if ($gameTeamResults[0]->gameTotalScore < $gameTeamResults[1]->gameTotalScore) class="bg-green round_total"
										@elseif ($gameTeamResults[0]->gameTotalScore > $gameTeamResults[1]->gameTotalScore) class="bg-red round_total"
										@else class="bg-default round_total"
										@endif
										id="t2_TotalScore"
									>
										{{ $gameTeamResults[1]->gameTotalScore }}
									</td>
									
								</tr>
							
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			
			</div>	
			<!-- /.col-md-12 -->
	</div>
	<!-- /.row GAME SCORECARD -->
	
	
	<div class="row">
		<div class="box box-solid">
			
			<div class="box-header">
				<div class="col-md-12">
					@if($gameIsOver)
					<h3>Game is Over</h3>
					<p>No further scoring is allowed</p>
					@else
					<button id="btn_finishGame" type="button"  class="btn btn-block btn-success btn-lg"> <i class="fas fa-check-circle"></i> Finish Game</button>
					@endif
				</div>
			</div>
			<div class="box-body">
				
				@if($gameIsOver)
				<div class="col-md-2">
					<button id="btn_unlockGame" type="button"  class="btn btn-block btn-warning btn-sm pull-right"> <i class="fas fa-unlock"></i> Unlock Game</button>
				</div>
				<div class="col-md-8">
				</div>
				@else
				<div class="col-md-10">
				</div>
				@endif
				<div class="col-md-2">
					<button id="btn_resetGame" type="button"  class="btn btn-block btn-danger btn-sm pull-left"> <i class="fas fa-trash"></i> Reset Game</button>
				</div>
			</div>
			
		</div>
	</div>
	
	
	@component('modals.default',['modal_id' => 'modal-default'])
	@endcomponent
	@component('modals.default',['modal_id' => 'modal-default-reset'])
	@endcomponent
	@component('modals.danger',['modal_id' => 'modal-danger'])
	@endcomponent

@stop

@section('css')
	<style type="text/css">
		.scoreCell	 { cursor: pointer; text-align:center; min-height: 45px;}
		.teamCell    { font-size: large; }
		.fa-circle   { display:none; }
		.playerCell  { text-align: center; }
		.round_total { font-weight:bold;font-size: large;text-align: center;}
		.gutter 	 { background-color: #f3f3f3;}
		.noselect {
		  -webkit-touch-callout: none; /* iOS Safari */
		    -webkit-user-select: none; /* Safari */
		     -khtml-user-select: none; /* Konqueror HTML */
		       -moz-user-select: none; /* Firefox */
		        -ms-user-select: none; /* Internet Explorer/Edge */
		            user-select: none; /* Non-prefixed version, currently  supported by Chrome and Opera */
		}
		.profile-user-img {
			width: 50px;
			padding: 0;
			float: left;
			margin-right: 6px;
		}
		th.playerCell {
			white-space: nowrap;
			min-width: 60px;
		}
		th.playerCell a {
			cursor: pointer;
		}
		.box.box-solid > .box-header .btn:hover, .box.box-solid > .box-header a:hover {
		    background: #20895e;
		}
		.table > tbody > tr > th, .table > tbody > tr > td.scoreCell, .table > tbody > tr > td.roundTitle, .table > tbody > tr > td.round_total
		{
			line-height: 1.3;
		}

	</style>
@stop

@section('js')
		
	<script>
		$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
		
		var leagueID 	 	= "{{ $league->id }}";

		var playersPerGame = {{ $league->seatsPerGame }};
		var playersPerTeam = (playersPerGame/2);
		
		var game_ID	 	 = "{{ $gameID }}";
		var gameteam1_ID = "{{ $team1->gameteam_id }}";
		var gameteam2_ID = "{{ $team2->gameteam_id }}";
		var team1_ID 	 = "{{ $team1->team_id }}";
		var team2_ID 	 = "{{ $team2->team_id }}";
		
		var game_isOver	 = {{ ($gameIsOver ? "true" : "false") }};
		
		var locked_rows  = {};
		
		if ( JSON.parse(localStorage.getItem(game_ID)) !== null ) { 
			locked_rows = JSON.parse(localStorage.getItem(game_ID)) 
		}
		
		/* Toggle the Locked Status of the Row */
		$.each(locked_rows, function(rowNumber, isLocked) {
		    $("#lock_round_"+rowNumber).prop('checked', isLocked);
		}); 
		
		@can('edit-scorecard')
		
			/* Toggle Score between possible Values of 2, 1, 0, -1 */
			$('.scoreCell').click(function()
			{
				// Handle the Score record ID, embedded in the td id
				cell_id 	  = event.target.id;
				round_number  = $("#"+cell_id).data("roundnumber");
				
				if(roundIsLocked(round_number) || game_isOver )
				{
					return false;
				}
				
				
				// Trigger a function that toggles the score between 2,1,0,-1,null and back again, and updates related gameteamround and gameplayer records
				$.ajax( { url: "/ajax/admin/games/" + game_ID + "/score/" + cell_id, cache: false } )
				.done(function(response) 
				{
					updateScoreCell(cell_id, response.score);
					updateGameTeamRoundTotals(response.teamPosition, response.roundNumber, response.roundScore, response.stealsAgainst, response.opponentRoundScore, response.opponentStealsAgainst);
					updateGamePlayerTotals(response.gamePlayerID, response.deuceCount);
					updateGameTotals();
				})
				.fail(function(response, status, error) 
				{
					console.log(response);
					$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
					$('#modal-danger').modal('show');
				})
			});
		
			/* Lock a Round Row to make it uneditable */
			$('.checkbox_lockRound').click(function()
			{
				cell_id 	  				= event.target.id;
				round_number  				= $("#"+cell_id).data("roundnumber");
				locked_rows[round_number] 	= $("#"+cell_id).prop('checked');
				localStorage.setItem(game_ID, JSON.stringify(locked_rows));
			});
		
			/* Change Team Player */
			$('.gamePlayer').click(function()
			{
				var cell_id 		= event.target.id;
				var gamePlayer_ID 	= cell_id.split("_")[1];
				var gameTeam_ID 	= cell_id.split("_")[2];
				var selectOptions	= "";
				
				$.ajax( {url: "/ajax/admin/gameteams/" + gameTeam_ID + "/teammates", cache: false } )
				.done(function(response) 
				{
					jQuery.each(response, function(i, val) {
						selectOptions+='<option value="'+i+'">'+val+'</option>';
					});				

					$('#modal-default .modal-body').html(
						'<div class="row">\
						<div class="col-xs-6">\
							<div class="form-group">\
							<label for="newUserID">Select a different Player</label><br>\
							<select class="select2" style="width: 100%;" name="newUserID" id="newUserID">\
								'+ selectOptions +'\
								</select>\
							</div>\
						</div></div>'
					);
					
					$('.select2').select2();
					$('#modal-default .modal-title').html("Change the Player?");
					$('#modal-default-btn-ok').data('gamePlayerID',gamePlayer_ID);
					$('#modal-default').modal('show');
	
					$('#modal-default-btn-ok').click(function() 
					{	
						var newUser_ID	= $("#newUserID").val();
						
						$.ajax({
							method: "POST",
							data: 
							{
								gamePlayerID: gamePlayer_ID,
								newUserID:	newUser_ID
							},
							url: "{{ route('adminAjaxChangePlayer', $gameID, false ) }}", 
							cache: false 
						})
						.done(function(response) 
						{
							$('#'+cell_id).html(response.name);
							$('#modal-default').modal('hide');
							location.reload();
						})
						.fail(function(response, status, error) 
						{
							console.log(response);
							$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
							$('#modal-danger').modal('show');
						})
					});
				})
				.fail(function(response, status, error) 
				{
					console.log(response);
					$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
					$('#modal-danger').modal('show');
				})
			});
			
			/* Swap the positions of Team1 and Team2, and the seats of players */
			$('#swapTeamPositions').click(function()
			{
				$.ajax( { url: "{{ route('adminAjaxSwapTeamPositions', $gameID, false) }}", cache: false } )
				.done(function(response) 
				{
					location.reload();					
				})
				.fail(function(response, status, error) 
				{
					console.log(response);
					$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
					$('#modal-danger').modal('show');
				})
			});
			
			/* End the game, declare a winner or a tie, and lock the Scores */
			$('#btn_finishGame').click(function()
			{
				$('#modal-default .modal-title').html('<h4><i class="far fa-check-circle"></i> Finish</h4>');
				$('#modal-default .modal-body').html("<p>Are you sure you want to finish off this game and lock the scores?</p>");
				$('#modal-default-btn-ok').text('Finish Game');
				$('#modal-default').modal('show');
				$('#modal-default-btn-ok').click(function() 
				{
					$.ajax( { url: "{{ route('adminAjaxFinishGame', $gameID, false) }}", cache: false } )
					.done(function(response)
					{
						location.reload();
					})
					.fail(function(response, status, error)
					{
						console.log(response);
						$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
						$('#modal-danger').modal('show');
					});
				});
			});
			
			
			/* undo the Finish Game */
			$('#btn_unlockGame').click(function()
			{
				$.ajax( { url: "{{ route('adminAjaxUnlockGame', $gameID, false) }}", cache: false } )
					.done(function(response)
					{
						location.reload();
					})
					.fail(function(response, status, error)
					{
						console.log(response);
						$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
						$('#modal-danger').modal('show');
					});
			});
			
			/* Delete the gamePlayers, gameTeamRounds and Scores */
			$('#btn_resetGame').click(function()
			{
				$('#modal-default-reset .modal-title').html('<h4 class="text-danger"><i class="fas fa-trash text-danger"></i> Reset</h4>');
				$('#modal-default-reset .modal-body').html("<p>Are you sure you want to completely reset this game? All the players and scores will be wiped out!");
				$('#modal-default-reset-btn-ok').text('Reset Game');
				$('#modal-default-reset').modal('show');
				$('#modal-default-reset-btn-ok').click(function() 
				{
					$.ajax( { url: "{{ route('adminAjaxResetGame', $gameID, false) }}", cache: false } )
					.done(function(response)
					{
						window.location.replace("{{ route('adminGameSetup', $gameID, false) }}");
					})
					.fail(function(response, status, error)
					{
						console.log(response);
						$('#modal-danger .modal-body').html("🙁 Something didn't work right . The server said: \"" + error + "\"");
						$('#modal-danger').modal('show');
					});
				});

			});
			
			
		@endcan
		
		
		/* Update the Score display in a Cell */
		function updateScoreCell (cell_id, score, roundTotalScore)
		{
			score = (score == null ? "" : score);
			newClass = getBGColorCell(score);
			$("#"+cell_id).text(score).attr( "class", "scoreCell noselect " + newClass );
		}

		/* Update the Score display in a Round cell */
		function updateGameTeamRoundTotals (teamPosition, roundNumber, roundScore, stealsAgainst, opponentRoundScore, opponentStealsAgainst)
		{
			var teamRoundScore 		 = roundScore + opponentStealsAgainst;
			var opposingTeamPosition = (teamPosition == 2 ? 1 : 2);
			var opposingRoundScore 	 = stealsAgainst + opponentRoundScore;
			$("#roundTotal_t" + teamPosition + "_" + roundNumber).text(teamRoundScore);
			$("#roundTotal_t" + opposingTeamPosition + "_" + roundNumber).text(opposingRoundScore);
			
		}
		
		/* Update the Score display in a Total cell */
		function updateGameTotals()
		{
			$.ajax( {url: "{{ route('publicAjaxGetGameTotals', $gameID, false) }}", cache: false } )
			.done(function(response)
			{

				newClassT1	= "default";
				newClassT2	= "default";
				
				t1Score = parseInt(response[0].TotalScore);
				t2Score = parseInt(response[1].TotalScore);
				
				if(t1Score > t2Score) 
				{
					newClassT1 = "green";
					newClassT2 = "red";
					$("#t1_trophy").show();
					$("#t2_trophy").hide();
				} 
				else if (t1Score < t2Score) 
				{
					newClassT1 = "red";
					newClassT2 = "green";
					$("#t1_trophy").hide();
					$("#t2_trophy").show();
				} 
				else 
				{
					$(".fa-circle").hide();
				}
				
				$("#t1_TotalScore").text(response[0].TotalScore).attr( "class", "round_total bg-" + newClassT1 );
				$("#t2_TotalScore").text(response[1].TotalScore).attr( "class", "round_total bg-" + newClassT2 );
				$("#t1_TeamName").attr( "class", "text-" + newClassT1 );
				$("#t2_TeamName").attr( "class", "text-" + newClassT2 );
				
			})
			.fail(function(response, status, error) {
				console.log(response);
			})
							
		}
		
		/* Update the Score display in a Player's cell */
		function updateGamePlayerTotals (gamePlayerID, deuceCount) 
		{
			$("#"+gamePlayerID).text(deuceCount);
		}
		
		/* Return the CSS Class that corresponds to the Score */
		function getBGColorCell(score)
		{
			newClass = "bg-light";
			if(score == 2) 			{ newClass="bg-success"; }
			else if(score == 1) 	{ newClass="bg-warning"; }
			else if(score == -1) 	{ newClass="bg-danger";  }
			else if(score == 0) 	{ newClass="bg-default"; }
			else  { newClass="bg-light"; }
			return newClass;
		}
		
		/* Check if the round is locked or not */
		function roundIsLocked(roundNumber)
		{
			if (locked_rows[roundNumber] == true){
				return true;	
			}
			else {
				return false;
			}
		}
		
		
	</script>
@stop