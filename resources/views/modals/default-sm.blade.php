<div class="modal fade bs-example-modal-sm" 
	@isset($modal_id) id="{{$modal_id}}" @else id="modal-default" @endisset 
	style="display: none;" tabindex="-1" >
		
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Excellent!</h4>
			</div>
			<div class="modal-body">
				{{ $slot }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal" 
					@isset($modal_id) id="{{$modal_id}}-btn-dismiss" @else id="modal-default-btn-dismiss" @endisset aria-hidden="true">Cancel</button>
				<button type="button" class="btn btn-primary" 
					@isset($modal_id) id="{{$modal_id}}-btn-ok" @else id="modal-default-btn-ok" @endisset> OK </button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>