<div class="modal modal-danger fade" 
	@isset($modal_id) id="{{$modal_id}}" @else id="modal-danger" @endisset 
	style="display: none;" tabindex="-1" >
	
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Whoops!</h4>
			</div>
			<div class="modal-body">
				{{ $slot }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal" 
				@isset($modal_id) id="{{$modal_id}}-btn-ok" @else id="modal-success-btn-ok" @endisset> Close </button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>