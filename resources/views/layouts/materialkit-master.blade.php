<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- Favicons -->
	<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">
    
    <title>
		@yield('title', config('app.name'))
	</title>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
    <link rel="stylesheet" href="/css/material-kit.min.css?v=2.0.2">
    <link rel="stylesheet" href="/css/overrides-public.css">
    <!-- iframe removal -->
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @yield('css')
    
    
</head>
<body class="@yield('body_class')">

	@yield('body')

	<footer class="footer ">
        <div class="container">
            <nav class="float-left">
                <ul>
                    <li>
                        <a href="{{route('publicCredits')}}">
                            Credits
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="copyright float-right">
                &copy;
                <script>
                    document.write(new Date().getFullYear())
                </script>, made by
                <a href="https://www.lintburger.com" target="_blank">Lintburger</a>
                <img src="/images/favicon-32x32.png" height="24px" class="rounded-circle" align="middle" alt="Lintburger">
            </div>
        </div>
    </footer>
    
    
    
	<!--   Core JS Files   -->
	<script src="/js/material-kit/jquery.min.js"></script>
	
	<!--   Laravel JS Files   -->
	<script src="{{ asset('/js/material-kit/app-public.js') }}"></script>
	
	
	<script src="/js/material-kit/popper.min.js"></script>
	<script src="/js/material-kit/bootstrap-material-design.min.js"></script>
	<!--  Plugin for Date Time Picker and Full Calendar Plugin  -->
	<script src="/js/material-kit/plugins/moment.min.js"></script>
	<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
	<script src="/js/material-kit/plugins/bootstrap-datetimepicker.min.js"></script>
	<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="/js/material-kit/plugins/nouislider.min.js"></script>
	<!-- Material Kit Core initialisations of plugins and Bootstrap Material Design Library -->
	<script src="/js/material-kit/material-kit.min.js?v=2.0.2"></script>
    
    @yield('js')

    
</body>

</html>