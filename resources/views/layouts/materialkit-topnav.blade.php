	<nav class="navbar navbar-color-on-scroll fixed-top navbar-expand-lg navbar-transparent" 
		color-on-scroll="100" id="sectionsNav">
		<div class="container">
			
			<div class="navbar-translate">
				<a class="navbar-brand" href="{{route('publicHome')}}">
					<img src="{{asset('storage/logos/league_logo@1x.png')}}" height="40px" class="" align="middle" alt="{{ config('app.name') }}">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					<span class="navbar-toggler-icon"></span>
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
			
			<div class="collapse navbar-collapse">
				<ul class="navbar-nav ml-auto">
					
					
					<li class="@isset($active_results) active @endisset nav-item">
						<a href="{{route('publicResults', optional($season)->id)}}" class="nav-link">Results</a>
					</li>
					<li class="@isset($active_schedule) active @endisset nav-item">
						<a href="{{route('publicSchedule', optional($season)->id)}}" class="nav-link">Schedule</a>
					</li>
					<li class="@isset($active_standings) active @endisset nav-item">
						<a href="{{route('publicStandings', optional($season)->id)}}" class="nav-link">Standings</a>
					</li>
					
					
					@isset($otherSeasons)
					<li class="dropdown @isset($active_archives) active @endisset nav-item">
						<a href="#pablo" class="dropdown-toggle nav-link" data-toggle="dropdown">
							<i class="material-icons">apps</i>
							Archives
						</a>
						<div class="dropdown-menu">
							@foreach($otherSeasons as $oldSeason)
								<h6 class="dropdown-header">{{$oldSeason->seasonName}}</h6>
								<a href="{{route('publicResults',$oldSeason->id)}}" class="dropdown-item">
									<i class="material-icons">layers</i>	
										Results
								</a>
								<a href="{{route('publicStandings',$oldSeason->id)}}" class="dropdown-item">
									<i class="material-icons">content_paste</i>	
										Standings
								</a>
								
								@if (!$loop->last) 
								<div class="dropdown-divider"></div>
								@endif
							@endforeach
						</div>
					</li>
					@endisset
					
				
					@php
						$arbitrary=json_decode($league->arbitraryJSON);
					@endphp
					@if( !empty($arbitrary->social_facebook) ) 
					
					<li class="nav-item">
					
						<a class="nav-link" rel="tooltip" title="" data-placement="bottom" 
							   href="{{$arbitrary->social_facebook}}" target="_blank" data-original-title="League Facebook Page">
								<i class="fab fa-facebook-square fa-2x"></i>
						</a>
					</li>
					@endif
					@if( !empty($arbitrary->league_email) )
					<li class="nav-item">
						<a class="nav-link" rel="tooltip" title="" data-placement="bottom" 
							href="mailto:{{$arbitrary->league_email}}" data-original-title="{{$arbitrary->league_email}}">
							<i class="fas fa-envelope fa-2x"></i>
						</a>
					</li>
					@endif
					
					<li class="nav-item" style="margin:4px auto 0px auto;">
						@if (Auth::check())
						
							@php
								$avatar = ( empty(auth()->user()->avatar)  ? asset('storage/avatars/users/default.jpg') : auth()->user()->avatar );
							@endphp
								
							<a href="{{ route('adminHome') }}" class="btn btn-raised btn-fab btn-round">
								<div class="profile-photo-small" >
									<img src="{{$avatar}}" class="rounded-circle img-fluid" alt="User Avatar">
									<span class="hidden-xs">{{Auth::user()->nameFull}}</span>
								</div>
								<div class="ripple-container"></div>
							</a>
									
						@elseif (!Auth::check())
						
							<a href="{{ route('login', null, false) }}" class="btn btn-danger btn-raised btn-round">
								<i class="material-icons">vpn_key</i> 
								Login
								<div class="ripple-container"></div>
							</a>
							
						@endif
					</li>
					
				</ul>
			</div>
		</div>
	</nav>