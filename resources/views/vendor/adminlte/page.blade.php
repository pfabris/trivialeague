@extends('adminlte::master')

@section('adminlte_css')
	<link rel="stylesheet"
		  href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
	@stack('css')
	@yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
	'boxed' => 'layout-boxed',
	'fixed' => 'fixed',
	'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
	<div class="wrapper">
		
		
		<!-- Main Header -->
		<header class="main-header">
			@if(config('adminlte.layout') == 'top-nav')
			<nav class="navbar navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
							{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
						</a>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
						<ul class="nav navbar-nav">
							@each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
						</ul>
					</div>
					<!-- /.navbar-collapse -->
			@else
			<!-- Logo -->
			<a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
			</a>

			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
				</a>
			@endif
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">

					<ul class="nav navbar-nav">
						@if(Auth::check())
							@php
								$avatar = ( empty(auth()->user()->avatar)  ? asset('storage/avatars/users/default.jpg') : auth()->user()->avatar );
							@endphp
									
							<li class="dropdown user user-menu">

								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
									<img src="{{$avatar}}" class="user-image" alt="User Avatar">
									<span class="hidden-xs">{{Auth::user()->nameFull}}</span>
								</a>
								<ul class="dropdown-menu">
	
									<!-- User image -->
									<li class="user-header">
										<img src="{{$avatar}}" class="img-circle" alt="User Avatar">
								
										<p>
											{{Auth::user()->nameFull}}
											<small>Member since {{Auth::user()->dateJoined}}</small>
										</p>
									</li>

									<li class="user-body">
										<div class="row">
											<div class="col-xs-6 text-center">
												@if(!empty(Auth::user()->team_id))
												<a href="{{ route('profileTeam', Auth::user()->team_id) }}">Team</a>
												@endif
											</div>
											<div class="col-xs-6 text-center">
												<a href="#"></a>
											</div>
										</div>
										<!-- /.row -->
									</li>							

									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="{{ route('profileUser', Auth::user()->id) }}" class="btn btn-default btn-flat">My Profile</a>
										</div>
										<div class="pull-right">
											<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Log Out</a>
										</div>
									</li>
								</ul>
							</li>

						
						
						@endif
						<li>
							@if (Auth::check())
								
									<a href="#"
									   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
									>
										<i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }} 
									</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@if(config('adminlte.logout_method'))
											{{ method_field(config('adminlte.logout_method')) }}
										@endif
										{{ csrf_field() }}
									</form>
									
									
							@elseif (!Auth::check())	
									<a href="{{ route('login') }}">
										<i class="fa fa-fw fa-power-off"></i> Login 
									</a>
							@endif
						</li>
					</ul>
				</div>
				@if(config('adminlte.layout') == 'top-nav')
				</div>
				@endif
			</nav>
		</header>

		@if(config('adminlte.layout') != 'top-nav')
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">

			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">

				<!-- Sidebar Menu -->
				<ul class="sidebar-menu" data-widget="tree">
					@each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
				</ul>
				<!-- /.sidebar-menu -->
			</section>
			<!-- /.sidebar -->
		</aside>
		@endif

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			@if(config('adminlte.layout') == 'top-nav')
			<div class="container">
			@endif

			<!-- Content Header (Page header) -->
			<section class="content-header">
				@yield('content_header')
			</section>

			<!-- Main content -->
			<section class="content">
				
				@include('modals.flash-message')
				
				@yield('content')

			</section>
			<!-- /.content -->
			@if(config('adminlte.layout') == 'top-nav')
			</div>
			<!-- /.container -->
			@endif
		</div>
		<!-- /.content-wrapper -->
		
		
		<!-- Main Footer -->
		<footer class="main-footer">
			@yield('footer')
			
			<strong>Copyright</strong> © {{ date('Y') }} Lintburger Software
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0 beta
			</div>
			
		</footer>
		<!-- ./footer -->
		
	</div>
	<!-- ./wrapper -->
@stop

@section('adminlte_js')
	<script src="{{ asset('vendor/adminlte/dist/js/adminlte.js') }}"></script>
	<script src="/js/custom.js"></script>
	@stack('js')
	@yield('js')
@stop
