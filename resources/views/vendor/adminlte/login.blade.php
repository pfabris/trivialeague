@extends('adminlte::master')
@section('title','TTL | Login')
@section('adminlte_css')
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
	<link rel="stylesheet" href="/css/overrides.css">
	@yield('css')
@stop

@section('body_class', 'login-page')

@section('body')

<div class="vertical-center">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12">
				<div class="register-logo">
					<a href="{{ route('publicHome') }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
				</div>
			</div>
		</div>
		
		
		<div class="row margin-bottom-30">
			<div class="col-xs-offset-1 col-xs-10	 col-sm-offset-2 col-sm-8	col-md-offset-3 col-md-6 	col-lg-offset-4 col-lg-4">
				<div class="login-box-body">
					
					<p class="login-box-msg">
						Welcome!
					</p>
					
					<form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
						{!! csrf_field() !!}

						<div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
							<input type="email" name="email" class="form-control" value="{{ old('email') }}"
								   placeholder="{{ trans('adminlte::adminlte.email') }}">
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
						
						<div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
							<input type="password" name="password" class="form-control"
								   placeholder="{{ trans('adminlte::adminlte.password') }}">
							<span class="glyphicon glyphicon-lock form-control-feedback"></span>
							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
						
						<div class="row">
							<div class="col-xs-8">
								<div class="checkbox icheck">
									<label>
										<input type="checkbox" name="remember"> {{ trans('adminlte::adminlte.remember_me') }}
									</label>
								</div>
							</div>
							<!-- /.col -->
							
							<div class="col-xs-4">
								<button type="submit" class="btn btn-primary btn-block">{{ trans('adminlte::adminlte.sign_in') }}</button>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
						
					</form>
					
					<div class="auth-links text-center">
						<div class="row">
							<div class="col-xs-6">
								<a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
								   class="text-center"
								>{{ trans('adminlte::adminlte.i_forgot_my_password') }}</a>
							</div>
							<!-- /.col -->
							
							<div class="col-xs-6">
								<a href="{{route('requestInvitation')}}">Request Invite</a>
							</div>
							<!-- /.col -->
						</div>				
					</div>
					<!-- /.auth-links -->
					
				</div>
				<!-- /.login-box-body -->
			</div>
		</div>

	</div>
	<!-- /.container -->
</div>
		
@stop

@section('adminlte_js')
	<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '10%' // optional
			});
		});
	</script>
	@yield('js')
@stop
