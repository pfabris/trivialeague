@extends('adminlte::master')
@section('title','TTL | Register')
@section('adminlte_css')
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
		<link rel="stylesheet" href="/css/overrides.css">
		@yield('css')
@stop

@section('body_class', 'register-page')

@section('body')

<div class="vertical-center">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12">
				<div class="register-logo">
					<a href="{{ route('publicHome') }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
				</div>
			</div>
		</div>
		

		<div class="row">
			<div class="col-xs-offset-1 col-xs-10	 col-sm-offset-2 col-sm-8	col-md-offset-3 col-md-6 	col-lg-offset-4 col-lg-4">

				<div class="register-box-body">
					
					<p class="login-box-msg">{{ trans('adminlte::adminlte.register_message') }}</p>
					
					<form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
						{!! csrf_field() !!}
						<input type="hidden" name="userID" value="{{$user->id}}">
					 
					 <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
						<input type="text" name="nameFirst" class="form-control" value="{{ $user->nameFirst }}"
								placeholder="{{ trans('adminlte::adminlte.first_name') }}">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
						@if ($errors->has('nameFirst'))
								<span class="help-block">
								 <strong>{{ $errors->first('nameFirst') }}</strong>
								</span>
						@endif
					 </div>
					 <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
						<input type="text" name="nameLast" class="form-control" value="{{ $user->nameLast }}"
								placeholder="{{ trans('adminlte::adminlte.last_name') }}">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					 </div>
					 <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
						<input type="email" name="email" class="form-control" value="{{ $invite->email }}"
								placeholder="{{ trans('adminlte::adminlte.email') }}">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('email'))
								<span class="help-block">
								 <strong>{{ $errors->first('email') }}</strong>
								</span>
						@endif
					 </div>
					 <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
						<input type="password" name="password" class="form-control"
								placeholder="{{ trans('adminlte::adminlte.password') }}">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						@if ($errors->has('password'))
								<span class="help-block">
								 <strong>{{ $errors->first('password') }}</strong>
								</span>
						@endif
					 </div>
					 <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
						<input type="password" name="password_confirmation" class="form-control"
								placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
						<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
						@if ($errors->has('password_confirmation'))
								<span class="help-block">
								 <strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
						@endif
					 </div>
					 <button type="submit"
								class="btn btn-primary btn-block btn-flat"
					 >{{ trans('adminlte::adminlte.register') }}</button>
					</form>
					<!-- /.form -->

					<div class="auth-links">
						<a href="{{ url(config('adminlte.login_url', 'login')) }}"
							class="text-center">{{ trans('adminlte::adminlte.i_already_have_a_membership') }}</a>
					</div>
					
				</div>
				<!-- /.register-box -->
				
			</div>
		</div>

	</div>
</div>
@stop

@section('adminlte_js')
		@yield('js')
@stop
