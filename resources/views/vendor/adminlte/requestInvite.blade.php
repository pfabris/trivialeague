@extends('adminlte::master')
@section('title','TTL | Request Invite')
@section('adminlte_css')
	<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
	<link rel="stylesheet" href="/css/overrides.css">
	@yield('css')
@stop

@section('body_class', 'register-page')

@section('body')
<div class="vertical-center">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12">
				<div class="register-logo">
					<a href="{{ route('publicHome') }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
				</div>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-xs-offset-1 col-xs-10	 col-sm-offset-2 col-sm-8	col-md-offset-3 col-md-6 	col-lg-offset-4 col-lg-4">
				<div class="register-box-body">
					
					<p class="login-box-msg">Requst an Invitation</p>
					
					@if (session('status'))
		                <div class="alert alert-success">
		                    {{ session('status') }}
		                </div>
		            @endif
					
					<p>
						If you're a captain, or a player in the league, or a QM, ask for an invite code so we can set you up with an account. 
					</p>
					<p>
						An account allows captains to manage teams, QMs to download questions, and anyone to submit rounds and update their profile.
					</p>
					<p>
						Not in the league? Looking to join? <a href="mailto:{{ json_decode($league->arbitraryJSON)->league_email }}">Email us!</a>
					</p>
					<form action="{{ route('requestInvitationSubmit') }}" method="post">
						{!! csrf_field() !!}
		
						<div class="form-group has-feedback {{ $errors->has('nameFirst') ? 'has-error' : '' }}">
							<input type="text" name="nameFirst" class="form-control" value=""
								   placeholder="{{ trans('adminlte::adminlte.first_name') }}">
							<span class="glyphicon glyphicon-user form-control-feedback"></span>
							@if ($errors->has('nameFirst'))
								<span class="help-block">
									<strong>{{ $errors->first('nameFirst') }}</strong>
								</span>
							@endif
						</div>
						
						<div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
							<input type="text" name="nameLast" class="form-control" value=""
								   placeholder="{{ trans('adminlte::adminlte.last_name') }}">
							<span class="glyphicon glyphicon-user form-control-feedback"></span>
						</div>
						
						<div class="form-group has-feedback {{ $errors->has('teamName') ? 'has-error' : '' }}">
							<input type="text" name="teamName" class="form-control" value=""
								   placeholder="What Team are you on?">
							<span class="glyphicons glyphicons-group form-control-feedback"></span>
						</div>
						
						<div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
							<input type="email" name="email" class="form-control" value=""
								   placeholder="{{ trans('adminlte::adminlte.email') }}">
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
						
						<button type="submit" class="btn btn-primary btn-block btn-flat">
							Request Invite
						</button>
						
					</form>
					
					<div class="auth-links text-center">
						<a href="{{ url(config('adminlte.login_url', 'login')) }}"
						   class="text-center">{{ trans('adminlte::adminlte.i_already_have_a_membership') }}
						</a>
					</div>
				
				</div><!-- /.register-box -->
			</div>
		</div>
	
	</div>
</div>	
@stop

@section('adminlte_js')
	@yield('js')
@stop
