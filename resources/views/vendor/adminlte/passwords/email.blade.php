@extends('adminlte::master')
@section('title','TTL | Password Reset - Enter Email')
@section('adminlte_css')
	<link rel="stylesheet" href="/css/overrides.css">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')

<div class="vertical-center">
	<div class="container">
		
		
		<div class="row">
			<div class="col-xs-12">
				<div class="register-logo">
					<a href="{{ route('publicHome') }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
				</div>
			</div>
		</div>

		<div class="row margin-bottom-30">
			<div class="col-xs-offset-1 col-xs-10	 col-sm-offset-2 col-sm-8	col-md-offset-3 col-md-6 	col-lg-offset-4 col-lg-4">

		        <div class="login-box-body">
		            <p class="login-box-msg">Reset Password - Step 1</p>
		            @if (session('status'))
		                <div class="alert alert-success">
		                    {{ session('status') }}
		                </div>
		            @endif
		            <form action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
		                {!! csrf_field() !!}
		
		                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
		                    <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}"
		                           placeholder="Enter your email address to begin">
		                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		                    @if ($errors->has('email'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('email') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <button type="submit"
		                        class="btn btn-primary btn-block"
		                >{{ trans('adminlte::adminlte.send_password_reset_link') }}</button>
		                <div class="auth-links text-center"></div>
		            </form>
		        </div>
		        <!-- /.login-box-body -->
		        
			</div>
		</div>

		
		
	</div>
</div>

    
@stop

@section('adminlte_js')
    @yield('js')
@stop
