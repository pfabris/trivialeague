@extends('adminlte::master')
@section('title','TTL | Password Reset - Reset Password')
@section('adminlte_css')
	<link rel="stylesheet" href="/css/overrides.css">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')

<div class="vertical-center">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12">
				<div class="register-logo">
					<a href="{{ route('publicHome') }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
				</div>
			</div>
		</div>

		<div class="row margin-bottom-30">
			<div class="col-xs-offset-1 col-xs-10	 col-sm-offset-2 col-sm-8	col-md-offset-3 col-md-6 	col-lg-offset-4 col-lg-4">

		        <div class="login-box-body">
		            <p class="login-box-msg">
			            Reset Password - Step 2
		            </p>
		            <form action="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" method="post">
		                {!! csrf_field() !!}
		
		                <input type="hidden" name="token" value="{{ $token }}">
		
		                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
		                    <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}"
		                           placeholder="{{ trans('adminlte::adminlte.email') }}">
		                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		                    @if ($errors->has('email'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('email') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
		                    <input type="password" name="password" class="form-control"
		                           placeholder="{{ trans('adminlte::adminlte.password') }}">
		                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		                    @if ($errors->has('password'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('password') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
		                    <input type="password" name="password_confirmation" class="form-control"
		                           placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
		                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
		                    @if ($errors->has('password_confirmation'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('password_confirmation') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <button type="submit"
		                        class="btn btn-primary btn-block btn-flat"
		                >{{ trans('adminlte::adminlte.reset_password') }}</button>
		            </form>
		        </div>
		        <!-- /.login-box-body -->
			</div>
		</div>


	</div>
</div>
		        
@stop

@section('adminlte_js')
    @yield('js')
@stop
