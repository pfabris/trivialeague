@component('mail::message')

{{-- Greeting --}}
# Hi {{ $user->nameFirst }}

{{-- Intro Lines --}}
You're getting this email because you've been invited to join the {{ $league->leagueName }}.

## Why bother?

* allows anyone to submit Rounds
* lets QMs keep score at games
* lets Team Captains manage their teams
* get notifications
* track scores and statistics


@component('mail::panel')
## How To:
1. Click the green __Activate__ button below
2. Setup a password (please don't re-use any of your other online passwords!)
3. Login and setup your Profile
@endcomponent

{{-- Action Button --}}
@component('mail::button', ['url' => route('acceptInvitation', $invite->token), 'color' => 'green'])
Activate your TTL Account
@endcomponent


##Welcome aboard!  
{{ $league->user->nameFull }}<br>
{{ json_decode($league->arbitraryJSON)->league_email }}

{{-- Subcopy --}}
@component('mail::subcopy')
If you don't know why you got this email, it's ok to ignore it. 
If you think something fishy is going on, you can send us an email: {{ config('mail.from_address')}}
@endcomponent

@endcomponent
