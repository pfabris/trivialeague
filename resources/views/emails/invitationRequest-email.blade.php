@component('mail::message')

{{-- Greeting --}}
# Hi League Admin

{{-- Intro Lines --}}
Someone is requesting an invitation to join the {{ $league->leagueName }}. As the League Admin, it's up to you to send them an invitation so they can join the league.

@component('mail::panel')
## Details:
Name: **{{ $request->get('nameFirst') }} {{ $request->get('nameLast') }}**  
Team: {{ $request->get('teamName') }}  
Email: **{{ $request->get('email') }}**  
@endcomponent

@component('mail::panel')
## What to do:
1. Check out who requested the invite  
2. Login and see if they already exist in the User Database  
3. If YES, click the Send Invitation button on their User Profile screen  
4. If NO, create a new User, associate them to their Team, or set them up as a QM or Author
5. Click the Send Invitation button - this will allow them to setup their own password and login  

If you don't know who this is, you may want to email them back.
@endcomponent

{{-- Action Button --}}
@component('mail::button', ['url' => route('adminLeagueUsers'), 'color' => 'green'])
Login Now
@endcomponent



{{-- Subcopy --}}
@component('mail::subcopy')
If you don't know why you got this email, it's probably ok to ignore it. 
If you think something fishy is going on, you can send us an email: {{ config('mail.from_address')}}
@endcomponent

@endcomponent
