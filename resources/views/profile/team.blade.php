@extends('adminlte::page')

@section('title', config('app.name') . ' Team Profile ' )

@section('content_header')
	<h1> Team Profile </h1>
@stop

@php
	$existsTeamAvatar = Storage::disk('public')->exists('avatars/teams/'. $team->avatar);
	
	if (!$existsTeamAvatar|| $team->avatar==null) {
		$avatar = 'default.jpg';
	} else {
		$avatar = $team->avatar;
	}
@endphp

@section('content')
	
	<!-- ######################  TEAM  ############################################################################ -->
		
	<div class="row">
			
			<!-- ######################  CALLOUT  ############################################################################ -->
			<div class="col-md-4">
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" 
							src="{{ asset('storage/avatars/teams/'. $team->avatar) }}" alt="Team avatar">
						
						<h3 class="profile-username text-center">{{ $team->teamName }}</h3>
						
						<p class="text-muted text-center">
							@isset($location) 
								from {{$location->locationName}}
							@endisset
						</p>						
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Games Played</b> <a class="pull-right">{{ formatNumber($gamesPlayed) }}</a>
							</li>
							<li class="list-group-item">
								<b>Current Season Rank </b> <a class="pull-right">{{ formatNumber($seasonRank) }}</a>
							</li>
							<li class="list-group-item">
								<b>All Time Rank</b> <a class="pull-right">{{ formatNumber($allTimeRank) }}</a>
							</li>
						</ul>

					</div>
					<!-- /.box-body -->
				</div>
			</div>
			<!-- ######################  FORM  ######################## -->
			<div class="col-md-8">
			
				<div class="box box-primary">
					
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border">
						<h3 class="box-title">
							<i class="fa fa-users"></i> Team Members
						</h3>
					</div>
					
					<div class="box-body">
						
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th>Name</th>
									<th style="width: 40px">Role</th>
								</tr>
								@forelse($teammates as $teammate)
								<tr>
									<td>
										<a href="{{route('profileUser',$teammate->id)}}">
										{{$teammate->nameFull}}
										</a>
									</td>
									<td>
										<span class="badge @if($teammate->primaryRole=='Captain') bg-green @else bg-primary @endif">
										{{$teammate->primaryRole}}
										</span>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
						
					</div>
					

	
	@component('modals.default')
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop

	