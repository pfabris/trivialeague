@extends('adminlte::page')

@section('title', config('app.name') . ' User Profile ' )

@section('content_header')
	<h1>User Profile Page</h1>
@stop

@section('content')



	<!-- ######################  USER  ############################################################################ -->
		
	<div class="row">
			
			<!-- ######################  CALLOUT  ######################## -->
			<div class="col-md-4">
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" 
							src="{{ ( empty($user->avatar) ? asset('storage/avatars/users/default.jpg') : $user->avatar ) }}" alt="User avatar">
						
						<h3 class="profile-username text-center">{{ $user->nameFull }}</h3>
						
						<p class="text-muted text-center">
							{{ $user->primaryRole }}
							@isset($team) 
								of <a href="{{route('profileTeam',$team->id)}}">{{$team->teamName}}</a>
							@endisset
						</p>
						
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Games Played</b> <a class="pull-right">{{ formatNumber($gamesPlayed) }}</a>
							</li>
							<li class="list-group-item">
								<b>Current Season Rank </b> <a class="pull-right">{{ formatNumber($seasonRank) }}</a>
							</li>
							<li class="list-group-item">
								<b>All Time Rank</b> <a class="pull-right">{{ formatNumber($allTimeRank) }}</a>
							</li>
						</ul>
						
						
					</div>
					<!-- /.box-body -->
				</div>
			</div>
			
			
			<!-- ######################  FORM  ######################## -->
			<div class="col-md-8">
			
				<div class="box box-primary">
					
					<!-- ****** HEADER ****************************************************** -->
					<div class="box-header with-border">
						<h3 class="box-title">
							<i class="fa fa-user"></i> User Details
						</h3>
					</div>
					
					
					
					@if ($errors->any())
					<div class="box-header with-border">
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
				    </div>
					@endif

					<!-- ****** USER  ****************************************************** -->
					<form class="form-horizontal" method="POST" action="{{ route('profileUserUpdate',['id' => $user->id]) }}">
						<input name="_method" type="hidden" value="PATCH">
				        {{csrf_field()}}
						<input type="hidden" name="league_id" value="{{$league->id}}">
						<input type="hidden" name="user_id" value="{{$user->id}}">
						
						<div class="box-body">
							
							<div class="form-group">
								<label for="inputFirstName" class="col-sm-2 control-label">First Name</label>
								<div class="col-sm-10">
									<input type="text" name="nameFirst" value="{{$user->nameFirst}}" class="form-control" id="inputFirstName" placeholder="First Name" required>
								</div>
							</div>

							<div class="form-group">
								<label for="inputLastName" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-10">
									<input type="text" name="nameLast" value="{{$user->nameLast}}" class="form-control" id="inputLastName" placeholder="Last Name">
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputDateJoined" class="col-sm-2 control-label">Date Joined</label>
								<div class="col-sm-10">
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" name="dateJoined" class="form-control pull-right datepicker" id="inputDateJoined" value="{{$user->dateJoined}}" placeholder="The date, roughly, that you joined the League (yyyy-mm-dd)">
									</div>
									<!-- /.input group -->
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Email</label>
					
								<div class="col-sm-10">
									<input type="email" name="email" value="{{$user->email}}" class="form-control" id="inputEmail" placeholder="Email" required data-error="Email address is invalid">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="inputPhone" class="col-sm-2 control-label">Mobile</label>
					
								<div class="col-sm-10">
									<input type="text" name="phoneMobile" value="{{$user->phoneMobile}}" class="form-control" id="inputPhone" placeholder="Phone">
								</div>
							</div>

							<div class="form-group">
								<label for="selectNotify" class="col-sm-2 control-label">Notify Via</label>
					
								<div class="col-sm-10">
									<select id="selectNotify" name="notificationPreference" class="form-control">
										@forelse ($notificationPrefs as $pref)
											<option value="{{$pref}}" @if($pref==$user->notificationPreference) selected=selected @endif>{{$pref}}</option>
										@empty
										
										@endforelse
										</optgroup>
									</select>
								</div>
							</div>
							
							
							<div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
								<label for="inputPassword" class="col-sm-4 control-label">{{ trans('adminlte::adminlte.password') }}</label>
								<div class="col-sm-8">
								<input type="password" name="password" id="inputPassword" class="form-control">
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
									@if ($errors->has('password'))
									    <span class="help-block">
									        <strong>{{ $errors->first('password') }}</strong>
									    </span>
									@endif
								</div>
							</div>
							<div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
								<label for="inputPasswordConfirmation" class="col-sm-4 control-label">{{ trans('adminlte::adminlte.retype_password') }}</label>
								<div class="col-sm-8">
									<input type="password" name="password_confirmation" id="inputPasswordConfirmation" class="form-control">
									<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
									@if ($errors->has('password_confirmation'))
									    <span class="help-block">
									        <strong>{{ $errors->first('password_confirmation') }}</strong>
									    </span>
									@endif
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="checkbox" disabled="disabled" name="status" value="Active" @if($user->status=="Active") checked=checked @endif> Active
								</div>
							</div>
							
						</div>
						<!-- /.box-body -->
						@can("edit-user-self") 
							@if(\Auth::user()->id == $user->id)
							<div class="box-footer">
								
								<button type="submit" class="btn btn-primary pull-right">Save Changes</button>
								
							</div>
							<!-- /.box-footer -->
							@endif
						@endcan
					</form>
					
				</div>
				<!-- /.box -->
				
			</div>
	</div>
	
	
	
	@component('modals.default')
	@endcomponent
	@component('modals.danger')
	@endcomponent

@stop

@section('js')

@stop

