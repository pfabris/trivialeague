@extends('layouts.materialkit-master')
@section('title','TTL | Schedule')

@section('css')
	
@stop

@section('body_class','schedule-page')
@section('body')

	@component('layouts.materialkit-topnav',
		[
			"active_schedule"	=> true,
			"season" 			=> $season,
			"otherSeasons"		=> $otherSeasons,
			"league"			=> $league
		])
	@endcomponent
	
	
	<div class="page-header header-filter" data-parallax="true" style=" background-image: url('/images/city-cityscape-amarpreet-kaur.jpg'); ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
	                <h4 class="display-4"> 
		                Schedule: {{$season->seasonName}}
	                </h4>
	                <h4>
		                {{ date('M j, Y', strtotime( $gameNight->date )) }}
	                </h4>
					
                </div>
            </div>
        </div>
    </div>
	
	<div class="main main-raised">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
			
					<div class="card text-left">
						
						<div class="card-header" style="margin-top: -60px;">
							<ul class="nav">
								@forelse($gameNights as $thisGameNight)
								
								<li class="nav-item">
									<a class="nav-link @if( $thisGameNight->id == $gameNight->id ) btn btn-primary @endif"
										href="{{ route('publicSchedule', [$season->id, $thisGameNight->id]) }}">
											{{ date('M j', strtotime( $thisGameNight->date )) }}
										</a>
								</li>
								@empty
								
								@endforelse
							</ul>
					  	</div>
					  	
						<!-- ###### BY LOCATION	####### -->
					  	<div class="card-body">
						  	<div class="row">
							  	<div class="col-md-12">
							  		<h3 class="title text-left">By Location</h3>
						  		</div>
						  	</div>
					  		<div class="row">
						  		
						  		@forelse($games as $game)
							  		
									<div class="col-md-4">
										
										
										<div class="card card-nav-tabs" >
											<img class="card-img-top" height="100px" src="{{ asset('storage/banners/locations/'. $game->location->banner) }}" alt="{{$game->location->locationName}} Banner">
											<div class="card-header card-header-primary">
												
												<h5 class="title" style="margin: 2px 0px 3px 0px">
													@ {{$game->location->locationName}}
												</h5>
												
											</div>
											<div class="card-body">
												
												<div class="row text-center">
													@forelse($game->gameteams as $gameTeam)
													<div class="col-md-6" style="padding-left:2px; padding-right:2px">
														<strong>{{$gameTeam->team->teamName}}</strong>
													</div>
													@empty
													@endforelse
												</div>
												
											</div>
											
										</div>
	
										
							  		</div>
							  		
						  		@empty
						  		@endforelse
					  		</div>
					  	</div>
					  	
					  	<!-- ###### BY TEAM	####### -->
					  	<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<h3 class="title text-left">By Team</h3>
								</div>
							</div>
							<div class="row">	
								@forelse($teams as $team)
								<div class="col-lg-3 col-md-12 mr-auto mb-5">
									<div class="info-horizontal">
									<div class="icon" style="margin-top:10px;">
	                                    <img src="{{ asset('storage/avatars/teams/' . $team->avatar) }}" style="height:72px;" class="rounded-circle img-thumbnail" alt="{{ $team->teamName }} Avatar">
	                                </div>
									<div class="description">
										<h4 class="title">{{$team->teamName}}
										<br><small>@ {{$team->locationName}}</small></h4>
									</div>
									</div>
								</div>
								@empty
								@endforelse

							</div>
					  	</div>
					</div>
				</div>
			</div>
        </div>
	</div>
@stop