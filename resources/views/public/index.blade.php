@extends('layouts.materialkit-master')
@section('title','TTL | Home')

@section('css')
<style type="text/css">
	
	html { font-size: .74rem; }
	
	@media (min-width: 320px) {
	    html { font-size: .9rem; }
	}
	
	@media (min-width: 375px) {
	    html { font-size: 1rem; }
	}
	
</style>
@stop

@section('body_class','index-page')
@section('body')

	@component('layouts.materialkit-topnav',
		[
			"active_index"	=> "active",
			"season" 		=> $season,
			"otherSeasons"	=> $otherSeasons,
			"league"		=> $league
		])
	@endcomponent

<div class="page-header header-filter" 
	data-parallax="true" style="background-image: url('/images/city-cityscape-amarpreet-kaur.jpg'); ">
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-12 col-md-8 ml-auto mr-auto">
				<img src="{{ asset('storage/logos/league_logo@2x.png') }}" class="img-fluid d-none d-sm-block ml-auto mr-auto" width="80%">
				
				
				<h1 class="text-center d-none d-sm-block">
					Toronto Trivia League
				</h1>
				<h3 class="text-center d-none d-sm-block">
					New players, teams and pubs are welcome!
				</h3>
				
				<h2 class="text-center d-block d-sm-none">
					Toronto Trivia League
				</h2>
				<h5 class="text-center d-block d-sm-none">
					New players, teams and pubs are welcome!
				</h5>
				
			</div>
		</div>
		
		<!-- end .row -->
		<div class="row mt-4">
			<div class="col-sm-4 text-center">
				<a class="btn btn-danger btn-round btn-sm" href="{{route('publicResults')}}"><i class="material-icons">flash_on</i> Latest Results</a>
			</div>
			<div class="col-sm-4 text-center">
				<a class="btn btn-danger btn-round btn-sm" href="{{route('publicStandings')}}"><i class="material-icons">star_border</i> Standings</a>
			</div>
			<div class="col-sm-4 text-center">
				<a class="btn btn-danger btn-round btn-sm" href="./#announcementsSection" data-id="#announcementsSection" data-scroll="true"><i class="material-icons">announcement</i> Announcements</a>
			</div>
		</div>
	</div>
	<!-- end .container -->
</div>
<!-- end .page-header -->
	
<div class="main main-raised">
	<div class="container">


		<div class="section" id="introductionSection">

			<div class="row text-center">
				<div class="col-md-8 ml-auto mr-auto">
					<h2 class="title">
						A league with a difference
					</h2>
					<h5 class="description">
						Mondays at 8PM. We play Winter, Spring and Fall seasons every year.
					</h5>
				</div>
			</div>
			 <!-- end .row -->

			<div class="row">
				<div class="col-md-12">
					<p class="lead">
						About the league:
					</p>
					<p> The Toronto Trivia League was created by Peter Mathieson in 1992.</p>
					<ul>
					   <li>It's a hands on league where QMs are treated with respect</li>
					   <li>Protests are resolved as the game is played</li>
					   <li>Every round is a bonus round</li>
					   <li>Questions are calibrated so that the best players get 7 or more correct answers each night</li>
					   <li>Players each pay $8 per night, regardless of how many show up - the team is not required to make up any shortfall</li>
					   <li>If we use a round you create, we pay you $12, you will be the QM for that round and the QM will attempt to answer your question</li>
					   <li>The banquet features good food and everyone is invited</li>
					   <li>QMs drink on the League's tab at the banquet</li>
					   <li>Results are posted on the website usually within 24 hours</li>
					</ul>
					<p class="lead text-center">
						<a href="http://www.trivialeague.com" target="_blank">Read more...</a>
					</p>
				</div>
				 <!-- end .col -->
				
			</div>
			 <!-- end .row -->

		</div>
		<!-- end .section -->

		<div class="section text-center" id="announcementsSection">

			<div class="row text-center">
				<div class="col-md-8 ml-auto mr-auto">
					<h2 class="title">
						Announcements
					</h2>
				</div>
			</div>
			 <!-- end .row -->

			<div class="row">
				<div class="col-sm-4">
					<div class="info">
						<div class="icon icon-success">
	                        <i class="material-icons">announcement</i>
	                    </div>
					</div>
					<h4 class="info-title">Spring 2018</h4>
					<p>
						The spring season starts <strong>April 30</strong>, all teams are at home. <a href="{{route('publicSchedule')}}">Check out the schedule.</a>
					</p>
				</div>
				<div class="col-sm-4">
					<div class="info">
						<div class="icon icon-info">
	                        <i class="material-icons">people</i>
	                    </div>
					</div>
					<h4 class="info-title">Teams Wanted</h4>	
					<p>
						We have space for 1 full new team of 5 (at Betty's). 
					</p>
				</div>
				<div class="col-sm-4">
					<div class="info">
						<div class="icon icon-danger">
	                        <i class="material-icons">gavel</i>
	                    </div>
					</div>
					<h4 class="info-title">QMs Wanted</h4>	
					<p>
						We always need spare Quizmasters (referees) for relief duties.  QM's get paid $15 per game.
					</p>
				</div>
			</div>
			
		</div>

		<div class="text-center" id="locationsSection">
			
			<div class="row">
				<div class="col-md-8 ml-auto mr-auto">
					<h2 class="title">
						Where we play
					</h2>
				</div>
			</div>
			
			
			<div class="row">
				@forelse($locations as $location)
					@php
						$arbitrary=json_decode($location->arbitraryJSON);
					@endphp
					<div class="col-md-4">
						<div class="team-player">
							<div class="card card-plain">
								<div class="col-4 col-md-6 ml-auto mr-auto">
									<img src="{{ asset('storage/avatars/locations/' . $location->avatar) }}" 
										alt="{{$location->locationName}}" class="img-raised rounded-circle img-fluid" 
										style="width: 100%;">
								</div>
								<h4 class="card-title">{{$location->locationName}}
									<br>
									<small class="card-description text-muted">{{$location->address}}</small>
								</h4>
								@isset($location->description)
								<div class="card-body">
									<p class="card-description">{{ $location->description }}</p>
								</div>
								@endisset
								<div class="card-footer justify-content-center">
									@isset( $location->website )
										<a href="{{ (starts_with( $location->website ,'http') ? $location->website : 'http://'.$location->website) }}" class="btn btn-link btn-just-icon">
											<i class="fas fa-link"></i>
										</a>
									@endisset
									@if( !empty($arbitrary->social_instagram) )
										<a href="{{ $arbitrary->social_instagram }}" class="btn btn-link btn-just-icon"><i class="fab fa-instagram"></i></a>
									@endif
									@if( !empty($arbitrary->social_facebook) )
										<a href="{{ $arbitrary->social_facebook }}" class="btn btn-link btn-just-icon"><i class="fab fa-facebook-square"></i></a>
									@endif
									@if( !empty($arbitrary->social_twitter) )
										<a href="{{ $arbitrary->social_twitter }}" class="btn btn-link btn-just-icon"><i class="fab fa-twitter"></i></a>
									@endif

								</div>
							</div>
						</div>
					</div>
					<!-- end .col-md-4 -->
				@empty
				@endforelse

			</div>
			<!-- end .row -->
		</div>
		<!-- end .section -->
	</div>
	<!-- end .container -->
</div>
<!-- end .main -->


<div class="cd-container main-raised">
	<div class="map "id="map" style="overflow: hidden;">
	</div>
		
</div>
<!-- end .section -->

	


@stop



@section('js')
	<script>
		$(document).ready(function ()
		{
			var map;
			var elevator;
			var center = {lat: 43.6659661, lng: -79.3407657};
			var mapOptions = {
				zoom: 13,
				center: center,
				//mapTypeId: 'terrain'
			};
			var labels = '123456789';
			var labelIndex = 0;
			var map = new google.maps.Map(document.getElementById('map'), mapOptions);
			var infoWindow = new google.maps.InfoWindow();

		  	var locations = {
				@foreach ($locations as $loc)
				'{{$loc->locationName}}': { name: '{{$loc->locationName}}', lat: {{$loc->lat}}, lng:{{$loc->lng}}, address:'{{$loc->address}}' } @if(!$loop->last),@endif
				@endforeach
			}

			for (var key in locations)
			{
				if (!locations.hasOwnProperty(key)) continue;
				var loc = locations[key];
				//console.log( p.lat + " " + p.lng );
				var latlng = new google.maps.LatLng(loc.lat, loc.lng);
				var marker = new google.maps.Marker({
								position: latlng,
								map: map,
								label: {text: labels[labelIndex++ % labels.length], color: "white"}
							});

				//Attach click event to the marker.
				(function (marker, loc)
				{
					google.maps.event.addListener(marker, "click", function (e) {
						//Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
						infoWindow.setContent("<div style = 'width:140px;min-height:32px'><strong>" + loc.name + "</strong><br>" + loc.address +"</div>");
						infoWindow.open(map, marker);
					});
				})(marker, loc);

			}
		});
	</script>

	<script 
		src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.maps.api_key') }}">
	</script>
@stop