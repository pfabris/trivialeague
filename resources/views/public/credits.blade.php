@extends('layouts.materialkit-master')
@section('title','TTL | Credits')
@section('body_class','profile-page')

@section('body')

	@component('layouts.materialkit-topnav',
		[
			"otherSeasons" 	=> null,
			"season"		=> null,
			"league" 		=> $league
		])
	
	@endcomponent
	
	<div class="page-header header-filter" data-parallax="true" style=" background-image: url('/images/city-cityscape-amarpreet-kaur.jpg'); ">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h4 class="display-4">
						Credits & Acknowlegements
					</h4>
					<h5>
						A shoutout to everyone who helped build this site.
					</h5>
				</div>
			</div>
		</div>
	</div>
	
	<div class="main main-raised">
		
			<div class="section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="title text-center">Content</h2>
							<h5 class="description">Most of the site copy was pulled from the original Toronto Trivia League site, written by Peter Mathieson.</h5>
							
							<h2 class="title text-center">Graphics</h2>
							<h5 class="description">Marc Henderson, Lintburger Logo. <a href="https://twitter.com/appsbymarc?lang=en" target="_blank">@appsByMarc</a></h5>
							<h5 class="description">Amarpreet Kaur, photography. <a href="http://www.amarpreetkaur.com" target="_blank">http://www.amarpreetkaur.com</a></h5>
							<h5 class="description">Vecteezy, vector graphics. <a href="http://www.vecteezy.com" target="_blank">http://www.vecteezy.com</a></h5>
							
							<h2 class="title text-center">Frameworks</h2>
							<h5 class="description">Laravel PHP Framework. <a href="http://www.laravel.com" target="_blank">http://www.laravel.com</a></h5>
							<h5 class="description">Public Frontend: MaterialKit by Creative Tim. <a href="https://demos.creative-tim.com/material-kit/index.html" target="_blank">https://demos.creative-tim.com/material-kit/index.html</a></h5>
							<h5 class="description">Admin Backend: AdminLTE by Abdullah Almsaeed. <a href="https://adminlte.io" target="_blank">https://adminlte.io</a></h5>
							
							<h2 class="title text-center">Testing</h2>
							<h5 class="description">Thanks to Bill Wood, long time captain of Bettys 2, for guineapigging this.</h5>
							
							<p>&nbsp;</p>
						</div>
					</div>
				</div>
			</div>
		
	</div>
	


@stop
