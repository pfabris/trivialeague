@extends('layouts.materialkit-master')
@section('title','TTL | Standings')

@section('css')
	
@stop

@section('body_class','results-page')
@section('body')

	@component('layouts.materialkit-topnav',
		[
			"active_standings"	=> true,
			"season"			=> $season,
			"otherSeasons"		=> $otherSeasons,
			"league"			=> $league
		])
	@endcomponent
	
	<div class="page-header header-filter" data-parallax="true" style=" background-image: url('/images/city-cityscape-amarpreet-kaur.jpg'); ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
	                
                    <h4 class="display-4"> 
		                Standings: {{ $season->seasonName }}
	                </h4>
	                
                </div>
            </div>
        </div>
    </div>
	
	<div class="main main-raised">
        <div class="container">

	
	
	<!-- ######################  SEASON TEAM STANDINGS  ############################################################################ -->
	<div class="row" id="teamStandings">
			
			<div class="col-md-12">
			
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">
							<i class="fa fa-users"></i> Team Standings for {{ $season->seasonName }}
						</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table class="table table-striped table-sm">
							<tbody>
								<tr>
									<th class="text-center">Rank</th>
									<th>Team</th>
									<th class="text-center">Points</th>
									<th class="text-center">Total Deuces</th>
									<th class="text-center">Avg Deuces</th>
									<th class="text-center">Total Steals</th>
									<th class="text-center">Total Score</th>
									
								</tr>
								@php
									$lastNumberOfPoints = null;
									$rank = null;
								@endphp
								@forelse ($teamSeasonStandings as $team)
								@php
									if($team->teamPoints != $lastNumberOfPoints) { $rank=$loop->iteration; $lastNumberOfPoints = $team->teamPoints; }
								@endphp
								<tr>
									<td class="text-center"><p>{{$rank}}</p></td>
									<td><p>{{$team->teamName}}</p></td>
									<td class="text-center">
										<button class="btn btn-dark btn-sm">{{$team->teamPoints}}</button>
									</td>
									<td class="text-center">
										<span class="badge badge-success">{{$team->deuceCount}}</span>
									</td>
									<td class="text-center">
										@if($team->gamesTeamPlayed>0)
										<span class="badge badge-warning">{{ round($team->deuceCount / $team->gamesTeamPlayed , 0 ) }}</span>
										@endif
									</td>
									<td class="text-center">
										<span class="badge badge-rose">{{$team->stealsForCount}}</span>
									</td>
									<td class="text-center">
										<span class="badge badge-info">{{$team->teamTotalScore}}</span>
									</td>
									
								</tr>
								@empty
								<tr>
									<th colspan="7">
										No results yet ☹️
									</th>
								</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>	
			<!-- /.col-md-12 -->
	</div>
	<!-- /.row SEASON TEAM STANDINGS -->	
	
	
	
	<!-- ######################  SEASON PLAYER STANDINGS  ############################################################################ -->
	<div class="row mb-5" id="playerStandings">
		&nbsp;
	</div>
	
	<div class="row">
			
			<div class="col-md-12">
			
				<div class="card">
					<div class="card-header mb-3">
						<a href="{{ route('publicStandings',$season->id) . ($sort=='byPlayer' ? '?byTeam=true' : '') . '#playerStandings' }}"
							class="btn btn-sm btn-warning float-right">
							<i class="fas fa-sort"></i> Sort @if($sort=='byTeam') By Player @else ByTeam @endif
						</a>
						<h3 class="card-title">
							<i class="fa fa-user"></i> Player Standings for {{ $season->seasonName }}
						</h3>
						
					</div>
					<!-- /.card-header -->
					
					
					<div class="card-body table-responsive">
						<table class="table table-bordered table-striped table-sm table-hover">
							<tbody>
								
								<tr>
									<th class="text-center">Rank</th>
									<th>Player</th>
									<th class="text-center">Deuces</th>
									<th class="text-center">Average</th>
									<th class="text-center">Games Played</th>
									<th colspan="2">Team</th>
								</tr>
								
								
								@php
									$lastDeuceCount = null;
									$lastTeam		= null;
								@endphp
								@forelse ($playerSeasonStandings as $player)
								
								
									@if($sort=='byTeam' && $lastTeam!=$player->teamShortName)
								
										<tr >
											<th colspan="7">
												<div class="profile-photo-small float-left mr-2" style="height: 48px;">
													<img src="{{ asset('storage/avatars/teams/'.$player->teamAvatar) }}" class="rounded-circle img-thumbnail mh-100" alt="{{ $player->nameFull }} Avatar">
												</div>
												<p class="lead mt-2">{{$player->teamName}}</p>
											</th>
										</tr>
								
									@endif
								
									@php
										if( $player->deuceCount != $lastDeuceCount && $sort=='byPlayer' ) { $rank=$loop->iteration; $lastDeuceCount = $player->deuceCount; }
										else if ($sort!='byPlayer') { $rank= ""; }
										if( $lastTeam != $player->teamShortName) { $lastTeam = $player->teamShortName; }
									@endphp
									
									<tr>
										<td class="text-center">{{$rank}}</td>
										<td>
											<p><a href="{{ route('publicStandingsPlayer', [$season->id, $player->user_id]) }}">
											{{$player->nameFull}}
											</a>
											</p>
										</td>
										<td class="text-center">
											<span class="badge badge-success">
												{{$player->deuceCount}}
											</span>
										</td>
										<td class="text-center">
											<span class="badge badge-default">
												@if($player->gameCount>0)
												{{round($player->deuceCount/$player->gameCount,2)}}
												@endif
											</span>
										</td>
										<td class="text-center">
											<p>{{$player->gameCount}}</p>
										</td>
										<td>{{$player->teamShortName}}</td>
										<td>{{$player->primaryRole}}</td>
									</tr>
								@empty
									<tr>
										<th colspan="7">
											<p>No results yet ☹️</p>
										</th>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
					
					
				</div>
				<!-- /.card -->
			</div>	
			<!-- /.col-md-12 -->
	</div>
	<!-- /.row SEASON PLAYER STANDINGS -->
	
	</div>
	</div>
	
@stop