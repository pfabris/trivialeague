@extends('layouts.materialkit-master')
@section('title','TTL | Player Standings: ' . $player->nameFull . ' ('. $player->team->teamShortName.')' )

@section('css')
	
@stop

@section('body_class','results-page')
@section('body')

	@component('layouts.materialkit-topnav',
		[
			"active_standings"	=> true,
			"season"			=> $season,
			"otherSeasons"		=> $otherSeasons,
			"league"			=> $league
		])
	@endcomponent
	@php
		$avatar = ( empty($player->avatar)  ? asset('storage/avatars/users/default.jpg') : $player->avatar );
	@endphp
	<div class="page-header header-filter" data-parallax="true" style=" background-image: url('/images/city-cityscape-amarpreet-kaur.jpg'); ">
        <div class="container">
            <div class="row">
                <div class="col-12">
	                
                    <h4 class="display-4"> 
		                Player<span class="d-none d-sm-inline"> Standings</span>: {{ $season->seasonName }}
	                </h4>
	                
                </div>
            </div>
        </div>
    </div>
	
	<div class="main main-raised">
        <div class="container">

		
	
	<!-- ###################### PLAYER STANDINGS  ############################################################################ -->
	
	<div class="row" id="playerStandings">
			
			<div class="col-md-12">
			
				<div class="card">
					
					<!-- ######### WHO  ########### -->
					<div class="card-header  mb-3">
						<div class="profile-photo-small float-left mr-2" style="height: 60px;">
							<img src="{{$avatar}}" class="rounded-circle img-thumbnail mh-100" alt="{{ $player->nameFull }} Avatar">
						</div>
						<h3 class="card-title">
							{{ $player->nameFull }}
							<small>({{$player->team->teamName}}) Standings for {{ $season->seasonName }}</small>
						</h3>
						
					</div>
					<!-- /.card-header -->
					
					
					<!-- ######### Player's Season STANDINGS  ########### -->
					<div class="card-body mb-3 border-bottom">
						<div class="row text-center">
						@forelse($seasonTotals as $seasonTotal)
						
								
								<div class="col-md-2 col-4">
									<h5 class="">
										<button class="btn btn-default btn-raised btn-fab btn-round">{{$seasonTotal->gameCount}}</button>
										<br>Games
									</p>
								</div>
								<div class="col-md-2 col-4">
									<h5 class="">
										<button class="btn btn-success btn-raised btn-fab btn-round">{{$seasonTotal->totalDeuces}}</button>
										<br>Deuces
									</p>
								</div>
								<div class="col-md-2 col-4">
									<h5 class="">
										<button class="btn btn-info btn-sm" style="font-size:1.5rem; line-height: 1">{{ round($seasonTotal->totalDeuces/$seasonTotal->gameCount,2)}}</button>
										<br>Avg
									</p>
								</div>
								
						@empty
							No results yet 😐
						@endforelse
						</div>						
					</div>
					<!-- /.card-body -->
					
					
					<!-- ######### Player's Game by Game STANDINGS  ########### -->
						@php
							$lastDate=null;
							
						@endphp
						
						@forelse ($scores as $score)
							
							@if($score->date!=$lastDate)
								@php
									$lastDate=$score->date;
								@endphp
								<div class="card-body ">
									
									<div class="row">
										<div class="col-md-2 col-6">
											<p class="lead">
												<a href="{{ route('adminScorecard',$score->gameID) }}">
												{{ date('M j', strtotime($score->date)) }} 
												</a>
											</p>
										</div>
										<div class="col-md-1 col-3">
											<ul class="pagination pagination-success">
												<li class="active page-item">
												<a class="page-link">{{$score->totalDeuces}}</a>
												</li>
											</ul>
										</div>
										<div class="col-md-1 col-3">
											@isset($score->totalStealsAgainst)
											<ul class="pagination pagination-danger">
												<li class="active page-item">
													<a class="page-link font-weight-bold">{{$score->totalStealsAgainst}}</a>
												</li>
											</ul>
											@endisset
										</div>
										
										<div class="col-md-6 col-7">
											<p class="lead"><small class="text-muted"> @ </small> {{$score->locationName}} 
											<small class="text-muted">vs.</small> {{$score->opposingTeamName}}
											</p>
										</div>
										<div class="col-md-2 col-5">
											<button class="btn btn-sm btn-default" type="button" data-toggle="collapse" data-target="#{{$score->gameID}}" aria-expanded="false" aria-controls="{{$score->gameID}}">
												<i class="fas fa-angle-down"></i> Details
											</button>
										</div>
									</div>
									<!-- /.row -->
								</div>
								<!-- /.card-body -->
							
							
							
							
								<div class="card-body table-responsive collapse" id="{{$score->gameID}}">
									<table class="table table-bordered table-striped table-sm">
										<tbody>
											<tr>
												
												<th class="text">Round</th>
												<th class="text-center">Question</th>
												<th class="text-center">Deuce</th>
												<th class="text-center">Steals Against</th>
												
											</tr>
											
							@endif
							
							
											<tr>
												<td class="">
													{{$score->roundNumber}}: {{$score->title}}
												</td>
												<td class="text-center">
													<span data-toggle="tooltip" data-placement="top" title="" data-container="body" data-original-title="{{$score->question}}">
													{{$score->questionNumber}}</span>
												</td>
												<td class="text-center">
													<span class="badge badge-success">{{$score->countDeuce}}</span>
												</td>
												<td class="text-center">
													<span class="badge badge-danger">{{$score->countStealAgainst}}</span>
												</td>										
											</tr>
											
							@if($score->roundNumber==10)											
										</tbody>
									</table>
								</div>
								<!-- /.card-body -->
								
								
							@endif
							
						@empty
							<p class="lead">No results yet ☹️</p>
						@endforelse
						
				</div>
				<!-- /.card -->
			</div>	
			<!-- /.col-md-12 -->
	</div>
	<!-- /.row PLAYER STANDINGS -->
	
	</div>
	</div>
	
@stop