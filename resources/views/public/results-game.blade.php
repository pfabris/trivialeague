@extends('layouts.materialkit-master')
@section('title','TTL | Results')

@section('css')

@stop

@section('body_class','results-page')
@section('body')

	@component('layouts.materialkit-topnav',
		[
			"active_results"	=> true,
			"season"			=> $season,
			"otherSeasons"		=> $otherSeasons,
			"league"			=> $league
		])
	@endcomponent

	<div class="page-header header-filter" data-parallax="true" style=" background-image: url({{ asset('storage/banners/locations/'. $gameTeamResults[0]->banner) }}); ">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					<h4 class="display-4">
						{{ date('M j', strtotime( $gameNight->date )) }}, Game {{ $gameNight->gameNumber }}
					</h4>
					<h5>{{$season->seasonName}}</h5>
					
				</div>
			</div>
		</div>
	</div>

	<!-- ######################  GAME DATE NAVIGATOR  ############################################################################ -->
	<div class="main main-raised">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="card text-center">

						<div class="card-header" style="">

							<ul class="nav nav-pills card-header-pills">
								@forelse($games as $game)

									<li class="nav-item">
										<a class="nav-link @if($game->gameID==$gameID) active @endif"
											data-toggle="tooltip" data-placement="bottom" title="{{$game->team1Name}} @ {{$game->team2Name}}"
											href="{{ route('publicResultsGame', [$season->id, $gameNight->id, $game->gameID]) }}">
												{{ $game->locationName }}
												<div class="ripple-container"></div>
										</a>
									</li>
								@empty
									<li class="nav-item">
										No Games setup yet ☹️
									</li>
								@endforelse
							</ul>
						</div>


					</div>
					<!-- /.card -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.container GAME DATE NAVIGATOR -->



		<!-- ######################  GAME RESULTS  ############################################################################ -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="box mb-5">
	
						<div class="box-body table-responsive-md table-sm">
							<table class="table">
								<tbody>
	
									<!-- TEAM HEADER -->
									<tr>
										<th colspan="2"></th>
	
										<!-- TEAM 1 -->
										<th colspan="6" class="teamCell text-left">
											<span id="t1_TeamName">
												<i id="t1_trophy" class="fa fa-fw fa-trophy"></i> {{ $gameTeamResults[0]->teamName }}
											</span>
										</th>
	
										<th></th>
	
										<!-- TEAM 2 -->
										<th colspan="6" class="teamCell text-left">
											<span id="t2_TeamName">
												<i id="t2_trophy" class="fa fa-fw fa-trophy"></i> {{ $gameTeamResults[1]->teamName }}
											</span>
										</th>
									</tr>
	
									<!-- PLAYER HEADER -->
									<tr>
										<th style="width: 10px">#</th>
										<th class="text-left">Round</th>
	
										@forelse ($playersTeam1 as $player)
											<th id="seat{{ $loop->iteration }}" class="playerCell">{{ $player->nameFull }}</th>
										@empty
											<th colspan="5"></th>
										@endforelse
										<th class="playerCell" style="background-color: #e1e1e1;">Total</th>
	
										<th style="width: 1px;"></th>
	
										@forelse ($playersTeam2 as $player)
											<th id="seat{{ $loop->iteration + 5 }}" class="playerCell">{{ $player->nameFull }}</th>
										@empty
											<th colspan="5"></th>
										@endforelse
										<th class="playerCell" style="background-color: #e1e1e1;">Total</th>
									</tr>
	
	
	
									<!-- SCORES GRID -->
									@php
										$i=0; // Special Counter for gameTeamRound, incremented on 5th and 10th iterations
									@endphp
									@forelse ($scores as $score)
	
										@if ($loop->index % 10 == 0)
										<tr>
										@endif
	
											@if ($loop->index % 10 == 0)
												<!-- ROUND TITLE -->
												<td class="text-left">
													{{ $score->roundNumber }}
												</td>
	
												<td class="text-left">
													{{ $score->title }}
												</td>
											@endif
	
											<!-- SCORE CELL -->
											<td
												@if ($score->score == 2) class="scoreCell noselect bg-success"
												@elseif ($score->score == 1) class="scoreCell noselect bg-warning"
												@elseif ($score->score == 0) class="scoreCell noselect bg-default"
												@elseif ($score->score == "-1") class="scoreCell noselect bg-danger"
												@else class="scoreCell noselect bg-secondary"
												@endif
												id="{{$score->scoreID}}"
	
												@can('view-questionsAndAnswers')
													title="Q: {{ $score->question }}
													A: {{ $score->answer }}"
												@endcan
	
											>
												@if( $score->score !==null ) {{ $score->scoreValue }} @endif
											</td>
	
	
											@if ($loop->index % 10 == 4 || $loop->index % 10 == 9)
												<!-- ROUND TOTAL CELL for TEAM -->
												<td id="roundTotal_t{{ $gameTeamRounds[$i]->teamPosition }}_{{ $gameTeamRounds[$i]->roundNumber }}" class="round_total">
	
													{{ $gameTeamRounds[$i]->totalScore }}
	
												</td>
												@php
													$i++;
												@endphp
											@endif
	
											@if ($loop->index % 10 == 4 )
												<!-- GUTTER -->
												<td class="gutter">
	
												</td>
											@endif
	
										@if ($loop->index % 10 == 9)
										</tr>
										@endif
	
									@empty
										<tr><td colspan="15"><p>No Results yet ☹️</td></tr>
									@endforelse
	
	
	
									<!-- BOTTOM ROW - TEAM PLAYER TOTALS AND TEAM TOTAL -->
									<tr>
										<td colspan="2"></td>
	
										@forelse ($playersTeam1 as $player)
											<!-- TEAM 1 -->
											<td style="text-align:center" id="{{$player->gamePlayerID}}" class="round_total">
												{{ $player->deuceCount }}
											</td>
										@empty
											<td colspan="5"></td>
										@endforelse
	
										<!-- TEAM 1 TOTAL -->
										<td
											@if ($gameTeamResults[0]->gameTotalScore > $gameTeamResults[1]->gameTotalScore) class="bg-success round_total"
											@elseif ($gameTeamResults[0]->gameTotalScore < $gameTeamResults[1]->gameTotalScore) class="bg-danger round_total"
											@else class="bg-default round_total"
											@endif
											id="t1_TotalScore"
										>
											{{ $gameTeamResults[0]->gameTotalScore }}
	
										</td>
	
	
										<td style="width: 1px;"></td>
	
	
										@forelse ($playersTeam2 as $player)
										<!-- TEAM 2 -->
										<td style="text-align:center" id="{{$player->gamePlayerID}}" class="round_total">
											{{ $player->deuceCount }}
										</td>
										@empty
										@endforelse
	
										<!-- TEAM 2 TOTAL -->
										<td style="text-align:center"
											@if ($gameTeamResults[0]->gameTotalScore < $gameTeamResults[1]->gameTotalScore) class="bg-success round_total"
											@elseif ($gameTeamResults[0]->gameTotalScore > $gameTeamResults[1]->gameTotalScore) class="bg-danger round_total"
											@else class="bg-default round_total"
											@endif
											id="t2_TotalScore"
										>
											{{ $gameTeamResults[1]->gameTotalScore }}
										</td>
	
									</tr>
	
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
	
				</div>
				<!-- /.col-md-12 -->
			</div>
			<!-- /.row GAME RESULTS -->
		</div>
		<!-- /.container GAME RESULTS -->










	</div>
	<!-- /.main -->


@stop



@section('js')

	<script type="text/javascript">

		var game_ID	= "{{ $gameID }}";

		Echo.channel('broadcastScore')
		.listen('.scoreUpdated', (payload) =>
			{
				//console.log(payload.broadcastData);
				updateScoreCell(
					payload.broadcastData.scoreID,
					payload.broadcastData.score
				);
				updateGameTeamRoundTotals(
					payload.broadcastData.teamPosition,
					payload.broadcastData.roundNumber,
					payload.broadcastData.roundScore,
					payload.broadcastData.stealsAgainst,
					payload.broadcastData.opponentRoundScore,
					payload.broadcastData.opponentStealsAgainst
				);
				updateGamePlayerTotals(
					payload.broadcastData.gamePlayerID, payload.broadcastData.deuceCount
				);
				updateGameTotals();
			}
		);


		$( document ).ready(function() {
			updateGameTotals();
		});

		function updateScoreCell (cell_id, score, roundTotalScore)
		{
			newClass = getBGColorCell(score);
			$("#"+cell_id).text(score).attr( "class", "scoreCell noselect " + newClass );
		}

		function updateGameTeamRoundTotals (teamPosition, roundNumber, roundScore, stealsAgainst, opponentRoundScore, opponentStealsAgainst)
		{
			var teamRoundScore = roundScore + opponentStealsAgainst;
			var opposingTeamPosition = (teamPosition == 2 ? 1 : 2);
			var opposingRoundScore = stealsAgainst + opponentRoundScore;
			$("#roundTotal_t" + teamPosition + "_" + roundNumber).text(teamRoundScore);
			$("#roundTotal_t" + opposingTeamPosition + "_" + roundNumber).text(opposingRoundScore);

		}

		function updateGameTotals()
		{
			$.ajax( {url: "/ajax/games/game/"+game_ID+"/totals", cache: false } )
			.done(function(response)
			{

				newClassT1	= "default";
				newClassT2	= "default";

				t1Score = parseInt(response[0].TotalScore);
				t2Score = parseInt(response[1].TotalScore);

				if(t1Score > t2Score) {
					newClassT1 = "success";
					newClassT2 = "danger";
					$("#t1_trophy").show();
					$("#t2_trophy").hide();
				} else if (t1Score < t2Score) {
					newClassT1 = "danger";
					newClassT2 = "success";
					$("#t1_trophy").hide();
					$("#t2_trophy").show();
				}
				else {
					$(".fa-trophy").hide();
				}

				$("#t1_TotalScore").text(response[0].TotalScore).attr( "class", "round_total bg-" + newClassT1 );
				$("#t2_TotalScore").text(response[1].TotalScore).attr( "class", "round_total bg-" + newClassT2 );
				$("#t1_TeamName").attr( "class", "text-" + newClassT1 );
				$("#t2_TeamName").attr( "class", "text-" + newClassT2 );

			})
			.fail(function(response, status, error) {
				console.log( response );
			})

		}

		function updateGamePlayerTotals (gamePlayerID, deuceCount)
		{
			$("#"+gamePlayerID).text(deuceCount);
		}

		function getBGColorCell(score)
		{
			newClass = "bg-light";
			if(score == 2) 			{ newClass="bg-success"; }
			else if(score == 1) 	{ newClass="bg-warning"; }
			else if(score == -1) 	{ newClass="bg-danger";  }
			else if(score == 0) 	{ newClass="bg-default"; }
			else  { newClass="bg-light"; }
			return newClass;
		}
	</script>
@stop