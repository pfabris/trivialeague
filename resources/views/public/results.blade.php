@extends('layouts.materialkit-master')
@section('title','TTL | Results')

@section('css')

@stop

@section('body_class','results-page')
@section('body')

	@component('layouts.materialkit-topnav',
		[
			"active_results"	=> true,
			"season"			=> $season,
			"otherSeasons"		=> $otherSeasons,
			"league"			=> $league
		])
	@endcomponent
    
    <div class="page-header header-filter" data-parallax="true" style=" background-image: url('/images/city-cityscape-amarpreet-kaur.jpg'); ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
	                
                    <h4 class="display-4">
	                    {{ date('M j', strtotime( $gameNight->date )) }}, Game {{ $gameNight->gameNumber }}
	                </h4>
	                <h4>{{$season->seasonName}}</h4>
	                
                </div>
            </div>
        </div>
    </div>
    
    <div class="main main-raised">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
			
					<div class="card text-left">
						
						<div class="card-header">
							<ul class="nav">
								@forelse($gameNights as $thisGameNight)
								
								<li class="nav-item">
									<a class="nav-link @if( $thisGameNight->id == $gameNight->id ) btn btn-primary @endif"
										href="{{ route('publicResults', [$season->id, $thisGameNight->id]) }}">
											{{ date('M j', strtotime( $thisGameNight->date )) }}
										</a>
								</li>
								@empty
								
								@endforelse
							</ul>
					  	</div>
					  	
						
					  	
					  	<div class="card-body">
					  		<div class="row">
						  		@forelse($games as $game)
							  		@if($loop->index % 2 == 0)
									<div class="col-md-4">
										
										
										<div class="card card-nav-tabs" >
											<img class="card-img-top" height="100px" src="{{ asset('storage/banners/locations/'. $game->banner) }}" alt="{{$game->teamName}} Banner">
											<div class="card-header card-header-primary">
												<a href="{{route('publicResultsGame',[ $season->id, $gameNight->id, $game->gameID ])}}" class="btn btn-secondary btn-sm float-right" role="button" aria-disabled="true">Scorecard</a>
												<h5 class="title" style="margin: 2px 0px 3px 0px">
													{{$game->locationName}}
												</h5>
												
											</div>
											<div class="card-body">
												
												<div class="row text-center">
												
													<div class="col-md-6" style="padding-left:0px; padding-right:2px">
														<span class="badge 
														@if($game->countWin==1)badge-success @elseif($game->countTie==1) badge-default @else badge-danger @endif">
														{{$game->totalScore}}</span><br>
														<strong>{{$game->teamName}}</strong>
													</div>
									
									@else			
													<div class="col-md-6" style="padding-left:2px; padding-right:0px">
														<span class="badge 
														@if($game->countWin==1)badge-success @elseif($game->countTie==1) badge-default @else badge-danger @endif">
														{{$game->totalScore}}</span><br>
														<strong>{{$game->teamName}}</strong>
													</div>
												</div>
												
											</div>
											
										</div>
	
										
							  		</div>
							  		@endif
						  		@empty
								@endforelse
					  		</div>
						</div>

					</div>
				
					
				</div>
			</div>
            
        </div>
    </div>
    


@stop



@section('js')
	
@stop