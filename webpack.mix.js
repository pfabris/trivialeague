let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 | npm run dev
 | npm run production
 | 
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
	'node_modules/sortablejs/Sortable.min.js'
], 'public/js/vendor.js');

mix.js('resources/assets/js/app-public.js', 'public/js/material-kit');
