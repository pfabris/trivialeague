<?php

    /**
     * Configuration for the "HTTP Very Basic Auth"-middleware
     */
    return [
        // Username
        'user'              => env('API_AUTHUSER'),

        // Password
        'password'          => env('API_AUTHPASS'),

        // Environments where the middleware is active
        'envs'              => [
            'local',
            'dev',
            'staging',
            'production',
        ],

        // Message to display if the user "opts out"/clicks "cancel"
        'error_message'     => 'Access denied.',

        // If you prefer to use a view with your error message you can uncomment "error_view".
        // This will superseed your default response message
        // 'error_view'        => 'very_basic_auth::default'
    ];
