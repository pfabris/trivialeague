<?php
	
	/*
    |--------------------------------------------------------------------------
    | TRIVIALEAGUE APP ENV VARIABLES
    |--------------------------------------------------------------------------
    |
    | Collect Trivialeague Specific values from the .env file
    | this allows us to use "php artisan config:cache"
    | 
    |
    */

return [
	
	'league_id' 			=> env('LEAGUE_ID'),

];