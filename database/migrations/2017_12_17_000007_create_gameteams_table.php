<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gameteams', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->uuid('game_id')->nullable();
			$table->uuid('team_id')->nullable();
			$table->enum('teamPosition',[1,2])->nullable();
			$table->tinyint('countWin')->nullable();
			$table->tinyint('countTie')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gameteams');
    }
}
