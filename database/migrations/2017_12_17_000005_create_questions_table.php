<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
            $table->tinyInteger('questionNumber')->nullable()->unsigned();
            $table->text('question')->nullable();
            $table->text('notes')->nullable();
            $table->text('answer')->nullable();
            $table->string('mediaFileName')->nullable();
            $table->string('mediaMIMEType')->nullable();
            $table->decimal('averageScore')->nullable();
        	$table->uuid('round_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
