<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTeamRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gameteamrounds', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->uuid('gameteam_id')->nullable();
			$table->uuid('round_id')->nullable();
			$table->tinyInteger('roundNumber')->unsigned()->nullable();
			$table->tinyInteger('totalScore')->unsigned()->nullable();
			$table->tinyInteger('totalStealsFor')->unsigned()->nullable();
			$table->tinyInteger('totalDeuces')->unsigned()->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gameteamrounds');
    }
}
