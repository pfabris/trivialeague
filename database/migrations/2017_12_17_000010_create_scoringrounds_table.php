<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoringRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scoringrounds', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->uuid('gameplayer_id')->nullable();
			$table->uuid('round_id')->nullable();
			$table->uuid('question_id')->nullable();			
			$table->tinyInteger('questionNumber')->unsigned()->nullable();
			$table->tinyInteger('roundNumber')->unsigned()->nullable();
			$table->tinyInteger('totalScore')->unsigned()->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scoringrounds');
    }
}
