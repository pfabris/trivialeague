<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->enum('primaryRole',['League Manager','Author','Quiz Master','Captain','Regular','Guest','Team','Location Contact','Administrator'])->default('Guest');
			$table->enum('type',['Human','Role Account'])->default('Human');
			$table->enum('status',['Active','Inactive','Retired'])->default('Active');
			$table->string('nameFirst',60)->nullable();
			$table->string('nameLast',60)->nullable();
			$table->string('nameFull',120)->nullable();
			$table->string('address',120)->nullable();
			$table->string('city',60)->nullable();
			$table->char('province',2)->nullable();
			$table->char('postalCode',10)->nullable();
			$table->string('phoneMobile',20)->nullable();
			$table->string('email')->unique();
			$table->string('password')->nullable();
			$table->string('avatar')->nullable();
			$table->date('dateJoined')->nullable();
			$table->enum('notificationPreference',['None','Email','SMS'])->default('None')->nullable();
			$table->text('optionsJSON')->nullable();
			$table->uuid('team_id')->nullable();
			$table->uuid('league_id')->nullable();
			//$table->foreign('team_id')->references('id')->on('teams');
			
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
