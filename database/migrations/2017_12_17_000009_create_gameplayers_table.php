<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gameplayers', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->uuid('gameteam_id')->nullable();
			$table->uuid('user_id')->nullable()->comment("The Player ID");
			$table->tinyInteger('seat')->unsigned()->nullable();
			$table->tinyInteger('totalDeuces')->unsigned()->nullable();
			$table->tinyInteger('totalStealsAgainst')->unsigned()->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gameplayers');
    }
}
