<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaguesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leagues', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->string('leagueName')->unique();
			$table->string('neighbourhood',60)->nullable();
			$table->string('city',60)->nullable();
			$table->char('province',2)->nullable();
			$table->char('country',2)->nullable();
			$table->tinyInteger('gameNightDayOfWeek')->unsigned()->default(1)->comment('Monday = 1, Sunday=7');
			$table->time('gameNightStartTime')->default('20:00:00')->comment('Default Game Start Time');
			$table->tinyInteger('roundsPerGame')->unsigned()->default(10);
			$table->tinyInteger('questionsPerRound')->unsigned()->default(10);			
			$table->tinyInteger('seatsPerGame')->unsigned()->default(10);
			$table->tinyInteger('swapQuestionOrder2ndHalf')->unsigned()->default(1);
			$table->text('arbitraryJSON')->nullable();
			$table->uuid('user_id')->nullable()->comment("The UserID of the League Manager");
			//$table->foreign('user_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leagues');
	}
}
