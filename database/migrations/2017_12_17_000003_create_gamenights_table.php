<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameNightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamenights', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
            $table->tinyInteger('gameNumber')->unsigned()->nullable();
            $table->date('date')->nullable();
        	$table->uuid('season_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gamenights');
    }
}
