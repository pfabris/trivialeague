<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teams', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->string('teamName',120);
			$table->string('teamShortName',30)->unique();
			$table->string('avatar')->nullable();
			$table->uuid('location_id')->nullable();
			$table->uuid('user_id')->nullable()->comment("The ID of the Captain");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teams');
	}
}
