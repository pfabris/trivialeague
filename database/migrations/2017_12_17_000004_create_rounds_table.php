<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rounds', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->tinyinteger('roundNumber')->unsigned()->nullable();
            $table->decimal('averageScore')->nullable();
        	$table->uuid('gamenight_id')->nullable();
        	$table->uuid('user_id')->nullable()->comment("Author of the round.");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rounds');
    }
}
