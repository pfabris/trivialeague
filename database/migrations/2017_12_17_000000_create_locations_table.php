<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->string('locationName')->unique();
			$table->string('address',120)->nullable();
			$table->string('city',60)->nullable();
			$table->char('province',2)->nullable();
			$table->char('postalCode',10)->nullable();
			$table->string('website')->nullable();
			$table->string('phone',20)->nullable();
			$table->string('email')->nullable();
			$table->string('avatar')->nullable();
			$table->string('banner')->nullable();
			$table->decimal('lat',10,8)->nullable();
			$table->decimal('lng',11,8)->nullable();
			$table->text('arbitraryJSON')->nullable();
			$table->uuid('league_id')->nullable();
			//$table->foreign('league_id')->references('id')->on('leagues');
			$table->uuid('user_id')->nullable()->comment("The ID of the Location Contact");
			//$table->foreign('user_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}
}
