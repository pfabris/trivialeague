<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->uuid('location_id')->nullable();
			$table->uuid('gamenight_id')->nullable();
			$table->uuid('user_id')->nullable()->comment("The Quiz Master");
			$table->text('notes')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
    }
}
