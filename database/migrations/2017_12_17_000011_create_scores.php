<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
			$table->uuid('id')->unique();
			$table->primary('id');
			$table->uuid('scoringround_id')->nullable(); // DEPRECATED
			$table->uuid('gameplayer_id')->nullable();
			$table->uuid('round_id')->nullable();
			$table->uuid('question_id')->nullable();
			$table->tinyInteger('sequence')->unsigned()->nullable()->comment("Absolute sequence number of this score in the game");
			$table->tinyInteger('roundNumber')->unsigned()->nullable()->comment("Absolute sequence number of this score in the game");
			$table->tinyInteger('questionNumber')->unsigned()->nullable()->comment("Absolute sequence number of this score in the game");
			$table->tinyInteger('score')->nullable()->comment("-1 = Steal, 0 = No Answer, 1 = Team Answer, 2 = Deuce");
			$table->tinyInteger('scoreValue')->unsigned()->nullable()->comment("0, 1 for Team, 2 for Deuce");
			$table->tinyInteger('countDeuce')->unsigned()->nullable()->comment("1 for Deuce");
			$table->tinyInteger('countTeam')->unsigned()->nullable()->comment("1 for Team");
			$table->tinyInteger('countStealAgainst')->unsigned()->nullable()->comment("1 for a Stolen Answer");
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scores');
    }
}
