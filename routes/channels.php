<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// This is Laravel boilerplate and not currently used
Broadcast::channel('App.User.{id}', function ($user, $id) {
    return $user->id === $id;
});

// This channel is used by Events/ScoreToggled
Broadcast::channel('broadcastScore', function () {
    return true;
});
