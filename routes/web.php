<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/******************************************* PUBLIC ROUTES */

/* WEB ROUTES -- PUBLIC */
Route::get('/',			'PublicController@generateViewIndex')->name('publicHome');
Route::get('/credits',	'PublicController@generateViewCredits')->name('publicCredits');


Route::get('/results/{seasonID?}/{gamenightID?}', 			'PublicController@generateViewResults')->name('publicResults');
Route::get('/results/{seasonID?}/{gamenightID?}/{gameID}', 	'PublicController@generateViewResultsGame')->name('publicResultsGame');
Route::get('/schedule/{seasonID?}/{gamenightID?}',			'PublicController@generateViewSchedule')->name('publicSchedule');
Route::get('/standings/{seasonID?}',  			  			'PublicController@generateViewStandings')->name('publicStandings');
Route::get('/standings/{seasonID?}/{playerID}',				'PublicController@generateViewStandingsPlayer')->name('publicStandingsPlayer');


/* AJAX ROUTES -- PUBLIC */
Route::get( '/ajax/games/game/{game_ID}/totals', 	'GameController@getGameTotals')->name('publicAjaxGetGameTotals');


/******************************************* ADMIN ROUTES */

/* WEB ROUTES -- AUTHENTICATED -- ADMIN */
Auth::routes();
Route::get('/admin',								'AdminController@generateViewHome')->name('adminHome');
Route::get('/admin/seasons',						'AdminController@generateViewSeasons')->name('adminSeasons');
Route::get('/admin/seasons/currentSeason/gamenights','AdminController@currentSeason')->name('adminCurrentSeason');
Route::get('/admin/seasons/{seasonID}/gamenights',	'AdminController@generateViewSeason')->name('adminSeason');

Route::get('/admin/gamenights/current',				'AdminController@currentGameNight')->name('adminCurrentGameNight');
Route::get('/admin/gamenights/{gameNightID}',		'AdminController@generateViewGameNight')->name('adminGameNight');
Route::get('/admin/gamenights/{gameNightID}/setup',	'AdminController@generateViewGameNightSetup')->name('adminGameNightSetup');

Route::get('/admin/games/{gameID}',					'AdminController@generateViewInitGame')->name('adminGameSetup');
Route::get('/admin/games/{gameID}/scorecard',		'AdminController@generateViewGameScoreCard')->name('adminScorecard');

Route::get('/admin/league',							'AdminController@generateViewLeague')->name('adminLeague');
Route::get('/admin/league/rounds/submit',			'AdminController@generateViewRoundsSubmit')->name('adminLeagueSubmitRound');
Route::get('/admin/league/rounds/my',				'AdminController@generateViewMyRounds')->name('adminLeagueMyRounds');

Route::get('/admin/league/users',					'AdminController@generateViewLeagueUsers')->name('adminLeagueUsers');
Route::get('/admin/league/users/{userID}',			'AdminController@generateViewLeagueUser')->name('adminLeagueUser');

Route::get('/admin/locations',						'AdminController@generateViewLocations')->name('adminLocations');
Route::get('/admin/locations/{locationID}',			'AdminController@generateViewLocation')->name('adminLocation');
Route::get('/admin/locations/{locationID}/teams',	'AdminController@generateViewLocationTeams')->name('adminLocationTeams');

Route::get('/admin/teams',							'AdminController@generateViewTeams')->name('adminTeams');
Route::get('/admin/teams/{teamID}',					'AdminController@generateViewTeam')->name('adminTeam');

/* WEB ROUTES -- AUTHENTICATED -- PROFILE */
Route::get('/profiles/users/{id}',					'ProfileController@generateViewProfileUser')->name('profileUser');
Route::get('/profiles/teams/{id}',					'ProfileController@generateViewProfileTeam')->name('profileTeam');


/* FORM RESPONSES -- AUTHENTICATED -- ADMIN */
Route::patch('/admin/league/{leagueID}/update', 	 	 'League@updateLeague')->name('adminLeagueUpdate');
Route::patch('/admin/league/users/{userID}/update', 	 'UserController@updateUser')->name('adminUserUpdate');
Route::delete('/admin/league/users/{userID}/delete', 	 'UserController@deleteUser')->name('adminUserDelete');
Route::patch('/admin/gamenights/round/{roundID}/update', 'RoundController@updateRound')->name('adminRoundUpdate');

/* FORM RESPONSES -- AUTHENTICATED -- PROFILE */
Route::patch('/profiles/users/{id}/update', 	 		 'UserController@updateUserProfile')->name('profileUserUpdate');

/* AJAX ROUTES -- AUTHENTICATED */
Route::group(['middleware' => 'auth'], function() 
{
	
	// Add Season
	Route::post( '/ajax/admin/seasons/add', 						'SeasonController@store')->name('adminAjaxAddSeason');
	Route::post( '/ajax/admin/rounds/submit',						'RoundController@submitRound')->name('adminAjaxSubmitRound');
	
	// Update Rounds
	Route::patch( '/ajax/admin/gamenights/{id}/rounds/update',		'RoundController@updateGameNightRounds')->name('adminAjaxUpdateRounds');
	Route::post( '/ajax/admin/leagues/{id}/round/submit',			'RoundController@submitRound')->name('adminAjaxSubmitRound');
	
	// Setup GameNight
	Route::post( '/ajax/admin/gamenights/{id}/setup',				'GameNightController@setupGameNight')->name('adminAjaxGameNightSetup');
	// Setup Game
	Route::get( '/ajax/admin/teams/{id}/users',						'TeamController@indexTeamMates')->name('adminAjaxTeamMembers');
	Route::get( '/ajax/admin/gameteams/{gameteam_id}/teammates',	'GameTeamController@indexTeamMates')->name('adminAjaxGameTeamMembers');
	Route::post('/ajax/admin/game-setup',							'GameController@setupGame')->name('adminAjaxGameSetup');
	
	// ScoreCard
	Route::get( '/ajax/admin/games/{game_ID}/score/{cell_id}',		'GameController@toggleScore')->name('adminAjaxToggleScore');
	Route::get( '/ajax/admin/games/{game_ID}/swapTeamPositions',	'GameController@swapTeamPositions')->name('adminAjaxSwapTeamPositions');
	Route::post('/ajax/admin/games/{game_ID}/changePlayer',			'GameController@changePlayer')->name('adminAjaxChangePlayer');
	Route::get( '/ajax/admin/games/{game_ID}/finishGame',			'GameController@finishGame')->name('adminAjaxFinishGame');
	Route::get( '/ajax/admin/games/{game_ID}/resetGame',			'GameController@resetGame')->name('adminAjaxResetGame');
	Route::get( '/ajax/admin/games/{game_ID}/unlockGame',			'GameController@unlockGame')->name('adminAjaxUnlockGame');
	
	Route::post('/ajax/admin/users',								'UserController@store')->name('adminAjaxAddUser');
});


/* INVITATIONS */  
// https://laravel-news.com/user-invitation-system
Route::post('/admin/invite', 			'InviteController@process')->name('processInvitation');
Route::get('/acceptInvitation/{token}', 'InviteController@accept')->name('acceptInvitation'); // {token} is a required parameter that will be exposed to us in the controller method
Route::get('/requestInvitation', 		'InviteController@requestInvite')->name('requestInvitation');
Route::post('/requestInvitation', 		'InviteController@processRequestInvite')->name('requestInvitationSubmit');



/******************************************* TEST ROUTES */
// Disable for production
/*
Route::get('/test', function () {
	return view('test');
});
*/

/*
Route::get('/mailable', function () {
    $invite = App\Models\Invite::find("07877492-6F8B-4907-AFB3-0ED1266A6D2B");
    return new App\Mail\InviteCreated($invite);
});
*/

