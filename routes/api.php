<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth.very_basic'], function() {


	/* Teams */
	Route::get('teams',					'TeamController@index');
	
	
	/* GamesNights */
	Route::get('gamenights',				'GameNightController@index');
	Route::get('gamenights/{gameNight}',	'GameNightController@show');
	Route::post('gamenights',				'GameNightController@store');
	Route::put('gamenights/{gameNight}',	'GameNightController@update');
	Route::delete('gamenights/{gameNight}',	'GameNightController@delete');	
	
	/* Rounds */
	Route::get('rounds',				'RoundController@index');
	Route::get('rounds/{round}',		'RoundController@show');
	Route::post('rounds',				'RoundController@store');
	Route::put('rounds/{round}',		'RoundController@update');
	Route::delete('rounds/{round}',		'RoundController@delete');	
	
	/* Questions */
	Route::get('questions',					'QuestionController@index');
	Route::get('questions/{question}',		'QuestionController@show');
	Route::post('questions',				'QuestionController@store');
	Route::post('questions/multiple',		'QuestionController@storeMultiple');
	Route::put('questions/{question}',		'QuestionController@update');
	Route::delete('questions/{question}',	'QuestionController@delete');	
	
	/* Games */
	Route::get('games',				'GameController@index');
	Route::get('games/{game}',		'GameController@show');
	Route::post('games',			'GameController@store');
	Route::put('games/{game}',		'GameController@update');
	Route::delete('games/{game}',	'GameController@delete');
	
	/* GameTeams */
	Route::get('gameteams',					'GameTeamController@index');
	Route::get('gameteams/{gameTeam}',		'GameTeamController@show');
	Route::post('gameteams',				'GameTeamController@store');
	Route::post('gameteams/multiple',		'GameTeamController@storeMultiple');
	Route::put('gameteams/{gameTeam}',		'GameTeamController@update');
	Route::delete('gameteams/{gameTeam}',	'GameTeamController@delete');
	
	/* GameTeamRounds */
	Route::get('gameteamrounds',					'GameTeamRoundController@index');
	Route::get('gameteamrounds/{gameTeamRound}',	'GameTeamRoundController@show');
	Route::post('gameteamrounds',					'GameTeamRoundController@store');
	Route::post('gameteamrounds/multiple',			'GameTeamRoundController@storeMultiple');
	Route::put('gameteamrounds/{gameTeamRound}',	'GameTeamRoundController@update');
	Route::delete('gameteamrounds/{gameTeamRound}',	'GameTeamRoundController@delete');
	
	/* GamePlayers */
	Route::get('gameplayers',						'GamePlayerController@index');
	Route::get('gameplayers/{gamePlayer}',			'GamePlayerController@show');
	Route::post('gameplayers',						'GamePlayerController@store');
	Route::put('gameplayers/{gamePlayer}',			'GamePlayerController@update');
	Route::delete('gameplayers/{gamePlayer}',		'GamePlayerController@delete');
	
	/* ScoringRounds */
	Route::get('scoringrounds',						'ScoringRoundController@index');
	Route::get('scoringrounds/{scoringRound}',		'ScoringRoundController@show');
	Route::post('scoringrounds',					'ScoringRoundController@store');
	Route::put('scoringrounds/{scoringRound}',		'ScoringRoundController@update');
	Route::delete('scoringrounds/{scoringRound}',	'ScoringRoundController@delete');
	
	/* Scores */
	Route::get('scores',				'ScoreController@index');
	Route::get('scores/{score}',		'ScoreController@show');
	Route::post('scores',				'ScoreController@store');
	Route::post('scores/multiple',		'ScoreController@storeMultiple');
	Route::put('scores/{score}',		'ScoreController@update');
	Route::delete('scores/{score}',		'ScoreController@delete');

});