<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Invite;
use App\Models\User;
use App\Models\League;


class InviteCreated extends Mailable
{
    use Queueable, SerializesModels;
	
	public $invite;
	public $league;
	public $user;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invite $invite)
    {
        $this->invite 	= $invite;
        $this->league 	= League::find(config('trivialeague.league_id'));
        $this->user 	= $invite->user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from( config('mail.from_address') )
        			->subject( $this->league->leagueName . " Invite")
        			->markdown('emails.invitation-email');
    }

}