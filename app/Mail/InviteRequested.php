<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\League;

class InviteRequested extends Mailable
{
    use Queueable, SerializesModels;

	public $league;
	public $request;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->league  = League::find(config('trivialeague.league_id'));
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from( config('mail.from_address') )
        			->subject( $this->league->leagueName . " Invite Request")
        			->markdown('emails.invitationRequest-email');
    }
}
