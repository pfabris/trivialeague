<?php

namespace App\Listeners;

use App\Events\ScoreToggled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ScoreToggledListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(ScoreToggled $event)
    {
         // Access the score using $event->score...
         // Log::info($event->score);
    }
}
