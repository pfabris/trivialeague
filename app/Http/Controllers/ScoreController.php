<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use App\Models\Score;
use	App\Http\Resources\ScoreResource;

class ScoreController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all Scores
	 *
	 * @return Resource
	 */

	public function index()
	{
		$scores = Score::all();
		return ScoreResource::collection($scores);
	}
	
	/**
	 * Show a single Score
	 *
	 * @return Resource
	 */

	public function show(Score $score)
    {
        return new ScoreResource($score);
    }
	
	
	/**
	 * CREATE a single Score
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $score = Score::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE scores SET id=? WHERE id=?;";
	    	$update = DB::update($sql, [$request->id,$score->id]);
        }
        return response()->json($score, 201);
    }
    
    
    /**
	 * CREATE/Update multiple Score Records
	 *
	 * @return Response
	 */
    
    public function storeMultiple(Request $request)
    {
	    foreach( $request->all() as $index=>$record )
	    {
		    $score = Score::find($record['id']);
		    if ($score==null)
		    {
			    $score = Score::create($record);
			    if(!empty($record['id']))
		        {
			    	$sql = "UPDATE scores SET id=? WHERE id=?;";
			    	$update = DB::update($sql, [$record['id'],$score->id]);
		        }
		        Log::info("Created Score ". $score->id);
			}
			else 
			{
				$score->update($record);
				Log::info("Updated Score ". $record['id']);
			}
	    }
        return response()->json("Done", 200);
    }
    
    
    /**
	 * UPDATE a single Score
	 *
	 * @return Response
	 */
    public function update(Request $request, Score $score)
    {
        $score->update($request->all());
        return response()->json($score, 200);
    }
    
    /**
	 * DELETE a single Score
	 *
	 * @return Response
	 */
    public function delete(Score $score)
    {
        $score->delete();

        return response()->json(null, 204);
    }
    
    
    

	/* *********************** WEB Methods **************************** */
	
	
}