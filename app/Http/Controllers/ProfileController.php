<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\League;
use App\Models\Location;
use App\Models\Season;
use App\Models\GameNight;
use App\Models\Round;
use App\Models\Question;
use App\Models\Game;
use App\Models\GameTeam;
use App\Models\GameTeamRound;
use App\Models\GamePlayer;
use App\Models\ScoringRound;
use App\Models\Score;
use App\Models\Team;
use App\Models\User;

class ProfileController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	
	/**
	 * Show the User Profile page
	 * 
	 * @return View
	 */
	public function generateViewProfileUser($userID)
	{
		
		$league = League::findOrFail( config('trivialeague.league_id') );
		$user 	= User::findOrFail($userID);
		$team 	= $user->team;
		$roles	= getEnumValues("users","primaryRole");
		$notificationPrefs = getEnumValues("users","notificationPreference");
		
		$gamesPlayed	= DB::table('gameplayers')->where('user_id', $user->id)->count();
		$seasonRank		= DB::table('gameplayers')
								->join('gameteams', 'gameteams.id', '=', 'gameplayers.gameteam_id')
								->join('games', 'games.id', '=', 'gameteams.game_id')
								->join('gamenights', 'gamenights.id', '=', 'games.gamenight_id')
								->join('seasons', 	 'seasons.id', '=', 'gamenights.season_id')
								->where('gameplayers.user_id', $user->id)
								->whereDate('dateStart', "<=", date('Y-m-d'))
								->whereDate('dateEnd', ">=", date('Y-m-d'))
								->average('totalDeuces');
		$allTimeRank	= DB::table('gameplayers')
								->where('user_id', $user->id)
								->average('totalDeuces');
		
		
		
		return view( 'profile.user',
			[
			'league'	=> $league,
			'user'		=> $user,
			'team'		=> $team,
			'roles'		=> $roles,
			'notificationPrefs' => $notificationPrefs,
			'gamesPlayed'	=> $gamesPlayed,
			'seasonRank'	=> $seasonRank,
			'allTimeRank'	=> $allTimeRank,
			]
		);
	}
	
	public function generateViewProfileTeam($teamID)
	{
		$team 		= Team::findOrFail($teamID);
		$captain	= $team->user;
		$teammates	= $team->users;
		$location	= $team->location;
		$league		= $location->league;

		$gamesPlayed	= 100;
		$seasonRank		= 3;
		$allTimeRank	= 5;

		return view( 'profile.team',
			[
			'league'	=> $league,
			'captain'	=> $captain,
			'team'		=> $team,
			'location'	=> $location,
			'teammates'	=> $teammates,
			'gamesPlayed'	=> $gamesPlayed,
			'seasonRank'	=> $seasonRank,
			'allTimeRank'	=> $allTimeRank,
			
			]
		);
	}
		
}