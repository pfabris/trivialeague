<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use	App\Http\Resources\UserResource;

// https://github.com/spatie/laravel-permission
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{

	/* *********************** API Methods **************************** */

	/**
	 * Index all Users
	 *
	 * @return Resource
	 */
	public function index()
	{
		$user = User::all();
		return UserResource::collection($user);
	}

	/**
	 * Show a single User
	 *
	 * @return Resource
	 */

	public function show(User $user)
	{
		return new UserResource($user);
	}

	/**
	 * CREATE a single User
	 *
	 * @return Response
	 */

	public function store(Request $request)
	{
		$user = User::create($request->all());
		if(!empty($request->id))
		{
			$sql = "UPDATE users SET id=? WHERE id=?;";
			$users = DB::update($sql, [$request->id,$user->id]);

		}
		$user->syncRoles([$user->primaryRole]);
		return response()->json($user, 201);
	}

	/**
	 * UPDATE a single User
	 *
	 * @return Response
	 */
	public function update(Request $request, User $user)
	{
		$user->update($request->all());
		$user->syncRoles([$user->primaryRole]);
		return response()->json($user, 200);
	}


	/* *********************** WEB Methods **************************** */
	/**
	 * UPDATE a single User from Admin User Form
	 *
	 * @return Redirect
	 */
	public function updateUser(Request $request, $id)
	{
		$user = User::find($id);

		$this->validate(request(), [
			'nameFirst'		=> 'required',
			'primaryRole'	=> 'required',
			'email'			=> 'unique:users,email,'.$user->id,
			'league_id' 	=> 'required',
			'phoneMobile' 	=> 'required_if:notificationPreference,SMS',
			//'password' 		=> 'required|string|min:6|confirmed',

		]);
		$user->nameFirst 	= $request->get('nameFirst');
		$user->nameLast 	= $request->get('nameLast');
		$user->nameFull 	= trim( $request->get('nameFirst') . " " . $request->get('nameLast'));
		$user->primaryRole 	= $request->get('primaryRole');
		$user->email 		= $request->get('email');
		$user->phoneMobile 	= $request->get('phoneMobile');
		$user->dateJoined 	= $request->get('dateJoined');		
		$user->team_id 		= $request->get('team_id');
		$user->league_id 	= $request->get('league_id');
		$user->status 		= ($request->get('status')=="Active" ? "Active" : "Inactive");
		$user->type 		= "Human";
		$user->notificationPreference = $request->get('notificationPreference');
		
		$user->save();
		
		// Roles and Permissions
		$roles = $user->getRoleNames();
		if(count($roles)<2)
		{
			$user->syncRoles([$user->primaryRole]);
		}
		
		// Avatar -- if user doesn't have an email address, generate a random Gravatar
		if(empty($user->email) && empty($user->avatar))
		{
			$user->avatar = getGravatar( str_random(40).'@'.str_random(10).".info", 256, 'robohash', 'g');
		}
		else if(empty($user->avatar)) 
		{
			$user->avatar = getGravatar( $user->email, 256, 'robohash', 'g');
		}
		
		$user->save();
		
		return redirect()->route('adminLeagueUser', ['userID' => $id])->with('success','User has been updated!');
	}
	
	/**
	 * DELETE a single User from Admin User Form
	 *
	 * @return Redirect
	 */
	public function deleteUser(Request $request, $id)
	{
		$user = User::find($id);
		$user->delete();
		return redirect()->route('adminLeagueUsers')->with('success','User has been deleted!');
	}
	
	/**
	 * DELETE a single User
	 *
	 * @return Response
	 */
	public function delete(User $user)
	{
		$user->delete();
		return response()->json(null, 204);
	}
	
	
	/**
	 * UPDATE a single User from User Profile Form
	 *
	 * @return Redirect
	 */
	public function updateUserProfile(Request $request, $id)
	{
		
		$this->authorize('edit-user-self', User::class);
		
		

		if($id!==\Auth::user()->id){
			return back()->with('error','You are not authorized edit this profile.');
		}

		$user = User::find($id);
		
		$this->validate(request(), [
			'nameFirst' 	=> 'required',
			'email'			=> 'required|email|unique:users,email,'.$user->id,
			'phoneMobile' 	=> 'required_if:notificationPreference,SMS',
			'password' 		=> 'required|string|min:6|confirmed',
		]);
		$user->nameFirst 	= $request->get('nameFirst');
		$user->nameLast 	= $request->get('nameLast');
		$user->nameFull 	= trim( $request->get('nameFirst') . " " . $request->get('nameLast'));
		$user->email 		= $request->get('email');
		$user->phoneMobile 	= $request->get('phoneMobile');
		$user->dateJoined 	= $request->get('dateJoined');
		$user->password 	= Hash::make($request->get('password'), ['rounds' => 12]);
		$user->notificationPreference = $request->get('notificationPreference');

		$user->save();

		return redirect()->route('profileUser', ['id' => $id])->with('success','Your profile has been updated!');
	}

	

}
