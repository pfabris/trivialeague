<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Team;
use	App\Http\Resources\TeamResource;
use App\Models\User;
use	App\Http\Resources\UserResource;

class TeamController extends Controller
{
	
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all Teams
	 *
	 * @return Resource
	 */
	public function index()
	{
		$team = Team::all();
		return TeamResource::collection($team);
	}
	
	
	/**
	 * Index all Members of a Team
	 *
	 * @return Resource
	 */
	public function indexTeamMates($team)
	{
		$teamMate = User::where('team_id', $team)->orderby("primaryRole")->orderby("nameFull")->get();
		return UserResource::collection($teamMate);
	}

}