<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\League;
use App\Models\Location;
use App\Models\Season;
use App\Models\GameNight;
use App\Models\Game;
use App\Models\GameTeam;
use App\Models\Team;
use App\Models\User;

class PublicController extends Controller
{
    
    private $league	= null;
	
	public function __construct()
	{
		$this->league = League::findOrFail( config('trivialeague.league_id') );
	}
	
	public function generateViewIndex()
	{
		return view( 'public.index',
        [
			'league'		=> $this->league,
			'locations'		=> $this->league->locations()->orderBy('locationName')->get(),
			'otherSeasons'	=> app(SeasonController::class)->getSeasons(),
			'season'		=> app(SeasonController::class)->getCurrentSeason(),
		]);

	}
	
	public function generateViewResults($seasonID=null,$gameNightID=null)
	{
		
		
		if($seasonID!=null) 
		{
			$season = Season::findOrFail($seasonID);
		} 
		else  
		{
			$season = app(SeasonController::class)->getCurrentSeason();
		}
		
		$otherSeasons = app(SeasonController::class)->getSeasonsExcluding($season->id);
		
		
		$gameNights = $season->gamenights()->orderBy('date','asc')->get();
		
		if(empty($gameNightID))
		{
			$lastGameNight	= app(GameNightController::class)->getLastGameNight();
			$gameNightID	= $lastGameNight->id;
			$seasonID		= $lastGameNight->season_id;
		}
		else 
		{
			$lastGameNight = GameNight::findOrFail($gameNightID);
		}
		
				
		$sql = 'SELECT games.gamenight_id AS gameNightID, games.id AS gameID, date, gameNumber, 
				locationName, banner, locations.avatar, 
				gameteams.id, teamName, gameteams.teamPosition, gameteams.countWin, gameteams.countTie,
				SUM(gameteamrounds.totalScore) AS totalScore
				FROM games 
				LEFT JOIN gamenights ON gamenights.id = games.gamenight_id
				LEFT JOIN locations ON locations.id = games.location_id
				LEFT JOIN gameteams ON gameteams.game_id = games.id
				LEFT JOIN teams ON teams.id = gameteams.team_id
				LEFT JOIN gameteamrounds ON gameteamrounds.gameteam_id = gameteams.id
				WHERE games.gamenight_id = ?
				GROUP BY gameteams.id
				ORDER BY locations.locationName, gameteams.teamPosition';
				
		$games = DB::select($sql, [$gameNightID]);
		
		return view('public.results',
        [
			'league'		=> $this->league,
			'locations'		=> $this->league->locations()->orderBy('locationName')->get(),
			'otherSeasons'	=> $otherSeasons,
			'season'		=> $season,
			'gameNight' 	=> $lastGameNight,
			'games'			=> $games,
			'gameNights'	=> $gameNights,
		]);

	}

	public function generateViewResultsGame($seasonID,$gameNightID,$gameID)
	{
		
		$scores				= null;
		$gameTeamResults	= null;
		$gameTeamRounds		= null;
		$playersTeam1		= null;
		$playersTeam2		= null;
		$playerStandings	= null;
		
		$season		= Season::findOrFail($seasonID);
		$otherSeasons 	= app(SeasonController::class)->getSeasonsExcluding($season->id);
		$gameNight	= GameNight::findOrFail($gameNightID);
		
		// Get Games and Teams for the sub NavBar
		$sql = "SELECT games.id AS gameID, locationName, t1.teamShortName AS team1Name, t2.teamShortName AS team2Name
				FROM games 
				LEFT JOIN locations ON locations.id = games.location_id
				LEFT JOIN gameteams AS gt1 ON games.id = gt1.game_id AND gt1.teamPosition=1
				LEFT JOIN teams AS t1 ON gt1.team_id = t1.id
				LEFT JOIN gameteams AS gt2 ON games.id = gt2.game_id AND gt2.teamPosition=2
				LEFT JOIN teams AS t2 ON gt2.team_id = t2.id
				WHERE gamenight_id = ?
				ORDER BY locationName";
		$games = DB::select($sql, [$gameNightID]);

		
		// Get Teams and the Grand totals by team 
		// -- should return 2 rows
		$sql = 'SELECT team_id, teamName, teamShortName, teamPosition, SUM(totalScore) AS gameTotalScore, countWin, date, gameNumber,  locationName, banner
				FROM gameteamrounds
				JOIN gameteams ON gameteams.id = gameteamrounds.gameteam_id
				JOIN games ON gameteams.game_id = games.id
				JOIN gamenights ON gamenights.id = games.gamenight_id
				JOIN locations ON locations.id = games.location_id
				JOIN teams ON teams.id = gameteams.team_id

				WHERE games.id = ?
				GROUP BY teamPosition
				ORDER BY teamPosition;';
				
		$gameTeamResults = DB::select($sql, [$gameID]);
		
		
		
		// Get the Game SCORES
		// Returns 100 rows
		$sql = 'SELECT scores.roundNumber, sequence, nameFull, teamName, teamPosition, rounds.title, scores.questionNumber, 
				questions.question, questions.answer, scoreValue, score, scores.id AS scoreID
				FROM games
				JOIN gameteams ON gameteams.game_id = games.id
				JOIN gameplayers ON gameplayers.gameteam_id = gameteams.id
				JOIN users ON users.id = gameplayers.user_id
				JOIN teams ON teams.id = gameteams.team_id
				JOIN scores ON scores.gameplayer_id = gameplayers.id
				JOIN rounds ON rounds.id = scores.round_id
				JOIN questions ON questions.id = scores.question_id
				
				WHERE games.id = ?
				ORDER BY scores.roundNumber, seat;';
				
		$scores = DB::select($sql, [$gameID]);
		
		
		
		// Get the Round totals by Team
		// -- should return 20 results
		$sql = 'SELECT roundNumber, totalScore, teamPosition
				FROM gameteamrounds
				JOIN gameteams ON gameteams.id = gameteamrounds.gameteam_id
				JOIN games ON gameteams.game_id = games.id

				WHERE games.id = ?
				ORDER BY roundNumber, teamPosition;';
				
		$gameTeamRounds  = DB::select($sql, [$gameID]);


		
		// Get the Players
		// -- returns 10 results
		$sql = 'SELECT gameplayers.id AS gamePlayerID, nameFull, teamName, seat, COUNT(countDeuce) AS deuceCount
				FROM gameplayers
				JOIN gameteams ON gameteams.id = gameplayers.gameteam_id 
				JOIN games ON gameteams.game_id = games.id
				JOIN users ON users.id = gameplayers.user_id
				JOIN teams ON teams.id = gameteams.team_id
				JOIN scores ON scores.gameplayer_id  = gameplayers.id
				WHERE games.id = ? AND gameteams.teamPosition = ?
				GROUP BY seat
				ORDER BY seat;';
				
		$playersTeam1 = DB::select($sql, [$gameID, 1 ]);
		$playersTeam2 = DB::select($sql, [$gameID, 2 ]);
		
		
		return view('public.results-game', [
			'gameID' 		=> $gameID,
			'gameNight'		=> $gameNight,
			'season' 		=> $season,
			'otherSeasons' 	=> $otherSeasons,
			'league' 		=> $this->league, 
			'scores' 		=> $scores,
			'playersTeam1' 		=> $playersTeam1, 
			'playersTeam2'		=> $playersTeam2,
			'gameTeamResults'	=> $gameTeamResults,
			'gameTeamRounds' 	=> $gameTeamRounds,
			'games'				=> $games
			]);
		
	}
	
	public function generateViewSchedule($seasonID=null,$gameNightID=null)
	{
		
		if($seasonID!=null) 
		{
			$season = Season::findOrFail($seasonID);
		} 
		else  
		{
			$season = app(SeasonController::class)->getCurrentSeason();
		}
		
		$otherSeasons 	= app(SeasonController::class)->getSeasonsExcluding($season->id);
		
		$gameNights = $season->gamenights()->orderBy('date','asc')->get();
		
		if(!empty($gameNightID)) 
		{
			$gameNight = GameNight::findOrFail($gameNightID);	
		}
		else 
		{
			//$gameNight   = GameNight::where("season_id",$season->id)->orderByRaw('ABS( DATEDIFF( date, NOW() ) )')->first();
			$gameNight	 = app(GameNightController::class)->getNextGame();
			$gameNightID = $gameNight->id;
		}
		
		$games = Game::where("gamenight_id",$gameNightID)
		->with("gameteams")
		->with("location")
		->select('games.*', \DB::raw('(SELECT locationName FROM locations WHERE games.location_id = locations.id ) AS sort'))
		->orderBy('sort')
		->get();
		
		
		$sql = "SELECT teams.*, locations.locationName 
				FROM teams 
				JOIN gameteams ON gameteams.team_id = teams.id 
				JOIN games ON gameteams.game_id = games.id 
				JOIN locations ON games.location_id = locations.id 
				WHERE games.gamenight_id=? 
				ORDER BY teamName";
		$teams = DB::select($sql,[$gameNightID]);
		

		return view('public.schedule',
        [
			'league'		=> $this->league,
			'otherSeasons'	=> $otherSeasons,
			'season'		=> $season,
			'gameNights'	=> $gameNights,
			'gameNight'		=> $gameNight,
			'games'			=> $games,
			'teams'			=> $teams,
		]);

	}
	
	public function generateViewStandings($seasonID=null, Request $request)
	{
		if($seasonID!=null) 
		{
			$season = Season::findOrFail($seasonID);
		} 
		else  
		{
			$season = app(SeasonController::class)->getCurrentSeason();
		}
		
		$otherSeasons 	= app(SeasonController::class)->getSeasonsExcluding($season->id);
		
		
		// Get the Player Standings for the Season
        $sql = 'SELECT nameFull, primaryRole, teamName, teamShortName, teams.avatar AS teamAvatar, gameplayers.id, 
        		gameplayers.user_id, COUNT(countDeuce) AS deuceCount, Round(COUNT(gameplayers.id)/10,0) AS gameCount 
				FROM gameplayers
				JOIN gameteams ON gameteams.id = gameplayers.gameteam_id 
				JOIN games ON gameteams.game_id = games.id
				JOIN gamenights ON games.gamenight_id = gamenights.id
				JOIN users ON users.id = gameplayers.user_id
				JOIN teams ON teams.id = gameteams.team_id
				JOIN scores ON gameplayers.id = scores.gameplayer_id
				
				WHERE gamenights.season_id = ? AND primaryRole<>"Team"
				GROUP BY gameplayers.user_id
				ORDER BY ' . ( $request->input('byTeam')==true ? 'teamShortName ASC,' : '') . 'deuceCount DESC, nameFull';
        $playerSeasonStandings = DB::select($sql, [$season->id]);
		
		
		// Get the Team Standings for the Season
		$sql = 'SELECT teamName, (( IFNULL(SUM(gtJoin.countWins),0) * 2) + IFNULL(SUM(gtJoin.countTies),0)) AS teamPoints, SUM(gtJoin.totalScores) AS teamTotalScore, SUM(gtJoin.totalDeuces) AS deuceCount, SUM(gtJoin.totalStealsFor) AS stealsForCount, SUM(gtJoin.countGames) AS gamesTeamPlayed
				FROM teams
				INNER JOIN 
				(
					SELECT gt.team_id, SUM(gtr.totalScores) AS totalScores, SUM(gt.countWin) AS countWins,SUM(gt.countTie) AS countTies, SUM(gtr.totalDeuces) AS totalDeuces, SUM(gtr.totalStealsFor) AS totalStealsFor, count(totalScores) AS countGames
					FROM gameteams gt
					INNER JOIN 
					(
						SELECT gameteam_id, SUM(totalScore) AS totalScores, SUM(totalDeuces) AS totalDeuces, SUM(totalStealsFor) AS totalStealsFor
						FROM gameteamrounds
						GROUP BY gameteam_id
					) gtr
					ON gtr.gameteam_id=gt.id
					JOIN games ON games.id = gt.game_id
					JOIN gamenights ON gamenights.id = games.gamenight_id
					WHERE season_id = ?
					GROUP BY gt.id
				) gtJoin
				ON teams.id = gtJoin.team_id
				GROUP BY teams.id
				ORDER BY teamPoints DESC, teamTotalScore DESC, teamName';
        $teamSeasonStandings = DB::select($sql, [$season->id]);
		
		
		return view('public.standings',
        [
			'league'		=> $this->league,
			'otherSeasons'	=> $otherSeasons,
			'season'		=> $season,
			'playerSeasonStandings' => $playerSeasonStandings,
			'teamSeasonStandings' 	=> $teamSeasonStandings,
			'sort'					=> ($request->input('byTeam')==true ? 'byTeam' : 'byPlayer')
		]);

	}
	
	public function generateViewStandingsPlayer($seasonID,$playerID)
	{

		$player 	= User::findOrFail($playerID);
		$season 	= Season::findOrFail($seasonID);
		$otherSeasons 	= app(SeasonController::class)->getSeasonsExcluding($season->id);
		
		$sql = "SELECT SUM(countDeuce) AS totalDeuces, count( distinct gameplayer_id) AS gameCount, SUM(countStealAgainst) AS totalStealsAgainst FROM scores 
				JOIN gameplayers ON gameplayers.id=scores.gameplayer_id
				JOIN gameteams 	ON gameteams.id=gameplayers.gameteam_id
				JOIN games 		ON games.id=gameteams.game_id
				JOIN gamenights ON gamenights.id=games.gamenight_id
				WHERE gamenights.season_id=?
				AND gameplayers.user_id=?";
		$seasonTotals = DB::select($sql, [$seasonID,$playerID]);
		
		$sql = "SELECT scores.roundNumber, scores.questionNumber, countDeuce, countStealAgainst, sequence,
				questions.question, rounds.title, gameTotals.totalDeuces, gameTotals.totalStealsAgainst,
				gamenights.date, gamenights.gameNumber, locations.locationName,
				opposingteam.teamName AS opposingTeamName, games.id as gameID
				FROM scores
				JOIN rounds 	ON rounds.id=scores.round_id
				JOIN questions 	ON questions.id=scores.question_id
				JOIN gameplayers ON gameplayers.id=scores.gameplayer_id
				JOIN gameteams 	ON gameteams.id=gameplayers.gameteam_id
				JOIN gameteams opposingGT ON opposingGT.id<>gameplayers.gameteam_id AND opposingGT.game_id=gameteams.game_id
				JOIN teams opposingteam ON opposingGT.team_id=opposingteam.id
				JOIN games 		ON games.id=gameteams.game_id
				JOIN gamenights ON gamenights.id=games.gamenight_id
				JOIN locations 	ON games.location_id=locations.id
				INNER JOIN (
					SELECT SUM(countDeuce) AS totalDeuces, SUM(countStealAgainst) AS totalStealsAgainst, gameplayer_id 
					FROM scores GROUP BY gameplayer_id
				) gameTotals ON scores.gameplayer_id=gameTotals.gameplayer_id 
				WHERE gameplayers.user_id=?
				ORDER BY gamenights.date,scores.roundNumber,scores.questionNumber";
		$scores = DB::select($sql, [$playerID]);
		
				
		return view ('public.standings-player',
		[
			'league'		=> $this->league,
			'otherSeasons'	=> $otherSeasons,
			'season'		=> $season,
			'player' 		=> $player,
			'seasonTotals'	=> $seasonTotals,
			'scores'		=> $scores
		]);
	}
	
	
	
	public function generateViewCredits()
	{
		return view('public.credits',
        [
			'league'	=> $this->league,
		]);
	}
}
