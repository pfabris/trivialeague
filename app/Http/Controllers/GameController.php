<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Game;
use	App\Http\Resources\GameResource;

use App\Models\League;
use App\Models\Season;
use App\Models\GameTeam;
use App\Models\GamePlayer;
use App\Models\GameTeamRound;
use App\Models\Score;
use App\Models\Question;

use App\Events\ScoreToggled;

class GameController extends Controller
{
	
	private $league = null;
	
	 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all Games
	 *
	 * @return Resource
	 */
	public function index()
	{
		$games = Game::all();
		return GameResource::collection($games);
	}
	
	/**
	 * Show a single Game
	 *
	 * @return Resource
	 */
	public function show(Game $game)
    {
        return new GameResource($game);
    }
	
	/**
	 * CREATE a single Game
	 *
	 * @return Response
	 */
	public function store(Request $request)
    {
        $game = Game::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE games SET id=? WHERE id=?;";
	    	$update = DB::update($sql, [$request->id,$game->id]);
        }
        return response()->json($game, 201);
    }
    
    /**
	 * UPDATE a single Game
	 *
	 * @return Response
	 */
    public function update(Request $request, Game $game)
    {
        $game->update($request->all());

        return response()->json($game, 200);
    }
    
    /**
	 * DELETE a single Game
	 *
	 * @return Response
	 */
    public function delete(Game $game)
    {
        $game->delete();

        return response()->json(null, 204);
    }
    
    
    

	
	
	/* *********************** AJAX Methods **************************** */	
	
	/**
	 * This is an omnibus function that takes $request input from the Admin Screen and generates
	 * Records in several tables to setup a Game at a GameNight.
	 *
	 * @return JSON Response
	 */
	public function setupGame(Request $request)
	{
		
		// $request object should include: game_id, gameteam1_ID, team1_ID, team1_players[], gameteam2_ID, team2_ID, team2_players[]
		
		$this->league = League::findOrFail( config('trivialeague.league_id') );
		
		// TODO Validate Inputs:		
		$gameID 		= $request->game_id;
		$gameTeam1_ID	= $request->gameteam1_ID;
		$gameTeam2_ID	= $request->gameteam2_ID;
		$team1_ID		= $request->team1_ID;
		$team2_ID		= $request->team2_ID;
		$team1_players	= $request->team1_players;
		$team2_players	= $request->team2_players;
		
		
		$roundsPerGame 				= $this->league->roundsPerGame;
		$questionsPerRound 			= $this->league->questionsPerRound;
		$playersPerTeam 			= ($this->league->seatsPerGame / 2);
		$swapQuestionOrder2ndHalf 	= ($this->league->swapQuestionOrder2ndHalf == 1);

		// Initialize other vars
		$gamePlayers	= [];
		$playersQOrder 	= [];
		$scoringRounds	= [];
		$scores			= null;
		
		// ============================= Fetch GAME
		$game = Game::findOrFail($gameID);
		
		// ============================= Update GAMETEAMS
		$gameTeam1 = GameTeam::findOrFail($gameTeam1_ID);
		$gameTeam1->team_id = $team1_ID;
		$gameTeam1->save();
		$gameTeam2 = GameTeam::findOrFail($gameTeam2_ID);
		$gameTeam2->team_id = $team2_ID;
		$gameTeam2->save();
		
		// ============================= Fetch ROUNDS
		$sql = "SELECT id,roundNumber FROM rounds WHERE gamenight_id=? ORDER BY roundNumber;";
		$rounds = DB::select( $sql , [$game->gamenight_id] );
		if(count($rounds) < $roundsPerGame){
			Log::info($rounds);
			return response()->json( ["status" => "Error", "message"=>"Not enough Rounds setup for this GameNight"], 500);
		}
		
		// ============================= Fetch QUESTIONS
		$sql1 = "SELECT roundNumber, rounds.id AS round_id, questionNumber, questions.id AS question_id
				FROM questions 
				JOIN rounds on rounds.id = questions.round_id
				WHERE rounds.gamenight_id = ? AND roundNumber>=? AND roundNumber<=?
				ORDER BY roundNumber, questionNumber ASC;";
				
		$questions1stHalf = DB::select( $sql1 , [$game->gamenight_id, 1, 5] );
		$sql2 = str_replace("questionNumber ASC", "questionNumber DESC", $sql1);
		$questions2ndHalf = DB::select( $sql2 , [$game->gamenight_id, 6, 10] );
		if(count($questions1stHalf) < $roundsPerGame/2 * $questionsPerRound ){
			Log::info($rounds);
			return response()->json( ["status" => "Error", "message"=>"Not enough Questions setup for this GameNight"], 500);
		}
		
		// =============================  Setup GameTeamRounds
		// Check if GameTeamRounds have been setup yet
		$gtrCount = GameTeamRound::whereIn('gameteam_id',[$gameTeam1_ID,$gameTeam2_ID])->count();
		if($gtrCount==0) {
			foreach($rounds as $round)
			{
				GameTeamRound::create( ['gameteam_id'=>$gameTeam1_ID, 'round_id'=>$round->id, 'roundNumber'=>$round->roundNumber] );
				GameTeamRound::create( ['gameteam_id'=>$gameTeam2_ID, 'round_id'=>$round->id, 'roundNumber'=>$round->roundNumber] );
			}
		}
		
		// =============================  Setup GamePlayers
		// Team 1
		// create an indexed array of gameplayers by Seat 
		// [0] =>team1 player1 seat1, [1] =>team1 player2 seat2... 
		foreach ($team1_players as $i=>$player) {
			$gamePlayers[] = GamePlayer::create( [ 'gameteam_id'=>$gameTeam1_ID, 'user_id'=>$player, 'seat'=>$i+1 ] );
		}
		// Team 2
		// [5] =>team2 player1 seat6, [6] =>team2 player2 seat7... 
		foreach ($team2_players as $i=>$player) {
			$gamePlayers[] = GamePlayer::create( [ 'gameteam_id'=>$gameTeam2_ID, 'user_id'=>$player, 'seat'=>$i+1+$playersPerTeam ] );
		}
		
		
		//Log::info($gamePlayers);
		
		//return response()->json( ["status" => "Fail"], 500);
		
		// =============================  Setup Scores
		// Iterate over the half-stack of Questions (this array should have 50 elements, 10 per round.)
		// Each Question Number can be associated with the Player at that same Seat Number
		foreach ($questions1stHalf as $i=>$question) 
		{
			//Assign each question to the appropriate GamePlayer by Seat
			if( $i % $questionsPerRound  == 0) {
				$counterT1 = 0;
				$counterT2 = $playersPerTeam;
			}
			if( $i % 2 ==0 )
			{
				$gameplayerIndex = $counterT1;
				$counterT1++;
			}
			else {
				$gameplayerIndex = $counterT2;
				$counterT2++;
			}
			
			$scores = Score::create(
				[
				'gameplayer_id' =>$gamePlayers[$gameplayerIndex]->id,
				'gameteam_id' 	=>$gamePlayers[$gameplayerIndex]->gameteam_id,
				'roundNumber' 	=>$question->roundNumber,
				'round_id' 		=>$question->round_id,
				'question_id' 	=>$question->question_id,
				'questionNumber'=>$question->questionNumber,
				'sequence'		=>$question->questionNumber,
				]
			);
		}
		$counterT1 = 0;
		$counterT2 = $playersPerTeam;
		foreach ($questions2ndHalf as $i=>$question) {
			//Assign each question to the appropriate GamePlayer by Seat, in reverse order
			if( $i % $questionsPerRound  == 0) {
				$counterT1 = 0;
				$counterT2 = $playersPerTeam;
			}
			if( $i % 2 ==0 )
			{
				$gameplayerIndex = $counterT1;
				$counterT1++;
			}
			else {
				$gameplayerIndex = $counterT2;
				$counterT2++;
			}			
			$scores = Score::create(
				[
				'gameplayer_id' =>$gamePlayers[$gameplayerIndex ]->id,
				'gameteam_id' 	=>$gamePlayers[$gameplayerIndex ]->gameteam_id,
				'roundNumber' 	=>$question->roundNumber,
				'round_id' 		=>$question->round_id,
				'question_id' 	=>$question->question_id,
				'questionNumber'=>$question->questionNumber,
				'sequence'		=>$question->questionNumber,
				]
			);
		}
		
		
				
		//\Debugbar::info($scoringRounds);
		return response()->json( ["status" => "Success"], 200);

	}
	
	/**
	 * Swap gameTeam1 and gameTeam2 Positions and update Scores
	 *
	 * @return JSON Response
	 */
	public function swapTeamPositions ($gameID)
	{
		
		$this->league = League::findOrFail( config('trivialeague.league_id') );
		
		$playersPerTeam = ($this->league->seatsPerGame / 2);
		
		$game 		= Game::findOrFail($gameID);
		$gameTeams 	= $game->gameteams()->get();
		
		foreach ($gameTeams as $gameTeam)
		{
			$newPosition = ($gameTeam->teamPosition == 1 ? 2 : 1);
			DB::table('gameteams')->where('id', $gameTeam->id)
				->update(['teamPosition' => $newPosition]);
			
			// Find all GamePlayers at this GameTeamID
			$gamePlayers = GamePlayer::where('gameteam_id',$gameTeam->id)->get();
			foreach($gamePlayers as $gamePlayer)
			{
				/* foreach, 
				update seat: 
					if seat =1, $newSeat=6
							=2, 7
							=6, 1
							=7, 2
				*/
				
				$newSeat = ( $gamePlayer->seat <= $playersPerTeam ? $gamePlayer->seat + $playersPerTeam : $gamePlayer->seat - $playersPerTeam );
				DB::table('gameplayers')->where('id', $gamePlayer->id)
					->update(['seat'=>$newSeat]);
			
				// Find all Scores at this GamePlayer ID
				$scores = Score::where('gameplayer_id',$gamePlayer->id)->get();
				foreach ( $scores as $score)
				{
					/*
					 foreach, update Score
						if  q=1, newQ# = 2; 
							q=2, 1 
							q=3, 4 
							q=4, 3
							q=5, 6
							q=6, 5
							q=7, 8
							q=8, 7
							q=9, 10
							q=10, 9
						select qID,q# from questions where roundID=$roundID AND q# = newQ#	
						update questionID, questionNumber, sequence 
					*/
					
					$newQ = ( $score->questionNumber % 2 == 1 ? $score->questionNumber + 1 : $score->questionNumber - 1 );
					
					$newQID = Question::where([ ['questionNumber','=', $newQ] , ['round_id','=', $score->round_id ] ])->first();
					DB::table('scores')->where('id',$score->id)
						->update([
							'sequence'=> $newQ,
							'questionNumber'=>$newQ, 
							'question_id'=> $newQID->id,
						]);
				}
			}
		}
		
		return response()->json(["message"=>"OK"],200);
	}
	
	/**
	 *	Change the user_ID of a gameplayer record
	 * 
	 *	TRIGGERED by click on TableHeader in Admin > Scorecard
	 *
	 *	@return JSON Response
	*/
	public function changePlayer(Request $request, $gameID)
	{
		$game = Game::findOrFail($gameID);
		$gamePlayerID = $request->input('gamePlayerID');
		
		$gamePlayer = GamePlayer::find($gamePlayerID);
		$gamePlayer->user_id = $request->input('newUserID');
		$gamePlayer->save();
		
		$gamePlayer = GamePlayer::with('user')->find($gamePlayerID);
		
		$response = [
			"status" => "Success",
			"name"	 => $gamePlayer->user->nameFull
		];
		return response()->json( $response , 200);
	}
	
	/**
	 * This function looks up the Score record and updates it
	 * Toggles using the following order ( null > 2 > 1 > 0 > -1 > null )
	 * It then updates related ScoringRound, GamePlayer, GameTeam, and GameTeamScoringRound records
	 * 
	 * TRIGGERED by a GET request from Score Keeper using the Admin interface and clicking on a score cell
	 * 
	 * @return JSON Response
	 */
	public function toggleScore($gameID,$scoreID)
	{
		//Log::info( "Fired toggleScore ". $scoreID );
		$game			= Game::findOrFail($gameID);
		$score			= Score::findOrFail($scoreID);
		$gamePlayerID	= $score->gameplayer_id;
		$gamePlayer		= GamePlayer::findOrFail($gamePlayerID);
		$isTeam			= ($gamePlayer->user->primaryRole == "Team");
		
		if($score->score === null){
			$score->score = ($isTeam ? 1 : 2) ;
			$score->scoreValue = ($isTeam ? 1 : 2) ;
			$score->countDeuce = ($isTeam ? null : 1) ;
			$score->countTeam  = null;
			$score->countStealAgainst = null;
			$score->save();
		} elseif ($score->score == 2){
			$score->score = 1; 
			$score->countDeuce = null;
			$score->countTeam  = 1;
			$score->countStealAgainst = null;
			$score->scoreValue = 1; 
			$score->save();
		} elseif ($score->score == 1){
			$score->score = 0; 
			$score->scoreValue = 0; 
			$score->countDeuce = null;
			$score->countTeam  = null;
			$score->countStealAgainst = null;			
			$score->save();
		} elseif ($score->score == 0){
			$score->score = "-1"; 
			$score->scoreValue = 0; 
			$score->countDeuce = null;
			$score->countTeam  = null;
			$score->countStealAgainst = 1;			
			$score->save();
		} else {
			$score->score = null; 
			$score->scoreValue = null;
			$score->countDeuce = null;
			$score->countTeam  = null;
			$score->countStealAgainst = null;			
			$score->save();
		}
		
		// ------- UPDATE the Player's totalDeuces and totalStealsAgainst
		
		$totalDeuces 					= GamePlayer::find($score->gameplayer_id)->scores()->sum('countDeuce'); 
		$totalStealsAgainst 			= GamePlayer::find($score->gameplayer_id)->scores()->sum('countStealAgainst'); 
		$gamePlayer->totalDeuces 		= $totalDeuces;
		$gamePlayer->totalStealsAgainst = $totalStealsAgainst;
		$gamePlayer->save();
		
		//Log::info( "Updated GamePlayer " . $score->gameplayer_id ." ". $totalDeuces ." ". $totalStealsAgainst );
		
		// ------- UPDATE GameTeamRounds 
		// 1. Load up the GameTeamRound for this GameTeam's Round
		$gameTeamRound  = GameTeamRound::where("gameteam_id",$gamePlayer->gameteam_id)->where("round_id",$score->round_id)->first();
		//\Debugbar::info($gamePlayer->gameteam_id . " " . $score->round_id . " " . $score->id);
		
		// 2. SUM up the score Value of this Team for this Round
		$sql1 = "SELECT SUM(scoreValue) AS totalScore, gameplayers.totalDeuces AS totalDeuces, 
				SUM(countStealAgainst) AS totalStealsAgainst, 
				scores.questionNumber AS questionNumber, scores.roundNumber AS roundNumber, teamPosition, 
				gameteams.id AS GameTeamID, gameplayer_id AS GamePlayerID
				FROM scores
				JOIN gameplayers ON gameplayers.id = scores.gameplayer_id
				JOIN gameteams ON gameteams.id = gameplayers.gameteam_id
				WHERE scores.round_id=? AND gameplayers.gameteam_id=?;";
	
		$roundScores = DB::select($sql1, [$score->round_id,$gamePlayer->gameteam_id]);	
		
		// 3. Find the Opposing Team's score record for this Round
		// Get the opposing Teams's data by twiddling the search WHERE it's NOT this gameteam_id
		// This allows us to count the opposing team's StealsAgainst as our team's StealsFOR!
		$sql2 = str_replace("gameplayers.gameteam_id=?", "gameplayers.gameteam_id!=?", $sql1);
		$sql2 = str_replace(";", " AND gameteams.game_id=?;", $sql2);
		$roundScoresOpposingTeam = DB::select( $sql2, [$score->round_id, $gamePlayer->gameteam_id, $gameID]);	
		
		// 4. Update the GameTeamRound for THIS team
		$gameTeamRound->totalScore		= ($roundScores[0]->totalScore + $roundScoresOpposingTeam[0]->totalStealsAgainst);
		$gameTeamRound->totalStealsFor	= $roundScoresOpposingTeam[0]->totalStealsAgainst;
		$gameTeamRound->save();
		//Log::info( "Updated GameTeamRound " . $gamePlayer->gameteam_id . " " . $score->round_id  . " " . $gameTeamRound->totalScore . " " . $gameTeamRound->totalStealsFor );
		
		// 5. Update the GameTeamRound for THE OPPOSING team
		$sql3 = 'UPDATE gameteamrounds SET totalScore=?, totalStealsFor=? 
				WHERE gameteam_id=? AND round_id=?;';
		$opposingGameTeamRound  = DB::update( $sql3, [
			($roundScoresOpposingTeam[0]->totalScore + $roundScores[0]->totalStealsAgainst),
			$roundScores[0]->totalStealsAgainst,
			$roundScoresOpposingTeam[0]->GameTeamID,
			$score->round_id
		]);
		//\Debugbar::info($opposingGameTeamRound);
		//Log::info( "Updated oppoGameTeamRound " . $roundScores[0]->GameTeamID . " " . $score->round_id  . " " . $roundScoresOpposingTeam[0]->totalScore. " " . $roundScores[0]->totalStealsAgainst );
		
		
		$response = [
			"status"			 => "Success", 
			"score" 			 => $score->score,
			"roundScore" 		 => (integer)$roundScores[0]->totalScore,
			"stealsAgainst" 	 => (integer)$roundScores[0]->totalStealsAgainst,
			"opponentRoundScore" 	=> (integer)$roundScoresOpposingTeam[0]->totalScore,
			"opponentStealsAgainst" => (integer)$roundScoresOpposingTeam[0]->totalStealsAgainst,
			"questionNumber" 	 => (integer)$roundScores[0]->questionNumber,
			"roundNumber" 		 => (integer)$roundScores[0]->roundNumber,
			"teamPosition" 		 => (integer)$roundScores[0]->teamPosition,
			"gamePlayerID" 		 => $score->gameplayer_id,
			"deuceCount" 		 => (integer)$totalDeuces,
			"scoreID"	 		 => $scoreID,
		];
		
		// 6. BROADCAST the $response an event to any passive listeners
		event(new ScoreToggled($response));
		
		// 7. RETURN the $response back to the Scorekeeper
		return response()->json( $response , 200);
	}
		
	/**
	 *	Wraps up a game by assigning a winner to gameteams table
	 *
	 *	TRIGGERED by "Finish Game" button click on the Admin > ScoreCard interface
	 *
	 *	@return JSON Response
	*/
	public function finishGame($gameID)
	{
		$gameTeams = GameTeam::where('game_id',$gameID)->with('gameteamrounds')->get();
		
		$scores = [];
		$lastScore = -1;
		$tie = false;
		
		// Examine the gameteam records, add up the scores and check for a Tie Game
		foreach($gameTeams as $gt)
		{
			$scores = array_add( $scores, $gt->id, $gt->gameteamrounds->sum('totalScore') );
			if( $lastScore==$gt->gameteamrounds->sum('totalScore') ) { $tie = true; }
			$lastScore = $gt->gameteamrounds->sum('totalScore');
		}
		arsort($scores);
		
		// Update the GameTeam records
		$i = 0;
		foreach($scores as $gameTeamID => $score) 
		{
			$gameTeam = GameTeam::find($gameTeamID);
			$gameTeam->countWin = ($i==0  && !$tie ? 1 : null);
			$gameTeam->countTie = ($tie ? 1 : null);
			$gameTeam->save();
			
			$gameTeamRounds = GameTeamRound::where('gameteam_id',$gameTeamID)->get();
			foreach($gameTeamRounds AS $gtr)
			{
				// Update GameTeamRounds with Total Deuces
				$sc = Score::where('gameteam_id',$gtr->gameteam_id)->where('round_id',$gtr->round_id)->get(); 
				$gtr->totalDeuces = $sc->sum('countDeuce');
				$gtr->save();
			}
			
			$i++;
			
		}
		
		
		$response = [
			"status"			=> "Success",
			"winningGameTeamID"	=> key($scores),
			];
			
		return response()->json( $response , 200);
	}
	
	/**
	 *	Undoes the Finish Game function
	 *
	 *	TRIGGERED by "Unlock Game" button click on the Admin > ScoreCard interface
	 *
	 *	@return JSON Response
	*/
	public function unlockGame($gameID)
	{
		$gameTeams = GameTeam::where('game_id',$gameID)
							->update(['countWin' => null, 'countTie'=>null]);		
		
		$response = [
			"status"			=> "Success",
			];
			
		return response()->json( $response , 200);
	}
	
	/**
	 *	Deletes all GamePlayers, GameTeamRounds and Scores from a Game
	 *
	 *	TRIGGERED by "Reset Game" button click on the Admin > ScoreCard interface
	 *
	 *	@return JSON Response
	*/
	public function resetGame($gameID)
	{
		
		$sql = "DELETE gameteamrounds FROM gameteamrounds 
				INNER JOIN gameteams ON gameteams.id = gameteamrounds.gameteam_id
				WHERE game_id = ?";
		$deleteGT = DB::select($sql, [$gameID]);
		
		$sql = "DELETE gameplayers FROM gameplayers 
				INNER JOIN gameteams ON gameteams.id = gameplayers.gameteam_id
				WHERE game_id = ?";
		$deleteGP = DB::select($sql, [$gameID]);
		
		$sql = "DELETE scores from scores
				INNER JOIN gameteams ON gameteams.id = scores.gameteam_id
				WHERE game_id = ?";
		$deleteScores = DB::select($sql, [$gameID]);		
		
		GameTeam::where('game_id', $gameID )
          ->update(['countWin' => null,'countTie' => null]);
		
		return response()->json(null,200);
	}
	
	/**
	 * This function looks up the Game Totals
	 * 
	 * TRIGGERED as a secondary function when Score Keeper clicks on a score cell 
	 * and Laravel Echo receives a broadcast message on the Game Page
	 * 
	 * @return JSON Response
	 */
	public function getGameTotals($gameID)
	{
		$sql = "SELECT teamPosition, SUM(totalScore) AS TotalScore
		FROM gameteams
		JOIN gameteamrounds ON gameteamrounds.gameteam_id = gameteams.id
		WHERE gameteams.game_id=?
		GROUP BY teamPosition ORDER BY teamPosition";
		
		$gameScores = DB::select($sql, [$gameID]);	
		
		return response()->json($gameScores,200);
	}
	
	
}