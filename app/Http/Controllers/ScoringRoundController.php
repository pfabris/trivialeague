<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\ScoringRound;
use	App\Http\Resources\ScoringRoundResource;

class ScoringRoundController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all ScoringRounds
	 *
	 * @return Resource
	 */

	public function index()
	{
		$scoringRounds = ScoringRound::all();
		return ScoringRoundResource::collection($scoringRounds);
	}
	
	/**
	 * Show a single ScoringRound
	 *
	 * @return Resource
	 */

	public function show(ScoringRound $scoringRound)
    {
        return new ScoringRoundResource($scoringRound);
    }
	
	
	/**
	 * CREATE a single ScoringRound
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $scoringRound = ScoringRound::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE scoringrounds SET id=? WHERE id=?;";
	    	$update = DB::update($sql, [$request->id,$scoringRound->id]);
        }
        return response()->json($scoringRound, 201);
    }
    
    /**
	 * UPDATE a single ScoringRound
	 *
	 * @return Response
	 */
    public function update(Request $request, ScoringRound $scoringRound)
    {
        $scoringRound->update($request->all());

        return response()->json($scoringRound, 200);
    }
    
    /**
	 * DELETE a single Game
	 *
	 * @return Response
	 */
    public function delete(ScoringRound $scoringRound)
    {
        $scoringRound->delete();

        return response()->json(null, 204);
    }
    
    
    

	/* *********************** WEB Methods **************************** */
	
	
}