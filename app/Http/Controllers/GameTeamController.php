<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use App\Models\GameTeam;
use	App\Http\Resources\GameTeamResource;

class GameTeamController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all GameTeams
	 *
	 * @return Resource
	 */

	public function index()
	{
		$gameTeams = GameTeam::all();
		return GameTeamResource::collection($gameTeams);
	}
	
	/**
	 * Show a single GameTeam
	 *
	 * @return Resource
	 */

	public function show(GameTeam $gameTeam)
    {
        return new GameTeamResource($gameTeam);
    }
	
	
	/**
	 * CREATE a single GameTeam
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $gameTeam = GameTeam::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE gameteams SET id=? WHERE id=?;";
	    	$update = DB::update($sql, [$request->id,$gameTeam->id]);
        }
        return response()->json($gameTeam, 201);
    }
    
    
    /**
	 * CREATE/Update multiple GameTeam Records
	 *
	 * @return Response
	 */
    
    public function storeMultiple(Request $request)
    {
	    foreach( $request->all() as $index=>$record )
	    {
		    $gameTeam = GameTeam::find($record['id']);
		    if ($gameTeam==null)
		    {
			    $gameTeam = GameTeam::create($record);
			    if(!empty($record['id']))
		        {
			    	$sql = "UPDATE gameteams SET id=? WHERE id=?;";
			    	$update = DB::update($sql, [$record['id'],$gameTeam->id]);
		        }
		        Log::info("Created GameTeam ". $gameTeam->id);
			}
			else 
			{
				$gameTeam->update($record);
				Log::info("Updated GameTeam ". $record['id']);
			}
	    }
        return response()->json("Done", 200);
    }
    
    
    /**
	 * UPDATE a single GameTeam
	 *
	 * @return Response
	 */
    public function update(Request $request, GameTeam $gameTeam)
    {
        $gameTeam->update($request->all());

        return response()->json($gameTeam, 200);
    }
    
    /**
	 * DELETE a single Game
	 *
	 * @return Response
	 */
    public function delete(GameTeam $gameTeam)
    {
        $gameTeam->delete();

        return response()->json(null, 204);
    }
    
    
    

	/* *********************** AJAX Methods **************************** */
	
	/**
	 * Index all Members of a Team
	 *
	 * @return Resource
	 */
	public function indexTeamMates($gameteam_id)
	{
		$gameTeam 	= GameTeam::findOrFail($gameteam_id);
		$teamMates 	= $gameTeam->team->users;
		$result		= [];
		foreach($gameTeam->team->users as $user) { 
			$result = array_add($result, $user->id, $user->nameFull); 
		}
		return response()->json($result, 200);
	}
}