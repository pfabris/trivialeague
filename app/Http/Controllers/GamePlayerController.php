<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\GamePlayer;
use	App\Http\Resources\GamePlayerResource;

class GamePlayerController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all GamePlayers
	 *
	 * @return Resource
	 */

	public function index()
	{
		$gamePlayers = GamePlayer::all();
		return GamePlayerResource::collection($gamePlayers);
	}
	
	/**
	 * Show a single GamePlayer
	 *
	 * @return Resource
	 */

	public function show(GamePlayer $gamePlayer)
    {
        return new GamePlayerResource($gamePlayer);
    }
	
	
	/**
	 * CREATE a single GamePlayer
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $gamePlayer = GamePlayer::create($request->all());
		if(!empty($request->id))
        {
	    	$sql = "UPDATE gameplayers SET id=? WHERE id=?;";
	    	$update = DB::update($sql, [$request->id,$gamePlayer->id]);
        }
        return response()->json($gamePlayer, 201);
    }
    
    /**
	 * UPDATE a single GamePlayer
	 *
	 * @return Response
	 */
    public function update(Request $request, GamePlayer $gamePlayer)
    {
        $gamePlayer->update($request->all());

        return response()->json($gamePlayer, 200);
    }
    
    /**
	 * DELETE a single Game
	 *
	 * @return Response
	 */
    public function delete(GamePlayer $gamePlayer)
    {
        $gamePlayer->delete();

        return response()->json(null, 204);
    }
    
    
    
	
	
}