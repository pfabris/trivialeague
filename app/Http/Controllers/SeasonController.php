<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Season;
use App\Models\GameNight;
use Carbon\Carbon;

class SeasonController extends Controller
{
	
    // Get the Current Season
    public function getCurrentSeason()
    {
	    $season = Season::where('league_id', config('trivialeague.league_id') )
    				->orderByRaw('ABS( DATEDIFF( dateStart, NOW() ) )')
					->get()
					->first();
   		return $season;
   	}
   	
   	// Get all Seasons in Reverse Sort Order
    public function getSeasons()
    {
    	$seasons = Season::where('league_id', config('trivialeague.league_id') )
    				->orderBy('dateStart', 'desc')
					->get();
   		return $seasons;
   	}
   	
   	// Get all Seasons except the Given Season ID
    public function getSeasonsExcluding($id)
    {
    	$seasons = Season::where('league_id', config('trivialeague.league_id') )
    				->where('id','<>',$id)
    				->orderBy('dateStart', 'desc')
					->get();
   		return $seasons;
   	}
   	
   	
   /**
	* CREATE a single Season
	*
	* @return Response
	*/
	public function store(Request $request)
	{
		$this->validate(request(), [
			'seasonName'	=> 'required',
			'league_id' 	=> 'required',
			'dateStart'		=> 'required|date',
			'dateEnd'		=> 'required|date',
			'dateSkip'		=> 'nullable|date'
		]);
		
		$season = Season::create($request->all());
		if(!empty($request->id))
		{
			$sql = "UPDATE users SET id=? WHERE id=?;";
			$seasons = DB::update($sql, [$request->id,$season->id]);
		}
		
		$dateNext = new Carbon($request->dateStart);
		$lastDate = new Carbon($request->dateEnd);
		$skipDate1 = new Carbon($request->dateSkip1);
		$skipDate2 = new Carbon($request->dateSkip2);
		$i = 1;
		while($dateNext <= $lastDate)
		{
			if($dateNext != $skipDate1 && $dateNext!=$skipDate2)
			{
				$gameNight = new GameNight;
		        $gameNight->date = $dateNext->toDateString();
		        $gameNight->gameNumber = $i;
		        $gameNight->season_id = $season->id;
		        $gameNight->save();
		        $i++;
	        }
			$dateNext->addDays(7);
			
		}
		
		return response()->json($season, 201);
	}

	
   	/**
	 * DELETE a single Season
	 *
	 * @return Response
	 */
	public function delete(Season $season)
	{
		$season->delete();
		return response()->json(null, 204);
	}
	
	

}
