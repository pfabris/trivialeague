<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use App\Models\Question;
use	App\Http\Resources\QuestionResource;

class QuestionController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all Questions
	 *
	 * @return Resource
	 */

	public function index()
	{
		$questions = Question::all();
		return QuestionResource::collection($questions);
	}
	
	/**
	 * Show a single Question
	 *
	 * @return Resource
	 */

	public function show(Question $question)
    {
        return new QuestionResource($question);
    }
	
	
	/**
	 * CREATE a single Question
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $question = Question::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE questions SET id=? WHERE id=?;";
	    	$questions = DB::update($sql, [$request->id,$question->id]);
        }
        return response()->json($question, 201);
    }
    
    /**
	 * CREATE/Update multiple Question Records
	 *
	 * @return Response
	 */
    
    public function storeMultiple(Request $request)
    {
	    foreach( $request->all() as $index=>$record )
	    {
		    $question = Question::find($record['id']);
		    if ($question==null)
		    {
			    $question = Question::create($record);
			    if(!empty($record['id']))
		        {
			    	$sql = "UPDATE questions SET id=? WHERE id=?;";
			    	$update = DB::update($sql, [$record['id'],$question->id]);
		        }
		        Log::info("Created Question ". $question->id);
			}
			else 
			{
				$question->update($record);
				Log::info("Updated Question ". $record['id']);
			}
	    }
        return response()->json("Done", 200);
    }
    
    
    /**
	 * UPDATE a single Question
	 *
	 * @return Response
	 */
    public function update(Request $request, Question $question)
    {
        $question->update($request->all());

        return response()->json($question, 200);
    }
    
    /**
	 * DELETE a single Question
	 *
	 * @return Response
	 */
    public function delete(Question $question)
    {
        $question->delete();

        return response()->json(null, 204);
    }
    
    
    

	/* *********************** WEB Methods **************************** */
	
	
}