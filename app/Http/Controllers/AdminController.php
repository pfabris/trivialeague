<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\League;
use App\Models\Location;
use App\Models\Season;
use App\Models\GameNight;
use App\Models\Round;
use App\Models\Question;
use App\Models\Game;
use App\Models\GameTeam;
use App\Models\GameTeamRound;
use App\Models\GamePlayer;
use App\Models\Score;
use App\Models\Team;
use App\Models\User;

use App\Events\ScoreToggled;

class AdminController extends Controller
{
	
	private $league = null;
	
	public function __construct()
	{
		$this->middleware('auth');
		$this->league = League::findOrFail( config('trivialeague.league_id') );
	}
	
	
	
	/**
	 * Show the "home" page of the Admin Site
	 * 
	 * @return View
	 */
	public function generateViewHome()
	{
		
		// If there is a "Next Game" handy, just load the page with all the Games
		$nextGame = app(GameNightController::class)->getNextGame();
		if(!empty($nextGame->id))
		{
			 return redirect()->route('adminGameNight', ['gameNightID' => $nextGame->id]);
		}
		
		// If there is a current season load the current Season
		$currentSeason = app(SeasonController::class)->getCurrentSeason();
		if(!empty($currentSeason->id))
		{
			return redirect()->route('adminSeason', ['id' => $currentSeason->id]);
		}
		
		// Otherwise, load all Seasons
		$Seasons = app(SeasonController::class)->getSeasons();
		return redirect()->route('adminSeasons', [null], false);
	}
	
	/**
	 * Show the League
	 * 
	 * @return View
	 */
	public function generateViewLeague()
	{
		
		$locations = $this->league->locations()->orderBy('locationName')->get();
		return view('admin.league',
			[
			'league'  => $this->league,
			'locations' => $locations,
			]
		);
	}

	/**
	 * Show the League Users
	 * 
	 * @return View
	 */
	public function generateViewLeagueUsers()
	{
		
		$users = User::where( 'league_id', config('trivialeague.league_id') )
			->leftJoin('teams', 'users.team_id', '=', 'teams.id')
			->selectRaw('users.*, teams.teamName, teams.id as teamID, users.id as userID')
			->orderBy('teamName')->orderBy('status')->orderBy('primaryRole')->orderBy('nameFull')
			->get();
		
		return view('admin.users',
			[
			'league'  => $this->league,
			'users'   => $users,
			]
		);
	}
	
	/**
	 * Show a single User
	 * 
	 * @return View
	 */
	public function generateViewLeagueUser($userID)
	{
		
		$teams = Team::where('locations.league_id',config('trivialeague.league_id'))
			->leftJoin('locations', 'teams.location_id', '=', 'locations.id')
			->selectRaw( 'teams.teamName, teams.id as teamID')
			->orderBy('teamName')
			->get();

		$user = User::where('users.id',$userID)
			->leftJoin('teams', 'users.team_id', '=', 'teams.id')
			->selectRaw('users.*, teams.teamName, teams.id as teamID, users.id as userID')
			->first();

		$gamesPlayed	= DB::table('gameplayers')->where('user_id', $user->id)->count();
		$seasonRank		= DB::table('gameplayers')
								->join('gameteams', 'gameteams.id', '=', 'gameplayers.gameteam_id')
								->join('games', 'games.id', '=', 'gameteams.game_id')
								->join('gamenights', 'gamenights.id', '=', 'games.gamenight_id')
								->join('seasons', 	 'seasons.id', '=', 'gamenights.season_id')
								->where('gameplayers.user_id', $user->id)
								->whereDate('dateStart', "<=", date('Y-m-d'))
								->whereDate('dateEnd', ">=", date('Y-m-d'))
								->average('totalDeuces');
		$allTimeRank	= DB::table('gameplayers')
								->where('user_id', $user->id)
								->average('totalDeuces');
		
		$roles = getEnumValues("users","primaryRole");
		$notificationPrefs = getEnumValues("users","notificationPreference");


		return view('admin.user',
			[
			'league'=> $this->league,
			'user' 	=> $user,
			'teams' => $teams,
			'roles' => $roles,
			'notificationPrefs'=>$notificationPrefs,
			'gamesPlayed'	=> $gamesPlayed,
			'seasonRank'	=> $seasonRank,
			'allTimeRank'	=> $allTimeRank,
			'permissions'	=> $user->getAllPermissions(),
			'roles'			=> $user->getRoleNames(),
			]
		);
	}
	
	/**
	 * Show the Location
	 * 
	 * @return View
	 */
	public function generateViewLocation($locationID)
	{
		
		$location = Location::findOrFail( $locationID );
		$teams = $location->teams()->get();
		$locationManager = $location->user()->get();
		$captains = $location->captains()->get();
		$gameNights = $location->gamesNights()->orderBy('date')->get();
		return view('admin.location',
			[
			'league'  => $this->league,
			'location' => $location,
			'teams' => $teams,
			'locationManager' => $locationManager,
			'captains' => $captains,
			'gameNights' => $gameNights,
			]
		);
	}
	
	/**
	 * Show the Team
	 * 
	 * @return View
	 */
	public function generateViewTeam($teamID)
	{
		
		$team = Team::findOrFail($teamID);
		$location = $team->location()->get();
		$captain = $team->user()->get();
		$teamMates = $team->users()->get();
		$gameTeams = $team->gameTeams()->get();
		
		return view('admin.team',
			[
			'league'  => $this->league,
			'location' => $location,
			'team' => $team,
			'captain' => $captain,
			'teamMates' => $teamMates,
			'gameTeams' => $gameTeams,
			]
		);
	}	
	
	/**
	 * List the Seasons for the League
	 * 
	 * @return View
	 */
	public function generateViewSeasons()
	{
		
		$seasons = app(SeasonController::class)->getSeasons();
		return view('admin.seasons',
			[
			'league'  => $this->league,
			'seasons' => $seasons,
			]
		);
	}
	
	/**
	 * List the GameNights for the SeasonID
	 * 
	 * @return View
	 */
	public function generateViewSeason($seasonID)
	{
		
		$season		= Season::findOrFail( $seasonID );
		$gameNights = $season->gamenights()->withCount('games')->orderBy('date','asc')->get();
		return view('admin.season',
			[
			'league'  	=> $this->league,
			'season' 	=> $season,
			'gameNights'=> $gameNights,
			'color'		=> null
			]
		);
	}	
	
	/**
	 * Show the GameNight Setup View, with the Locations and available teams
	 * allowing the Administrator to Match teams at locations and create Game records
	 * 
	 * @return View
	 */
	public function generateViewGameNightSetup($gameNightID)
	{
		
		$gameNight	= GameNight::findOrFail($gameNightID);
		$season		= Season::findOrFail( $gameNight->season_id );
		$locations  = $this->league->locations()
							->with(array('teams' => function($query) {
						        $query->orderBy('teamName');
						    }))
							->orderBy('locationName')->get();
		return view('admin.gamenight-setup',
			[
			'league'  	=> $this->league,
			'season' 	=> $season,
			'gameNight'	=> $gameNight,
			'locations' => $locations,
			]);
	}
	
	/**
	 * Show a GameNight, listing the Games at the Locations of the League
	 * 
	 * 
	 * @return View
	 */
	public function generateViewGameNight($gameNightID)
	{
		$gameNight	= GameNight::findOrFail($gameNightID);
		$season		= Season::findOrFail( $gameNight->season_id );
		$rounds		= $gameNight->rounds;
		
		$sql = 'SELECT games.id AS gameID, date, gameNumber, 
				locationName, locations.lat, locations.lng, locations.avatar, locations.banner, 
				gt1.id AS gameTeam1_ID, gt2.id AS gameTeam2_ID, 
				t1.teamName as team1_Name, t2.teamName AS team2_Name, 
				COUNT(gameplayers.id) AS gamePlayersCount
				FROM games 
				JOIN gamenights ON gamenights.id = games.gamenight_id
				JOIN locations ON locations.id = games.location_id
				JOIN gameteams AS gt1 ON gt1.game_id = games.id AND gt1.teamPosition=1
				JOIN teams AS t1 ON t1.id = gt1.team_id
				JOIN gameteams AS gt2 ON gt2.game_id = games.id AND gt2.teamPosition=2
				JOIN teams AS t2 ON t2.id = gt2.team_id
				LEFT JOIN gameplayers ON gameplayers.gameteam_id = gt1.id
				WHERE gamenight_id = ?
				GROUP BY gameID
				ORDER BY locationName';
				
		$games = DB::select($sql, [$gameNightID]);
			
		return view('admin.gamenight',
			[
			'league'  	=> $this->league,
			'season' 	=> $season,
			'gameNight'	=> $gameNight,
			'games'		=> $games,
			'rounds'	=> $rounds,
			]); 
	}
	
	/**
	 * Show the setup screen for the chosen Game
	 * 
	 * @return View
	 */
	public function generateViewInitGame($gameID)
	{
		
		$game 		= Game::findOrFail($gameID);
		$gameNight 	= $game->gamenight;
		$location	= $game->location;
		$season		= Season::findOrFail( $gameNight->season_id );
		$sql = "SELECT gameteams.id AS gameteam_id, team_id, teamName, avatar, game_id
				FROM gameteams
				JOIN teams ON teams.id = gameteams.team_id 
				WHERE game_id=?
				ORDER BY teamPosition";
		$teams = DB::select( $sql , [$gameID]);
		//\Debugbar::info($game);
		//\Debugbar::info($gameNight);
		return view('admin.game-edit', 
			[
			'league'		=> $this->league,
			'season'		=> $season,
			'teams'			=> $teams,
			'gameID'		=> $gameID,
			'game'			=> $game,
			'gameNight'		=> $gameNight,
			'location'		=> $location,
			]);	 
	}

	/**
	 * This presents the Game ScoreCard interface for a QM to input scores in realtime
	 * It's similar to the Table setup in the public Game view, but 
	 * 		allows inputs
	 *		sets up POST functions that take inputs and updates Score Records
	 * 		updates ScoringRound, GamePlayer, and GameTeamRound aggregates
	 *
	 * @return View
	 */
	public function generateViewGameScoreCard ($gameID)
	{
		$season					= null;
		$gameResultsFirstHalf	= null;
		$gameResultsSecondHalf	= null;
		$gameTeamResults		= null;
		$gameTeamRoundResults	= null;
		$playersTeam1			= null;
		$playersTeam2			= null;

		
		$sql = "SELECT gameteams.id AS gameteam_id, team_id, teamName, countWin, countTie
				FROM gameteams 
				JOIN teams ON teams.id = gameteams.team_id  
				WHERE game_id=? AND teamPosition=?";
		$team1 = DB::select( $sql , [$gameID,1] )[0];
		$team2 = DB::select( $sql , [$gameID,2] )[0];
		
		// Get Teams and the Grand totals by team 
		// -- should return 2 rows
		$sql =  'SELECT team_id, teamName, teams.avatar, teamPosition, SUM(totalScore) AS gameTotalScore, countWin, date, gameNumber, locationName
				FROM gameteamrounds
				JOIN gameteams ON gameteams.id = gameteamrounds.gameteam_id
				JOIN games ON gameteams.game_id = games.id
				JOIN gamenights ON gamenights.id = games.gamenight_id
				JOIN locations ON locations.id = games.location_id
				JOIN teams ON teams.id = gameteams.team_id

				WHERE games.id = ?
				GROUP BY teamPosition
				ORDER BY teamPosition;';
		$gameTeamResults = DB::select($sql, [$gameID]);
		
		
		
		// Get the Game SCORES
		// Returns 100 rows
		$sql = 'SELECT sequence, nameFull, gameplayers.id AS gamePlayerID, 
				teamName, teamPosition, gameteams.id AS gameTeamID,
				scores.roundNumber, rounds.title, rounds.id AS roundID,
				scores.questionNumber, questions.question, questions.answer, 
				scoreValue, score, 
				scores.id AS scoreID 
				FROM games
				JOIN gameteams ON gameteams.game_id = games.id
				JOIN gameplayers ON gameplayers.gameteam_id = gameteams.id
				JOIN users ON users.id = gameplayers.user_id
				JOIN teams ON teams.id = gameteams.team_id
				JOIN scores ON scores.gameplayer_id = gameplayers.id
				JOIN rounds ON rounds.id = scores.round_id
				JOIN questions ON questions.id = scores.question_id
				
				WHERE games.id = ?
				ORDER BY scores.roundNumber, seat;';
				
		$scores = DB::select($sql, [$gameID]);
		//\Debugbar::info($scores);
		
		
		
		// Get the Round totals by Team
		// -- should return 20 results
		$sql = 'SELECT roundNumber, totalScore, totalStealsFor, teamPosition
				FROM gameteamrounds
				JOIN gameteams ON gameteams.id = gameteamrounds.gameteam_id
				JOIN games ON gameteams.game_id = games.id

				WHERE games.id = ?
				ORDER BY roundNumber, teamPosition;';
				
		$gameTeamRounds  = DB::select($sql, [$gameID]);
		
		
			
		// Get the Players
		// -- returns 10 results
		$sql = 	'SELECT gameplayers.id AS gamePlayerID, nameFull, teamName, seat, COUNT(countDeuce) AS deuceCount
				FROM gameplayers
				JOIN gameteams ON gameteams.id = gameplayers.gameteam_id 
				JOIN games ON gameteams.game_id = games.id
				JOIN users ON users.id = gameplayers.user_id
				JOIN teams ON teams.id = gameteams.team_id
				JOIN scores ON scores.gameplayer_id  = gameplayers.id
				WHERE games.id = ? AND gameteams.teamPosition = ?
				GROUP BY seat
				ORDER BY seat;';
				
		$playersTeam1 = DB::select($sql, [$gameID, 1 ]);
		$playersTeam2 = DB::select($sql, [$gameID, 2 ]);

		$game 		= Game::findOrFail($gameID);
		$gameNight 	= $game->gamenight;
		$location	= $game->location;
		$season		= Season::findOrFail( $gameNight->season_id );
		$gameIsOver	= ($team1->countWin==1 || $team2->countWin==1 || $team1->countTie==1 || $team2->countTie==1 ? true : false);
		
		return view('admin.game-scorecard',
			[
			'league'		=> $this->league,
			'season'		=> $season,
			'gameNight'		=> $gameNight,
			'location'		=> $location,
			'team1'			=> $team1,
			'team2'			=> $team2,
			'playersTeam1' 	=> $playersTeam1, 
			'playersTeam2' 	=> $playersTeam2,
			'gameID'		=> $gameID,
			'gameTeamResults'		=> $gameTeamResults,
			'scores' 				=> $scores, 
			'gameResultsSecondHalf' => $gameResultsSecondHalf, 
			'gameTeamRounds'	 	=> $gameTeamRounds,
			'gameIsOver'			=> $gameIsOver
			]);
	}

	
	/**
	 * This presents a form where Users can submit and edit Rounds to the League Manager
	 * 
	 *
	 * @return View
	*/
	public function generateViewRounds()
	{
		$rounds = Round::orderBy('created_at', 'desc')->get();
		
		return view('admin.rounds',	[ 
			'league' => $this->league,
			'rounds' => $rounds
			]);
	}
	
	/**
	 * This presents a form where Users can submit and edit Rounds to the League Manager
	 * 
	 *
	 * @return View
	*/
	public function generateViewMyRounds()
	{
		$rounds = Round::where('user_id','=', \Auth::user()->id )->orderBy('created_at', 'desc')->get();
		
		return view('admin.rounds', [ 
				'league' => $this->league, 
				'rounds' => $rounds 
			]);
	}
	
	/**
	 * This presents a form where Users can submit and edit Rounds to the League Manager
	 * 
	 *
	 * @return View
	*/
	public function generateViewRoundsSubmit()
	{
		return view('admin.round-edit',	[ 'league' => $this->league ]);
	}


	/**
	 * Show the "current" season
	 * 
	 * @return View via redirect
	 */
	public function currentSeason()
	{
		// If there is a current season load the current Season
		$currentSeason = app(SeasonController::class)->getCurrentSeason();
		return redirect()->route('adminSeason', ['id' => $currentSeason->id]);
	}

	/**
	 * Show the "current" gamenight
	 * 
	 * @return View via redirect
	 */
	public function currentGameNight()
	{
		// If there is a current season load the current Season
		$currentGameNight = app(GameNightController::class)->getNextGame();
		return redirect()->route('adminGameNight', ['id' => $currentGameNight->id]);
	}
	
	
	

	
}