<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nameFirst' => 'required|string|max:255',
            'email' 	=> 'required|string|email|max:255',
            'password' 	=> 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Model\User
     */
    protected function create(array $data)
    {
	    // We are HIJACKING the Create function, which is in Illuminate/Foundation/Auth/RegistersUsers
	    // We are registering with invitation emails, so 
	    // We find existing User record and update it with data from the Form
	    // Then we delete the invite
	    
	    \Auth::logout();
	    
	    $user = User::findOrFail($data['userID']);
	    
	    $user->nameFirst	= $data['nameFirst'];
	    $user->nameLast		= $data['nameLast'];
	    $user->nameFull		= trim($data['nameFirst'] . " " . $data['nameLast']) ;
	    $user->email		= $data['email'];
        $user->password		= bcrypt($data['password']);
		$user->dateJoined	= date('Y-m-d');
		
		$user->save();
		
		// Avatar -- if user doesn't have an email address, generate a random Gravatar
		if(empty($user->email) && empty($user->avatar))
		{
			$user->avatar = getGravatar( str_random(40).'@'.str_random(10).".info", 256, 'robohash', 'g');
		}
		else if(empty($user->avatar)) 
		{
			$user->avatar = getGravatar( $user->email, 256, 'robohash', 'g');
		}
		
		$user->save();
		
		// Roles and Permissions
		$roles = $user->getRoleNames();
		if(count($roles)<2)
		{
			$user->syncRoles([$user->primaryRole]);
		}
		
		// Handle the dead invitation
		$invites = $user->invites;
		foreach($invites as $invite)
		{
			// delete the invite so it can't be used again after a successful registration
			$invite->delete();
		}
		
		
		$this->redirectTo = route('profileUser', ['id' => $user->id]);
		return $user;
    }
}
