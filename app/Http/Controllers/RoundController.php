<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Round;
use App\Models\Question;
use App\Models\League;
use App\Models\GameNight;
use	App\Http\Resources\RoundResource;

class RoundController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all Rounds
	 *
	 * @return Resource
	 */

	public function index()
	{
		$rounds = Round::all();
		return RoundResource::collection($rounds);
	}
	
	/**
	 * Show a single Round
	 *
	 * @return Resource
	 */

	public function show(Round $round)
    {
        return new RoundResource($round);
    }
	
	
	/**
	 * CREATE a single Round
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $round = Round::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE rounds SET id=? WHERE id=?;";
	    	$seasons = DB::update($sql, [$request->id,$round->id]);
        }
        return response()->json($round, 201);
    }
    
    /**
	 * UPDATE a single Round
	 *
	 * @return Response
	 */
    public function update(Request $request, Round $round)
    {
        $round->update($request->all());

        return response()->json($round, 200);
    }
    
    /**
	 * DELETE a single Round
	 *
	 * @return Response
	 */
    public function delete(Round $round)
    {
        $round->delete();

        return response()->json(null, 204);
    }
    
    
    

	/* *********************** WEB Methods **************************** */
		
	/**
	 * UPDATE 10 Rounds at once from Admin GameNights Form
	 *
	 * @return json
	 */
	public function updateGameNightRounds(Request $request, GameNight $id)
	{
		$i=0;
		foreach($request->get('title') as $id=>$thisTitle)
		{
			if(!empty($thisTitle))
			{
				$round = Round::find($id);
				$round->title = $thisTitle;
				$round->description = $request->description[$id];
				$round->save();	
				$i++;			
			}
		}
		
		return response()->json([
			'message' => "Successfully updated $i Rounds for GameNight $id"
		]);
	}
	
	/**
	 * ADD a Round with 10 Questions at once from Admin Rounds Form
	 *
	 * @return json
	 */
	public function submitRound(Request $request, League $id)
	{
		
		$round = Round::create([ 
			'title' => $request->get('roundTitle'), 
			'description' => $request->get('description'),
			'user_id' => \Auth::user()->id
		]);
		 
		$i=0;
		foreach($request->get('question') as $number=>$thisQuestion)
		{
			if(!empty($thisQuestion))
			{
				$question = Question::create([
					'questionNumber' => ($number+1),
					'question' 	=> $thisQuestion,
					'answer' 	=> $request->answer[$number],
					'notes'		=> $request->notes[$number],
					'round_id' 	=> $round->id
				]);
			}
			$i++;
		}
			
		return response()->json([
			'message' => "Successfully added $i Questions for Round ".$round->id
		]);
	}
		
	
}