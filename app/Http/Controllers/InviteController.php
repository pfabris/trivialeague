<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\League;
use App\Models\User;
use App\Models\Invite;
use App\Mail\InviteCreated;
use App\Mail\InviteRequested;
use Illuminate\Support\Facades\Mail;

class InviteController extends Controller
{

	private $league	= null;

	public function __construct()
	{
		$this->league = League::findOrFail( config('trivialeague.league_id') );
	}
	
	public function invite()
	{
		// show the user a form with an email field to invite a new user
		return view('admin.user-invite', ['league' => $this->league]);
	}

	public function process(Request $request)
	{

		// process the form submission and send the invite by email
		// validate the incoming request data
		do {
			//generate a random string using Laravel's str_random helper
			$token = str_random(36);
		} //check if the token already exists and if it does, try again
		while (Invite::where('token', $token)->first());

		//create a new invite record
		$invite = Invite::create([
			'user_id' => $request->get('userID'),
			'email'   => $request->get('email'),
			'token'   => $token
		]);

		// send the email
		Mail::to($request->get('email'))->send(new InviteCreated($invite));

		// redirect back where we came from
		return redirect()->back();
	}



	public function accept($token)
	{
		// here we'll look up the user by the token sent provided in the URL
		// Look up the invite
		if (!$invite = Invite::where('token', $token)->first()) 
		{
			//if the invite doesn't exist do something more graceful than this
			abort(404);
		}

		$user = $invite->user;
		
		// Let the user Register
		return view('vendor.adminlte.register', 
			[
				'invite' => $invite,
				'user'	 => $user,
				'league' => $this->league
			]
		);
	}
	
	
	
	
	public function requestInvite()
	{
		
		return view('vendor.adminlte.requestInvite', ['league' => $this->league]);
	}
	
	public function processRequestInvite(Request $request)
	{
		
		// process the form submission and send the League Admin an email
		$leagueAdminEmail = json_decode($this->league->arbitraryJSON)->league_email;
		$leagueAdminEmail = "paul@lintburger.com";
		
		// send the email
		Mail::to( $leagueAdminEmail )->send(new InviteRequested($request)); 

		// redirect back where we came from
		$request->session()->flash('status', 'Thanks. The League Manager will get back to you soon!');
		return redirect()->back(); 
		
	}


}
