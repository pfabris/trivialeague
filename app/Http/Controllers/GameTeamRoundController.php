<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use App\Models\GameTeamRound;
use	App\Http\Resources\GameTeamRoundResource;

class GameTeamRoundController extends Controller
{
		 
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all GameTeamRounds
	 *
	 * @return Resource
	 */

	public function index()
	{
		$gameTeamRounds = GameTeamRound::all();
		return GameTeamRoundResource::collection($gameTeamRounds);
	}
	
	/**
	 * Show a single GameTeamRound
	 *
	 * @return Resource
	 */

	public function show(GameTeamRound $gameTeamRound)
    {
        return new GameTeamRoundResource($gameTeamRound);
    }
	
	
	/**
	 * CREATE a single GameTeamRound
	 *
	 * @return Response
	 */
	
	public function store(Request $request)
    {
        $gameTeamRound = GameTeamRound::create($request->all());
        if(!empty($request->id))
        {
	    	$sql = "UPDATE gameteamrounds SET id=? WHERE id=?;";
	    	$update = DB::update($sql, [$request->id,$gameTeamRound->id]);
        }
        return response()->json($gameTeamRound, 201);
    }
    
    
    /**
	 * CREATE/Update multiple GameTeamRound Records
	 *
	 * @return Response
	 */
    
    public function storeMultiple(Request $request)
    {
	    foreach( $request->all() as $index=>$record )
	    {
		    $gameTeamRound = GameTeamRound::find($record['id']);
		    if ($gameTeamRound==null)
		    {
			    $gameTeamRound = GameTeamRound::create($record);
			    if(!empty($record['id']))
		        {
			    	$sql = "UPDATE gameteamrounds SET id=? WHERE id=?;";
			    	$update = DB::update($sql, [$record['id'], $gameTeamRound->id]);
		        }
		        Log::info("Created GameTeamRound ". $gameTeamRound->id . " requestID: ". $record['id']);
			}
			else 
			{
				$gameTeamRound->update($record);
				Log::info("Updated GameTeamRound ". $record['id']);
			}
	    }
        return response()->json("Done", 200);
    }
    
    
    /**
	 * UPDATE a single GameTeamRound
	 *
	 * @return Response
	 */
    public function update(Request $request, GameTeamRound $gameTeamRound)
    {
        $gameTeamRound->update($request->all());
        return response()->json($gameTeamRound, 200);
    }
    
    /**
	 * DELETE a single GameTeamRound
	 *
	 * @return Response
	 */
    public function delete(GameTeamRound $gameTeamRound)
    {
        $gameTeamRound->delete();
        return response()->json(null, 204);
    }
    
    
    

	
	
	
}