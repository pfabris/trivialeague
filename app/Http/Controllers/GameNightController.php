<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\League;
use App\Models\Season;
use App\Models\GameNight;
use App\Models\Game;
use App\Models\GameTeam;
use App\Models\Round;
use App\Models\Question;

use	App\Http\Resources\GameNightResource;

class GameNightController extends Controller
{
	
	/* *********************** API Methods **************************** */
	
	/**
	 * Index all GameNights
	 *
	 * @return Response
	 */
	public function index()
	{
		$gameNights = GameNight::all();
		return GameNightResource::collection($gameNights);
	}
	
	/**
	 * Show a single GameNight
	 *
	 * @return Response
	 */
	public function show(GameNight $gameNight)
	{
		return new GameNightResource($gameNight);
	}
	
	/**
	 * CREATE a single GameNight from Request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$gameNight = GameNight::create($request->all());
		if(!empty($request->id))
		{
			$sql = "UPDATE gamenights SET id=? WHERE id=?;";
			$update = DB::update($sql, [$request->id,$gameNight->id]);
		}
		return response()->json($gameNight, 201);
	}
	
	
	
	/**
	 * UPDATE a single GameNight
	 *
	 * @return Response
	 */
	public function update(Request $request, GameNight $gameNight)
	{
		$gameNight->update($request->all());
		return response()->json($gameNight, 200);
	}
	
	/**
	 * DELETE a single GameNight
	 *
	 * @return Response
	 */
	public function delete(GameNight $gameNight)
	{
		$gameNight->delete();
		return response()->json(null, 204);
	}

	
	/* *********************** CLASS Methods **************************** */
	
	/**
	 * Get the Nearest GameNight in the current Season
	 *
	 * @return Collection
	 */
	public function getNextGame()
	{
		
		$season	= app(SeasonController::class)->getCurrentSeason();
		$gameNight = $season->gamenights()->orderByRaw('ABS( DATEDIFF( date, NOW() ) )')->first();
		
		return $gameNight;
		
	}
	
	/**
	 * Get the Last GameNight in the Current Season
	 *
	 * @return Collection
	 */
	public function getLastGameNight()
	{
		
		$season	= app(SeasonController::class)->getCurrentSeason();
		$gameNight = $season->gamenights()->where('date', '<=', date('Y-m-d') )->first();
		
		return $gameNight;
		
	}

	/* *********************** AJAX Methods **************************** */	

	/**
	 * Setup a GameNight - Match Teams at Locations, and Init Round and Question Records
	 *
	 * @return Response
	 */
	public function setupGameNight(Request $request, $id)
	{

		// INFO: $request object should include: visitorTeamList[$locationID=>TeamID], homeTeamList[$locationID=>TeamID], gamenight_id (also in the URL)
		
		$league = League::findOrFail( config('trivialeague.league_id') );
		
		// TODO Validate Inputs:		
		$gameNightID 	= $id;
		$visitorTeamList= $request->visitorTeamList;
		$homeTeamList	= $request->homeTeamList;
		$qmID 			= $request->qmID;
		
		$gameNight = GameNight::findOrFail($id);
		
		//\Debugbar::info($visitorTeamList);
		//\Debugbar::info($homeTeamList);
		
		// Setup Game and GameTeams
		foreach($homeTeamList as $locationID => $homeTeamID)
		{
			// Create the Game
			$game = new Game;
			$game->location_id  = $locationID;
			$game->gamenight_id = $gameNightID;
			$game->user_id		= $qmID;
			$game->save();
			
			// Create the GameTeam for the Home Team
			$gameTeam = new GameTeam;
			$gameTeam->game_id = $game->id;
			$gameTeam->team_id = $homeTeamID;
			$gameTeam->teamPosition = 2;
			$gameTeam->save();
			
			// Create the GameTeam for the Visitor
			$gameTeam = new GameTeam;
			$gameTeam->game_id = $game->id;
			$gameTeam->team_id = $visitorTeamList[$locationID];
			$gameTeam->teamPosition = 1;
			$gameTeam->save();
		}
		
		// Setup Rounds for GameNight
		for ($i = 1; $i <= $league->roundsPerGame; $i++) 
		{
			$round = new Round;
			$round->gamenight_id = $gameNightID;
			$round->title = "Round $i";
			$round->roundNumber = $i;
			$round->save();
			for ($ii = 1; $ii <= $league->questionsPerRound; $ii++) {
				$question = new Question;
				$question->questionNumber = $ii;
				$question->question = "Question $ii";
				$question->round_id = $round->id;
				$question->save();
			}
		}
		
		return response()->json($gameNight, 201);	
	}
	
}