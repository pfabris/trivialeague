<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // prevent /logout link from not working if the session has expired
        // when session expires, CSRF token is invalid and we get a 
        // "Page has expired" error, which is bad and confusing
        '/logout'
    ];
}
