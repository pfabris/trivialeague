<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ScoreResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 					=> $this->id,
	      'gameteam_id'			=> $this->gameteam_id,  
	      'gameplayer_id'		=> $this->gameplayer_id,  
	      'round_id'			=> $this->round_id,  
	      'question_id'			=> $this->question_id,  
	      'sequence' 			=> $this->sequence,
	      'roundNumber' 		=> $this->roundNumber,
	      'questionNumber' 		=> $this->questionNumber,
	      'score' 				=> (integer)$this->score,
	      'scoreValue' 			=> (integer)$this->scoreValue,
	      'countDeuce'			=> (boolean)$this->countDeuce,
	      'countTeam'			=> (boolean)$this->countTeam,
	      'countStealAgainst'	=> (boolean)$this->countStealAgainst,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
