<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GamePlayerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 					=> $this->id,
	      'gameteam_id' 		=> $this->gameteam_id,  
	      'user_id' 			=> $this->user_id,
	      'seat' 				=> (integer)$this->seat,
	      'totalDeuces' 		=> (integer)$this->totalDeuces,
	      'totalStealsAgainst'	=> (integer)$this->totalStealsAgainst,
	      
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
