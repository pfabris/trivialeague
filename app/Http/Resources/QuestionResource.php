<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class QuestionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 				=> $this->id,
	      'question' 		=> $this->question,  
	      'answer' 			=> $this->answer,  
	      'notes' 			=> $this->notes,
	      'averageScore' 	=> $this->averageScore,
	      'questionNumber' 	=> (integer)$this->questionNumber,
	      'round_id' 		=> $this->round_id,  
	      'mediaFileName' 	=> $this->mediaFileName,
	      'mediaMIMEType' 	=> $this->mediaMIMEType,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
