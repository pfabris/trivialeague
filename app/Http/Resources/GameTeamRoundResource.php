<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GameTeamRoundResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 				=> $this->id,
	      'gameteam_id' 	=> $this->gameteam_id,  
	      'round_id' 		=> $this->round_id,
	      'roundNumber' 	=> (integer)$this->roundNumber,
	      'totalScore' 		=> (integer)$this->totalScore,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
