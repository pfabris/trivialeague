<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GameResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 				=> $this->id,
	      'location_id' 	=> $this->location_id,  
	      'gamenight_id' 	=> $this->gamenight_id,  
	      'quizmaster_id' 	=> $this->user_id,
	      'notes' 			=> $this->notes,
	      'quizmasterName' 	=> optional($this->user)->nameFull,
	      'locationName' 	=> optional($this->location)->locationName,
	      'date'			=> optional($this->gamenight)->date,
	      'gameNumber'		=> optional($this->gamenight)->gameNumber,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
