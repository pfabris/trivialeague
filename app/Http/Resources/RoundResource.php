<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RoundResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 				=> $this->id,
	      'title' 			=> $this->title,  
	      'description' 	=> $this->description,
	      'averageScore' 	=> $this->averageScore,
	      'roundNumber' 	=> (integer)$this->roundNumber,
	      'gamenight_id' 	=> $this->gamenight_id,  
	      'author_id' 		=> $this->user_id,
	      'authorName' 		=> optional($this->user)->nameFull,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
