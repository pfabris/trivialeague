<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GameNightResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
		    'id' 			=> $this->id,
		    'season_id' 	=> $this->season_id,  
		    'date' 			=> $this->date,  
		    'gameNumber' 	=> (integer)$this->gameNumber,
        ];
       
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
