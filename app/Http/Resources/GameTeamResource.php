<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class GameTeamResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
		    'id' 			=> $this->id,
		    'game_id' 		=> $this->game_id,  
		    'team_id' 		=> $this->team_id,
		    'teamPosition' 	=> (integer)$this->teamPosition,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
