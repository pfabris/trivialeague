<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TeamResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
		    'id' 			=> $this->id,
		    'league_id' 	=> $this->league_id,  
		    'teamName' 		=> $this->teamName,  
		    'teamShortName' => $this->teamShortName,
        ];
       
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
