<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ScoringRoundResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        return [
	      'id' 				=> $this->id,
	      'gameplayer_id' 	=> $this->gameplayer_id,  
	      'round_id' 		=> $this->round_id,
	      'question_id' 	=> $this->question_id,
	      'questionNumber' 	=> $this->questionNumber,
	      'roundNumber'		=> $this->roundNumber,
	      'totalScore'		=> $this->totalScore,
        ];
    }
    
    public function with($request)
    {
	    return ["status" => "success"];
    }
}
