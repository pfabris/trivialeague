<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Team extends Model
{
	use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'teamName', 'teamShortName', 
	    'location_id', 'user_id',
	  ];
	
	/**
     * Get the Location record associated with the Team
     */
    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }
    
    /**
     * Get the User record associated with the Team (the Team Captain)
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    
    /**
     * Get all the Users associated with the Team (the TeamMates)
     */
	public function users()
    {
        return $this->hasMany('App\Models\User');
    }
    
    /**
     * Get all the GameTeam records played by the Team 
     */
	public function gameTeams()
    {
        return $this->hasMany('App\Models\GameTeam');
    }
    
    
}

