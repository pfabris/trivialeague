<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class GameNight extends Model
{
    use Uuids;
    
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	protected $table = 'gamenights';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'gameNumber', 'date', 'season_id', 
	];



	/**
     * Get the Season record associated with the GameNight
     */
    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }
    
    /**
     * Get the Games associated with the GameNight (1 Game per Location)
     */
	public function games()
    {
        return $this->hasMany('App\Models\Game', 'gamenight_id');
    }
    
    /**
     * Get the Locations associated with the GameNight, through the Game Relationship
     */
    public function locations()
    {
        return $this->hasManyThrough(
        	'App\Models\Location', 	// final model
        	'App\Models\Game',		// intermediate model
        	'gamenight_id',			// Foreign key on intermediate model...
            'id' 					// Foreign key on final model...
         );
    }
    
    /**
     * Get the GameTeams associated with the GameNight, through the Game Relationship
     */
    public function gameteams()
    {
        return $this->hasManyThrough(
        	'App\Models\GameTeam', 	// final model
        	'App\Models\Game',		// intermediate model
        	'gamenight_id',			// Foreign key on intermediate model...
            'game_id' 				// Foreign key on final model...
        );
    }
    
    /**
     * Get the Rounds associated with the GameNight
     */
	public function rounds()
    {
        return $this->hasMany('App\Models\Round', 'gamenight_id')->orderBy('roundNumber');
    }
}
