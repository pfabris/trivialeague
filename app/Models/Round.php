<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Round extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'title', 'description', 'roundNumber', 'averageScore', 
	    'gamenight_id', 'user_id',
	];
	
	/**
     * Get the User record associated with this Round (the Author)
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
	
	/**
     * Get the GameNight record associated with the Round.
     */
    public function gamenight()
    {
        return $this->belongsTo('App\Models\GameNight','gamenight_id');
    }
    
    /**
     * Get the Questions associated with the Round 
     */
	public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }
}
