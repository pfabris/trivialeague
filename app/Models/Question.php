<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Question extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'questionNumber', 'question', 'notes', 'answer', 'mediaFileName', 
	    'mediaMIMEType', 'averageScore', 'round_id',
	];
	
	/**
     * Get the Round record associated with this Question
     */
    public function round()
    {
        return $this->belongsTo('App\Models\Round');
    }

}
