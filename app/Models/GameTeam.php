<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class GameTeam extends Model
{
	use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $table = 'gameteams';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'game_id', 'team_id', 'teamPosition', 'countWin', 'countTie'
	];


	/**
	 * Get the Game record associated with the GameTeam
	 */
	public function game()
	{
		return $this->belongsTo('App\Models\Game');
	}
	
	/**
	 * Get the Team record associated with the GameTeam
	 */
	public function team()
	{
		return $this->belongsTo('App\Models\Team');
	}
	


	/**
	 * Get the GamePlayers associated with the GameTeam (5 players per GameTeam)
	 */
	public function gameplayers()
	{
		return $this->hasMany('App\Models\GamePlayer', 'gameteam_id');
	}
	
	/**
	 * Get the GameTeamRounds associated with the GameTeam (10 rounds per GameTeam)
	 */
	public function gameteamrounds()
	{
		return $this->hasMany('App\Models\GameTeamRound', 'gameteam_id');
	}
	
	/**
	 * Get the GameNight associated with the GameTeam through the Game
	 */
	public function gamenight()
	{
		return $this->hasManyThrough(
			'App\Models\GameNight', // final model
			'App\Models\Game',		// intermediate model
			'gamenight_id',			// Foreign key on intermediate model...
			'id' 					// Foreign key on final model...
		);
	}

}
