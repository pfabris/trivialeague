<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class GamePlayer extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	
	protected $table 		= 'gameplayers';
    protected $primaryKey 	= 'id';
    protected $guarded 		= ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'gameteam_id', 'user_id', 'seat', 'totalDeuces', 'totalStealsAgainst',
	];
	

	/**
     * Get the GameTeam record associated with this GamePlayer
     */
    public function gameteam()
    {
        return $this->belongsTo('App\Models\GameTeam', 'gameteam_id');
    }
    
    /**
     * Get the User record associated with this GamePlayer (the Player)
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    /**
     * Get the Scoring Rounds associated with the GamePlayer (10 rounds per GamePlayer)
     */
	public function scoringrounds()
    {
        return $this->hasMany('App\Models\ScoringRound', 'scoringround_id');
    }
    
    /**
     * Get the Scores associated with the GamePlayer (10 scores per GamePlayer)
     */
	public function scores()
    {
        return $this->hasMany('App\Models\Score', 'gameplayer_id');
    }

}
