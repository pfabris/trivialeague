<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Emadadly\LaravelUuid\Uuids;
use Spatie\Permission\Traits\HasRoles;  //https://github.com/spatie/laravel-permission

class User extends Authenticatable
{
    use Notifiable;
	use Uuids;
	use HasRoles;  // from Spatie\Permission\Traits\HasRoles -- php artisan cache:forget spatie.permission.cache
	
	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'primaryRole', 'type', 'status', 'nameFirst', 'nameLast', 'nameFull', 
        'address', 'city', 'province', 'postalCode', 'phoneMobile', 'email', 'password', 
        'dateJoined', 'notificationPreference', 'optionsJSON', 
        'team_id', 'league_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    /**
     * Get the League record associated with the User (for Users who are NOT members of a Team)
     */
    public function league()
    {
        return $this->belongsTo('App\Models\League');
    }
    
	/**
     * Get the Team record associated with the User (for Users who ARE members of a Team)
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
    
    /**
     * Get the Invites associated with the User
     */
    public function invites()
    {
        return $this->hasMany('App\Models\Invite');
    }
    
    /**
     * Get the GamePlayers associated with the User
     */
    public function gameplayers()
    {
        return $this->hasMany('App\Models\GamePlayer');
    }
    
    /**
     * Get the Scores associated with the User
     */
    public function scores()
    {
        return $this->hasManyThrough(
			'App\Models\Score', 	// final model
			'App\Models\GamePlayer',// intermediate model
			'user_id',				// Foreign key on intermediate model...
			'gameplayer_id' 		// Foreign key on final model...
		);
    }
    
    
}
