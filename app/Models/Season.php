<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Season extends Model
{
	use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'seasonName', 'dateStart', 'dateEnd', 'league_id',
	];
	
	
	/**
	 * Get the League record associated with the Season
	 */
	public function league()
	{
		return $this->belongsTo('App\Models\League');
	}
	
	
	
	/**
	 * Get the GameNights associated with the Season, in reverse order
	 */
	public function gamenights()
	{
		return $this->hasMany('App\Models\GameNight');
	}
}
