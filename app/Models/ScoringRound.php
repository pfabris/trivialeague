<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class ScoringRound extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	protected $table = 'scoringrounds';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'gameplayer_id', 'round_id', 'question_id', 'questionNumber', 
	    'roundNumber', 'totalScore'
	];
	
	/**
     * Get the GamePlayer record associated with this ScoringRound
     */
    public function gameplayer()
    {
        return $this->belongsTo('App\Models\GamePlayer');
    }
    
    /**
     * Get the Round record associated with this ScoringRound
     */
    public function round()
    {
        return $this->belongsTo('App\Models\Round');
    }
    
    /**
     * Get the Question record associated with this ScoringRound
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }
    
    /**
     * Get the Scores associated with the ScoringRound (10 scores per ScoringRound)
     */
	public function scores()
    {
        return $this->hasMany('App\Models\Score');
    }
    
    
}