<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Invite extends Model
{
	use Uuids;
	
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $table = 'invites';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    
	protected $fillable = [
		'email', 'token', 'user_id'
	];
	
	
	/**
     * Get the User record associated with the Invite
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
