<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Score extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'scoringround_id', 
	    'gameteam_id', 
	    'gameplayer_id', 
	    'round_id', 
	    'question_id', 
	    'sequence', 
	    'roundNumber', 
	    'questionNumber', 
	    'score', 
	    'scoreValue', 
	    'countDeuce', 
	    'countTeam', 
	    'countStealAgainst', 
	];
	
	
	
    /**
     * Get the GameTeam record associated with this Score
     */
    public function gameteam()
    {
        return $this->belongsTo('App\Models\GameTeam', 'gameteam_id');
    }


    /**
     * Get the GamePlayer record associated with this Score
     */
    public function gameplayer()
    {
        return $this->belongsTo('App\Models\GamePlayer', 'gameplayer_id');
    }
    
    /**
     * Get the Round record associated with this Score
     */
    public function round()
    {
        return $this->belongsTo('App\Models\Round');
    }
    
    /**
     * Get the Question record associated with this Score
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

}
