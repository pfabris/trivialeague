<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Game extends Model
{
	
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

    protected $primaryKey = 'id';
    protected $guarded = ['id'];
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_id', 'gamenight_id', 'user_id', 'notes',
    ];
    
    
    
    /**
     * Get the Location record associated with the Game.
     */
    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }
    
    /**
     * Get the GameNight record associated with the Game.
     */
    public function gamenight()
    {
        return $this->belongsTo('App\Models\GameNight','gamenight_id');
    }
    
    /**
     * Get the User record associated with the Game (the Quiz Master)
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
	/**
     * Get the GameTeams associated with the Game (2 teams per Game)
     */
	public function gameteams()
    {
        return $this->hasMany('App\Models\GameTeam');
    }
    
}
