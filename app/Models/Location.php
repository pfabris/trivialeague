<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class Location extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'locationName', 'address', 'city', 'province', 'postalCode', 
	    'website', 'phone', 'email', 'league_id', 'user_id',
	];
	
	
	/**
     * Get the User record associated with this Location (the Location Contact)
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    /**
     * Get the League record associated with this Location
     */
    public function league()
    {
        return $this->belongsTo('App\Models\League');
    }
    
    
    /**
     * Get the Teams associated with the Location 
     */
	public function teams()
    {
        return $this->hasMany('App\Models\Team');
    }
    
    /**
     * Get the Games associated with the Location 
     */
	public function games()
    {
        return $this->hasMany('App\Models\Game');
    }
    
	/**
     * Get the GameNights associated with the Location through the Game
     */
	public function gamesNights()
    {
        return $this->hasManyThrough
        (
        'App\Models\GameNight', // Final Model
        'App\Models\Game',		// Intermediate Model
        	'location_id', 	// Foreign key on Intermediate Model
            'id', 				// Foreign key on Final Model
            'id',				// Local key of this Model
            'gamenight_id'			// Local key of Intermediate Model
        );
    }
    
    /**
     * Get the User associated with the Team (the Captain) 
     */
	public function captains()
    {
        return $this->hasManyThrough
        (
        	'App\Models\User',	// Final Model
        	'App\Models\Team',  // Intermediate Model
        	'location_id', 		// Foreign key on Intermediate Model
            'id', 				// Foreign key on Final Model
            'id',				// Local key of this Model
            'user_id'			// Local key of Intermediate Model
		);
    }

}
