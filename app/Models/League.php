<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class League extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'leagueName', 'neighbourhood', 'city', 'province', 'country',
	    'gameNightDayOfWeek', 'roundsPerGame', 'questionsPerRound', 'seatsPerGame', 
	    'user_id',
	];
	
	
	/**
     * Get the User record associated with this League (the League Manager)
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

	/**
     * Get the Seasons associated with the League
     */
	public function seasons()
    {
        return $this->hasMany('App\Models\Season');
    }
    
    /**
     * Get the Locations associated with the League
     */
	public function locations()
    {
        return $this->hasMany('App\Models\Location');
    }
    

}
