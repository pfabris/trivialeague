<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class GameTeamRound extends Model
{
    use Uuids;
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $table = 'gameteamrounds';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'gameteam_id', 'round_id', 'roundNumber', 'totalScore', 'totalDeuces', 'totalStealsFor',
	];

	/**
     * Get the GameTeam record associated with this GameTeamRound
     */
    public function gameteam()
    {
        return $this->belongsTo('App\Models\GameTeam', 'gameteam_id');
    }

}
