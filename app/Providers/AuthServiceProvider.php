<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        
        $this->registerPolicies();

		/* Migrated to Spatie Roles & Permissions
        Gate::define('see-admin', function ($user) {
			//
    	});
    	
    	Gate::define('edit-scorecard', function ($user) {
	    	//
    	});
		
    	Gate::define('see-scorecard', function ($user) {
	    	//
    	});
		*/
    }
}
