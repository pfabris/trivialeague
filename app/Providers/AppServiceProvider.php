<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Events\Dispatcher;

//use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

use App\Models\League;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */

	public function boot(Dispatcher $events)
	{
		
		Schema::defaultStringLength(191); // For MySQL mb Compatibility
		
		/*
		// AdminLTE Menu
		// https://github.com/jeroennoten/Laravel-AdminLTE#51-menu
		$events->listen(BuildingMenu::class, function (BuildingMenu $event) {
			$event->menu->add('ADMIN');
			
			$event->menu->add([
				'text' => 'Current Season',
				'url' => '/admin/season/currentSeason' ,
				'icon' => 'calendar',
				'can' => 'see-admin',
			]);
		});
		*/
	}
	
	
	
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
