<?php

namespace App\Events;

use App\Models\Score;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

	/**
     * This Event occurs whenever someone clicks on a Score cell in the Admin interface
     * 
     */

class ScoreToggled implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	public $broadcastData;

    /**
     * Create a new Scored event instance
     *
     * @return void
     */
    public function __construct($response)
    {
        $this->broadcastData = $response;
    }

	public function broadcastAs()
    {
        return 'scoreUpdated';
    }
    
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('broadcastScore');
    }
}
